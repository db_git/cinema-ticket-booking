namespace cinema_ticket_booking.api.Services;

public static class CorsService
{
	public static void ConfigureCors(this IServiceCollection services)
	{
		services.AddCors(c => c
			.AddPolicy("React", b => b
				.AllowAnyOrigin()
				.AllowAnyMethod()
				.AllowAnyHeader()
				.WithExposedHeaders("records")));
	}
}
