using cinema_ticket_booking.dal.Identity;
using cinema_ticket_booking.dal.common;
using Microsoft.AspNetCore.Identity;

namespace cinema_ticket_booking.api.Services;

public static class IdentityService
{
	public static void ConfigureIdentity(this IServiceCollection services)
	{
		services.AddIdentity<User, Role>(options =>
			{
				options.Password.RequireLowercase = false;
				options.Password.RequireNonAlphanumeric = false;
				options.Password.RequireUppercase = false;
				options.Password.RequiredLength = 8;
			})
			.AddEntityFrameworkStores<DatabaseContext>()
			.AddDefaultTokenProviders();
	}
}
