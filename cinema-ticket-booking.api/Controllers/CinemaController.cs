using AutoMapper;
using cinema_ticket_booking.api.Filters;
using cinema_ticket_booking.dal;
using cinema_ticket_booking.model.CinemaRest;
using cinema_ticket_booking.service.common;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace cinema_ticket_booking.api.Controllers;

[ApiController]
[Route("api/cinema")]
public class CinemaController : ControllerBase
{
	private readonly IGenericService<Cinema, UpdateCinemaDto> _service;
	private readonly IMapper _mapper;

	public CinemaController(IGenericService<Cinema, UpdateCinemaDto> service, IMapper mapper)
	{
		_service = service;
		_mapper = mapper;
	}

	[HttpGet]
	public async Task<IActionResult> GetAll([FromQuery] int? page, int? size, string? search, int? sort)
	{
		try
		{
			var cinemas = await _service.GetAll(search, sort);
			if (cinemas == null || cinemas.Count == 0) return StatusCode(StatusCodes.Status204NoContent);

			if (page is null || size is null || page.Value < 1 || size.Value is < 1 or > 200)
			{
				page = 1;
				size = 10;
			}

			var pages = Pagination<Cinema>.Create(
				cinemas.AsQueryable().AsNoTracking(),
				page.Value, size.Value
			);

			Response.Headers.Add("records", cinemas.Count.ToString());
			if (pages.Count == 0) return StatusCode(StatusCodes.Status204NoContent);
			return StatusCode(StatusCodes.Status200OK, _mapper.Map<ICollection<CinemaDto>>(pages));
		}
		catch
		{
			return StatusCode(StatusCodes.Status500InternalServerError);
		}
	}

	[HttpGet]
	[Route("{id:Guid}")]
	public async Task<IActionResult> Get(Guid id)
	{
		try
		{
			var cinema = await _service.Get(g => g.Id == id);
			if (cinema == null) return StatusCode(StatusCodes.Status400BadRequest);

			return StatusCode(StatusCodes.Status200OK, _mapper.Map<CinemaDto>(cinema));
		}
		catch
		{
			return StatusCode(StatusCodes.Status500InternalServerError);
		}
	}

	[HttpPost]
	[Authorize(Roles = "Admin")]
	public async Task<IActionResult> Create([FromForm] CreateCinemaDto createCinema)
	{
		try
		{
			var cinema = _mapper.Map<Cinema>(createCinema);
			await _service.Create(cinema);

			return StatusCode(StatusCodes.Status201Created, _mapper.Map<CinemaDto>(cinema));
		}
		catch
		{
			return StatusCode(StatusCodes.Status500InternalServerError);
		}
	}

	[HttpPut]
	[Authorize(Roles = "Admin")]
	[Route("{id:Guid}")]
	public async Task<IActionResult> Update(Guid id, [FromForm] UpdateCinemaDto updateCinema)
	{
		try
		{
			await _service.Update(id, updateCinema);
			return StatusCode(StatusCodes.Status200OK);
		}
		catch
		{
			return StatusCode(StatusCodes.Status400BadRequest);
		}
	}

	[HttpDelete]
	[Authorize(Roles = "Admin")]
	[Route("{id:Guid}")]
	public async Task<IActionResult> Delete(Guid id)
	{
		try
		{
			await _service.Delete(id);
			return StatusCode(StatusCodes.Status200OK);
		}
		catch
		{
			return StatusCode(StatusCodes.Status400BadRequest);
		}
	}
}
