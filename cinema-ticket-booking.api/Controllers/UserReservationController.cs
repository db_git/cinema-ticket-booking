using AutoMapper;
using cinema_ticket_booking.api.Filters;
using cinema_ticket_booking.dal;
using cinema_ticket_booking.model.ReservationRest;
using cinema_ticket_booking.service.common;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Net.Http.Headers;

namespace cinema_ticket_booking.api.Controllers;

[ApiController]
[Authorize(Roles = "User")]
[Route("api/reservation/user")]
public class UserReservationController : ControllerBase
{
	private readonly IUserReservationService _service;
	private readonly IMapper _mapper;

	public UserReservationController(IUserReservationService service, IMapper mapper)
	{
		_service = service;
		_mapper = mapper;
	}

	[HttpGet]
	public async Task<IActionResult> GetAll([FromQuery] int? page, int? size, string? search, int? sort)
	{
		try
		{
			var reservations = await _service.UserGetAll(GetToken(), search, sort);
			if (reservations == null || reservations.Count == 0) return StatusCode(StatusCodes.Status204NoContent);

			if (page is null || size is null || page.Value < 1 || size.Value is < 1 or > 200)
			{
				page = 1;
				size = 10;
			}

			var pages = Pagination<Reservation>.Create(
				reservations.AsQueryable().AsNoTracking(),
				page.Value, size.Value
			);

			Response.Headers.Add("records", reservations.Count.ToString());
			if (pages.Count == 0) return StatusCode(StatusCodes.Status204NoContent);
			return StatusCode(StatusCodes.Status200OK, _mapper.Map<ICollection<ReservationDto>>(pages));
		}
		catch
		{
			return StatusCode(StatusCodes.Status500InternalServerError);
		}
	}

	[HttpGet]
	[Route("{id:Guid}")]
	public async Task<IActionResult> Get(Guid id)
	{
		try
		{
			var reservation = await _service.UserGet(GetToken(), g => g.Id == id);
			if (reservation == null) return StatusCode(StatusCodes.Status400BadRequest);

			return StatusCode(StatusCodes.Status200OK, _mapper.Map<ReservationDto>(reservation));
		}
		catch
		{
			return StatusCode(StatusCodes.Status500InternalServerError);
		}
	}

	[HttpPost]
	public async Task<IActionResult> Create([FromForm] CreateReservationDto createReservation)
	{
		try
		{
			var reservation = _mapper.Map<Reservation>(createReservation);
			await _service.UserCreate(GetToken(), reservation);

			return StatusCode(StatusCodes.Status201Created);
		}
		catch
		{
			return StatusCode(StatusCodes.Status500InternalServerError);
		}
	}

	[HttpPut]
	[Route("{id:Guid}")]
	public async Task<IActionResult> Update(Guid id, [FromForm] UpdateReservationDto updateReservation)
	{
		try
		{
			await _service.UserUpdate(GetToken(), id, updateReservation);
			return StatusCode(StatusCodes.Status200OK);
		}
		catch
		{
			return StatusCode(StatusCodes.Status400BadRequest);
		}
	}

	[HttpDelete]
	[Route("{id:Guid}")]
	public async Task<IActionResult> Delete(Guid id)
	{
		try
		{
			await _service.UserDelete(GetToken(), id);
			return StatusCode(StatusCodes.Status200OK);
		}
		catch
		{
			return StatusCode(StatusCodes.Status400BadRequest);
		}
	}

	private string GetToken()
	{
		return Request.Headers[HeaderNames.Authorization].ToString().Replace("Bearer ", "");
	}
}
