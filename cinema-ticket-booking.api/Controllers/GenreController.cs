using AutoMapper;
using cinema_ticket_booking.api.Filters;
using cinema_ticket_booking.dal;
using cinema_ticket_booking.model.GenreRest;
using cinema_ticket_booking.service.common;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace cinema_ticket_booking.api.Controllers;

[ApiController]
[Route("api/genre")]
public class GenreController : ControllerBase
{
	private readonly IGenericService<Genre, UpdateGenreDto> _service;
	private readonly IMapper _mapper;

	public GenreController(IGenericService<Genre, UpdateGenreDto> service, IMapper mapper)
	{
		_service = service;
		_mapper = mapper;
	}

	[HttpGet]
	public async Task<IActionResult> GetAll([FromQuery] int? page, int? size, string? search, int? sort)
	{
		try
		{
			var genres = await _service.GetAll(search, sort);
			if (genres == null || genres.Count == 0) return StatusCode(StatusCodes.Status204NoContent);

			if (page is null || size is null || page.Value < 1 || size.Value is < 1 or > 200)
			{
				page = 1;
				size = 10;
			}

			var pages = Pagination<Genre>.Create(
				genres.AsQueryable().AsNoTracking(),
				page.Value, size.Value
			);

			Response.Headers.Add("records", genres.Count.ToString());
			if (pages.Count == 0) return StatusCode(StatusCodes.Status204NoContent);
			return StatusCode(StatusCodes.Status200OK, _mapper.Map<ICollection<GenreDto>>(pages));
		}
		catch
		{
			return StatusCode(StatusCodes.Status500InternalServerError);
		}
	}

	[HttpGet]
	[Route("{id:Guid}")]
	public async Task<IActionResult> Get(Guid id)
	{
		try
		{
			var genre = await _service.Get(g => g.Id == id);
			if (genre == null) return StatusCode(StatusCodes.Status400BadRequest);

			return StatusCode(StatusCodes.Status200OK, _mapper.Map<GenreDto>(genre));
		}
		catch
		{
			return StatusCode(StatusCodes.Status500InternalServerError);
		}
	}

	[HttpPost]
	[Authorize(Roles = "Admin")]
	public async Task<IActionResult> Create([FromForm] CreateGenreDto createGenre)
	{
		try
		{
			var genre = _mapper.Map<Genre>(createGenre);
			await _service.Create(genre);

			return StatusCode(StatusCodes.Status201Created, _mapper.Map<GenreDto>(genre));
		}
		catch
		{
			return StatusCode(StatusCodes.Status500InternalServerError);
		}
	}

	[HttpPut]
	[Authorize(Roles = "Admin")]
	[Route("{id:Guid}")]
	public async Task<IActionResult> Update(Guid id, [FromForm] UpdateGenreDto updateGenre)
	{
		try
		{
			await _service.Update(id, updateGenre);
			return StatusCode(StatusCodes.Status200OK);
		}
		catch
		{
			return StatusCode(StatusCodes.Status400BadRequest);
		}
	}

	[HttpDelete]
	[Authorize(Roles = "Admin")]
	[Route("{id:Guid}")]
	public async Task<IActionResult> Delete(Guid id)
	{
		try
		{
			await _service.Delete(id);
			return StatusCode(StatusCodes.Status200OK);
		}
		catch
		{
			return StatusCode(StatusCodes.Status400BadRequest);
		}
	}
}
