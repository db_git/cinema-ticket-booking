using AutoMapper;
using cinema_ticket_booking.api.Filters;
using cinema_ticket_booking.dal.Identity;
using cinema_ticket_booking.model.UserRest;
using cinema_ticket_booking.service.common;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace cinema_ticket_booking.api.Controllers;

[ApiController]
[Route("api/user")]
public class UserController : ControllerBase
{
	private readonly IUserManagerService _service;
	private readonly IConfiguration _configuration;
	private readonly IMapper _mapper;

	public UserController(IUserManagerService service, IConfiguration configuration, IMapper mapper)
	{
		_service = service;
		_configuration = configuration;
		_mapper = mapper;
	}

	[HttpGet]
	[Authorize(Roles = "Admin")]
	public async Task<IActionResult> GetAll([FromQuery] int? page, int? size, string? search, int? sort)
	{
		try
		{
			var users = await _service.GetAll(search, sort);
			if (users == null || users.Count == 0) return StatusCode(StatusCodes.Status204NoContent);

			if (page is null || size is null || page.Value < 1 || size.Value is < 1 or > 200)
			{
				page = 1;
				size = 10;
			}

			var pages = Pagination<User>.Create(
				users.AsQueryable().AsNoTracking(),
				page.Value, size.Value
			);

			Response.Headers.Add("records", users.Count.ToString());
			if (pages.Count == 0) return StatusCode(StatusCodes.Status204NoContent);
			return StatusCode(StatusCodes.Status200OK, _mapper.Map<ICollection<UserDto>>(pages));
		}
		catch
		{
			return StatusCode(StatusCodes.Status500InternalServerError);
		}
	}

	[HttpGet]
	[Authorize(Roles = "Admin")]
	[Route("{id:Guid}")]
	public async Task<IActionResult> Get(Guid id)
	{
		try
		{
			var user = await _service.Get(u => u.Id == id);
			if (user == null) return StatusCode(StatusCodes.Status400BadRequest);

			return StatusCode(StatusCodes.Status200OK, _mapper.Map<UserDto>(user));
		}
		catch
		{
			return StatusCode(StatusCodes.Status500InternalServerError);
		}
	}

	[HttpPost]
	[Route("register")]
	public async Task<IActionResult> Register([FromForm] UserRegisterDto userDto)
	{
		try
		{
			var user = _mapper.Map<User>(userDto);
			var result = await _service.CreateAsync(user, userDto.Password);

			if (!result.Succeeded) return StatusCode(StatusCodes.Status400BadRequest, result);
			return StatusCode(StatusCodes.Status202Accepted);
		}
		catch
		{
			return StatusCode(StatusCodes.Status500InternalServerError);
		}
	}

	[HttpPost]
	[Route("login")]
	public async Task<IActionResult> Login([FromForm] UserLoginDto userDto)
	{
		try
		{
			var user = await _service.Get(u => u.Email == userDto.Email);

			if (user == null)
				return StatusCode(StatusCodes.Status404NotFound);
			if (!await _service.ValidateUser(userDto.Email, userDto.Password))
				return StatusCode(StatusCodes.Status401Unauthorized);

			return StatusCode(StatusCodes.Status202Accepted, new
				{
					token = await _service.CreateToken(_configuration, userDto.Email)
				}
			);
		}
		catch
		{
			return StatusCode(StatusCodes.Status500InternalServerError);
		}
	}

	[HttpPut]
	[Authorize(Roles = "Admin")]
	[Route("{id:Guid}")]
	public async Task<IActionResult> Update(Guid id, [FromForm] UpdateUserDto updateUser)
	{
		try
		{
			await _service.Update(id, updateUser);
			return StatusCode(StatusCodes.Status200OK);
		}
		catch
		{
			return StatusCode(StatusCodes.Status400BadRequest);
		}
	}

	[HttpDelete]
	[Authorize(Roles = "Admin")]
	[Route("{id:Guid}")]
	public async Task<IActionResult> Delete(Guid id)
	{
		try
		{
			await _service.DeleteAsync(id);
			return StatusCode(StatusCodes.Status200OK);
		}
		catch
		{
			return StatusCode(StatusCodes.Status400BadRequest);
		}
	}
}
