using AutoMapper;
using cinema_ticket_booking.api.Filters;
using cinema_ticket_booking.dal;
using cinema_ticket_booking.model.MovieRest;
using cinema_ticket_booking.service.common;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace cinema_ticket_booking.api.Controllers;

[ApiController]
[Route("api/movie")]
public class MovieController : ControllerBase
{
	private readonly IGenericService<Movie, UpdateMovieDto> _service;
	private readonly IMapper _mapper;

	public MovieController(IGenericService<Movie, UpdateMovieDto> service, IMapper mapper)
	{
		_service = service;
		_mapper = mapper;
	}

	[HttpGet]
	public async Task<ActionResult> GetAll([FromQuery] int? page, int? size, string? search, int? sort)
	{
		try
		{
			var movies = await _service.GetAll(search, sort);
			if (movies == null || movies.Count == 0) return StatusCode(StatusCodes.Status204NoContent);

			if (page is null || size is null || page.Value < 1 || size.Value is < 1 or > 200)
			{
				page = 1;
				size = 10;
			}

			var pages = Pagination<Movie>.Create(
				movies.AsQueryable().AsNoTracking(),
				page.Value, size.Value
			);

			Response.Headers.Add("records", movies.Count.ToString());
			if (pages.Count == 0) return StatusCode(StatusCodes.Status204NoContent);
			return StatusCode(StatusCodes.Status200OK, _mapper.Map<ICollection<MovieDto>>(pages));
		}
		catch
		{
			return StatusCode(StatusCodes.Status500InternalServerError);
		}
	}

	[HttpGet]
	[Route("{id:Guid}")]
	public async Task<ActionResult> Get(Guid id)
	{
		try
		{
			var movie = await _service.Get(g => g.Id == id);
			if (movie == null) return StatusCode(StatusCodes.Status400BadRequest);

			return StatusCode(StatusCodes.Status200OK, _mapper.Map<MovieDto>(movie));
		}
		catch
		{
			return StatusCode(StatusCodes.Status500InternalServerError);
		}
	}

	[HttpPost]
	[Authorize(Roles = "Admin")]
	public async Task<IActionResult> Create([FromForm] CreateMovieDto createMovie)
	{
		try
		{
			var movie = _mapper.Map<Movie>(createMovie);
			await _service.Create(movie);

			return StatusCode(StatusCodes.Status201Created);
		}
		catch
		{
			return StatusCode(StatusCodes.Status500InternalServerError);
		}
	}

	[HttpPut]
	[Authorize(Roles = "Admin")]
	[Route("{id:Guid}")]
	public async Task<IActionResult> Update(Guid id, [FromForm] UpdateMovieDto updateMovie)
	{
		try
		{
			await _service.Update(id, updateMovie);
			return StatusCode(StatusCodes.Status200OK);
		}
		catch
		{
			return StatusCode(StatusCodes.Status400BadRequest);
		}
	}

	[HttpDelete]
	[Authorize(Roles = "Admin")]
	[Route("{id:Guid}")]
	public async Task<IActionResult> Delete(Guid id)
	{
		try
		{
			await _service.Delete(id);
			return StatusCode(StatusCodes.Status200OK);
		}
		catch
		{
			return StatusCode(StatusCodes.Status400BadRequest);
		}
	}
}
