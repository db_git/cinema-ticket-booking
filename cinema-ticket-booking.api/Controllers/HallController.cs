using AutoMapper;
using cinema_ticket_booking.api.Filters;
using cinema_ticket_booking.dal;
using cinema_ticket_booking.model.HallRest;
using cinema_ticket_booking.service.common;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace cinema_ticket_booking.api.Controllers;

[ApiController]
[Route("api/hall")]
public class HallController : ControllerBase
{
	private readonly IGenericService<Hall, UpdateHallDto> _service;
	private readonly IMapper _mapper;

	public HallController(IGenericService<Hall, UpdateHallDto> service, IMapper mapper)
	{
		_service = service;
		_mapper = mapper;
	}

	[HttpGet]
	public async Task<IActionResult> GetAll([FromQuery] int? page, int? size, string? search, int? sort)
	{
		try
		{
			var halls = await _service.GetAll(search, sort);
			if (halls == null || halls.Count == 0) return StatusCode(StatusCodes.Status204NoContent);

			if (page is null || size is null || page.Value < 1 || size.Value is < 1 or > 200)
			{
				page = 1;
				size = 10;
			}

			var pages = Pagination<Hall>.Create(
				halls.AsQueryable().AsNoTracking(),
				page.Value, size.Value
			);

			Response.Headers.Add("records", halls.Count.ToString());
			if (pages.Count == 0) return StatusCode(StatusCodes.Status204NoContent);
			return StatusCode(StatusCodes.Status200OK, _mapper.Map<ICollection<HallDto>>(pages));
		}
		catch
		{
			return StatusCode(StatusCodes.Status500InternalServerError);
		}
	}

	[HttpGet]
	[Route("{id:Guid}")]
	public async Task<IActionResult> Get(Guid id)
	{
		try
		{
			var hall = await _service.Get(g => g.Id == id);
			if (hall == null) return StatusCode(StatusCodes.Status400BadRequest);

			return StatusCode(StatusCodes.Status200OK, _mapper.Map<HallDto>(hall));
		}
		catch
		{
			return StatusCode(StatusCodes.Status500InternalServerError);
		}
	}

	[HttpPost]
	[Authorize(Roles = "Admin")]
	public async Task<IActionResult> Create([FromForm] CreateHallDto createHall)
	{
		try
		{
			var hall = _mapper.Map<Hall>(createHall);
			await _service.Create(hall);

			return StatusCode(StatusCodes.Status201Created, _mapper.Map<HallDto>(hall));
		}
		catch
		{
			return StatusCode(StatusCodes.Status500InternalServerError);
		}
	}

	[HttpPut]
	[Authorize(Roles = "Admin")]
	[Route("{id:Guid}")]
	public async Task<IActionResult> Update(Guid id, [FromForm] UpdateHallDto updateHall)
	{
		try
		{
			await _service.Update(id, updateHall);
			return StatusCode(StatusCodes.Status200OK);
		}
		catch
		{
			return StatusCode(StatusCodes.Status400BadRequest);
		}
	}

	[HttpDelete]
	[Authorize(Roles = "Admin")]
	[Route("{id:Guid}")]
	public async Task<IActionResult> Delete(Guid id)
	{
		try
		{
			await _service.Delete(id);
			return StatusCode(StatusCodes.Status200OK);
		}
		catch
		{
			return StatusCode(StatusCodes.Status400BadRequest);
		}
	}
}
