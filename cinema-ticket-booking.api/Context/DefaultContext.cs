using cinema_ticket_booking.dal.common;
using Microsoft.EntityFrameworkCore;

namespace cinema_ticket_booking.api.Context;

public sealed class DefaultContext : DatabaseContext
{
	public DefaultContext(DbContextOptions options) : base(options) {}
}
