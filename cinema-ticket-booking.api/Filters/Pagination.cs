namespace cinema_ticket_booking.api.Filters;

public class Pagination<T> : List<T>
{
	private const int MaxPageSize = 50;
	private int PageIndex { get; }
	private int  TotalPages { get; }

	private Pagination(IEnumerable<T> items, int count, int index, int size)
	{
		PageIndex = index;
		TotalPages = (int)Math.Ceiling(count / (double)size);
		AddRange(items);
	}

	public bool HasPreviousPage => PageIndex > 1;
	public bool HasNextPage => PageIndex < TotalPages;

	public static Pagination<T> Create(IQueryable<T> source, int index, int size)
	{
		size = size > MaxPageSize ? MaxPageSize : size;
		var count = source.Count();
		var items = source.Skip((index - 1) * size).Take(size).ToList();
		return new Pagination<T>(items, count, index, size);
	}
}
