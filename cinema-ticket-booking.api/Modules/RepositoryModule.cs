using Autofac;
using cinema_ticket_booking.dal;
using cinema_ticket_booking.dal.Identity;
using cinema_ticket_booking.repository;
using cinema_ticket_booking.repository.common;

namespace cinema_ticket_booking.api.Modules;

public sealed class RepositoryModule : Module
{
	protected override void Load(ContainerBuilder builder)
	{
		builder.RegisterType<RepositoryWork>()
			.AsSelf()
			.As<IRepositoryWork>()
			.AsImplementedInterfaces()
			.InstancePerLifetimeScope();
#region Cinema
		builder.RegisterType<GenericRepository<Cinema>>()
			.AsSelf()
			.As<IGenericRepository<Cinema>>()
			.AsImplementedInterfaces()
			.InstancePerLifetimeScope();
#endregion
#region Genre
		builder.RegisterType<GenericRepository<Genre>>()
			.AsSelf()
			.As<IGenericRepository<Genre>>()
			.AsImplementedInterfaces()
			.InstancePerLifetimeScope();
#endregion
#region Hall
		builder.RegisterType<GenericRepository<Hall>>()
			.AsSelf()
			.As<IGenericRepository<Hall>>()
			.AsImplementedInterfaces()
			.InstancePerLifetimeScope();
#endregion
#region Movie
		builder.RegisterType<GenericRepository<Movie>>()
			.AsSelf()
			.As<IGenericRepository<Movie>>()
			.AsImplementedInterfaces()
			.InstancePerLifetimeScope();
#endregion
#region MovieGenre
		builder.RegisterType<GenericRepository<MovieGenre>>()
			.AsSelf()
			.As<IGenericRepository<MovieGenre>>()
			.AsImplementedInterfaces()
			.InstancePerLifetimeScope();
#endregion
#region MoviePerson
		builder.RegisterType<GenericRepository<MoviePerson>>()
			.AsSelf()
			.As<IGenericRepository<MoviePerson>>()
			.AsImplementedInterfaces()
			.InstancePerLifetimeScope();
#endregion
#region MovieRating
		builder.RegisterType<GenericRepository<MovieRating>>()
			.AsSelf()
			.As<IGenericRepository<MovieRating>>()
			.AsImplementedInterfaces()
			.InstancePerLifetimeScope();
#endregion
#region Person
		builder.RegisterType<GenericRepository<Person>>()
			.AsSelf()
			.As<IGenericRepository<Person>>()
			.AsImplementedInterfaces()
			.InstancePerLifetimeScope();
#endregion
#region Projection
		builder.RegisterType<GenericRepository<Projection>>()
			.AsSelf()
			.As<IGenericRepository<Projection>>()
			.AsImplementedInterfaces()
			.InstancePerLifetimeScope();
#endregion
#region Rating
		builder.RegisterType<GenericRepository<Rating>>()
			.AsSelf()
			.As<IGenericRepository<Rating>>()
			.AsImplementedInterfaces()
			.InstancePerLifetimeScope();
#endregion
#region Reservation
		builder.RegisterType<GenericRepository<Reservation>>()
			.AsSelf()
			.As<IGenericRepository<Reservation>>()
			.AsImplementedInterfaces()
			.InstancePerLifetimeScope();
#endregion
#region Seat
		builder.RegisterType<GenericRepository<Seat>>()
			.AsSelf()
			.As<IGenericRepository<Seat>>()
			.AsImplementedInterfaces()
			.InstancePerLifetimeScope();
#endregion
#region User
		builder.RegisterType<GenericRepository<User>>()
			.AsSelf()
			.As<IGenericRepository<User>>()
			.AsImplementedInterfaces()
			.InstancePerLifetimeScope();
#endregion
	}
}
