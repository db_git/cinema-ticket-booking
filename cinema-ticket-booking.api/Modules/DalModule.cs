using Autofac;
using cinema_ticket_booking.api.Context;
using cinema_ticket_booking.dal.common;
using Microsoft.EntityFrameworkCore;

namespace cinema_ticket_booking.api.Modules;

public sealed class DalModule : Module
{
	protected override void Load(ContainerBuilder builder)
	{
		builder.Register(c =>
		{
			var configuration = c.Resolve<IConfiguration>();
			var options = new DbContextOptionsBuilder<DefaultContext>();
			options.UseNpgsql(configuration.GetConnectionString("postgreSQL"));

			return new DefaultContext(options.Options);
		}).AsSelf().InstancePerLifetimeScope();

		builder.Register(c =>
		{
			var configuration = c.Resolve<IConfiguration>();
			var options = new DbContextOptionsBuilder<DatabaseContext>();
			options.UseNpgsql(configuration.GetConnectionString("postgreSQL"));

			return new DatabaseContext(options.Options);
		}).AsSelf().InstancePerLifetimeScope();
	}
}
