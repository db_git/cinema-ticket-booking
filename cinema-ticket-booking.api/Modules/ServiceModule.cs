using Autofac;
using cinema_ticket_booking.dal;
using cinema_ticket_booking.model.CinemaRest;
using cinema_ticket_booking.model.GenreRest;
using cinema_ticket_booking.model.HallRest;
using cinema_ticket_booking.model.MovieRest;
using cinema_ticket_booking.model.PersonRest;
using cinema_ticket_booking.model.ProjectionRest;
using cinema_ticket_booking.model.RatingRest;
using cinema_ticket_booking.model.ReservationRest;
using cinema_ticket_booking.model.SeatRest;
using cinema_ticket_booking.service;
using cinema_ticket_booking.service.common;

namespace cinema_ticket_booking.api.Modules;

public sealed class ServiceModule : Module
{
	protected override void Load(ContainerBuilder builder)
	{
#region Cinema
		builder.RegisterType<CinemaService>()
			.AsSelf()
			.As<IGenericService<Cinema, UpdateCinemaDto>>()
			.AsImplementedInterfaces()
			.InstancePerLifetimeScope();
#endregion
#region Genre
		builder.RegisterType<GenreService>()
			.AsSelf()
			.As<IGenericService<Genre, UpdateGenreDto>>()
			.AsImplementedInterfaces()
			.InstancePerLifetimeScope();
#endregion
#region Hall
		builder.RegisterType<HallService>()
			.AsSelf()
			.As<IGenericService<Hall, UpdateHallDto>>()
			.AsImplementedInterfaces()
			.InstancePerLifetimeScope();
#endregion
#region Movie
		builder.RegisterType<MovieService>()
			.AsSelf()
			.As<IGenericService<Movie, UpdateMovieDto>>()
			.AsImplementedInterfaces()
			.InstancePerLifetimeScope();
#endregion
#region Person
		builder.RegisterType<PersonService>()
			.AsSelf()
			.As<IGenericService<Person, UpdatePersonDto>>()
			.AsImplementedInterfaces()
			.InstancePerLifetimeScope();
#endregion
#region Projection
		builder.RegisterType<ProjectionService>()
			.AsSelf()
			.As<IGenericService<Projection, UpdateProjectionDto>>()
			.AsImplementedInterfaces()
			.InstancePerLifetimeScope();
#endregion
#region Rating
		builder.RegisterType<RatingService>()
			.AsSelf()
			.As<IGenericService<Rating, UpdateRatingDto>>()
			.AsImplementedInterfaces()
			.InstancePerLifetimeScope();
#endregion
#region Reservation
		builder.RegisterType<ReservationService>()
			.AsSelf()
			.As<IGenericService<Reservation, UpdateReservationDto>>()
			.AsImplementedInterfaces()
			.InstancePerLifetimeScope();
#endregion
#region Seat
		builder.RegisterType<SeatService>()
			.AsSelf()
			.As<IGenericService<Seat, UpdateSeatDto>>()
			.AsImplementedInterfaces()
			.InstancePerLifetimeScope();
#endregion
#region User
		builder.RegisterType<UserService>()
			.AsSelf()
			.As<IUserManagerService>()
			.AsImplementedInterfaces()
			.InstancePerLifetimeScope();
#endregion
#region UserReservation
		builder.RegisterType<UserReservationService>()
			.AsSelf()
			.As<IUserReservationService>()
			.AsImplementedInterfaces()
			.InstancePerLifetimeScope();
#endregion
	}
}
