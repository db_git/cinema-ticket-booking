using Autofac;
using AutoMapper;
using cinema_ticket_booking.model.common;

namespace cinema_ticket_booking.api.Modules;

public sealed class ModelModule : Module
{
	protected override void Load(ContainerBuilder builder)
	{
		builder.Register(c => new MapperConfiguration(cfg =>
			{
				cfg.AddProfile<CinemaMapper>();
				cfg.AddProfile<GenreMapper>();
				cfg.AddProfile<HallMapper>();
				cfg.AddProfile<MovieMapper>();
				cfg.AddProfile<PersonMapper>();
				cfg.AddProfile<ProjectionMapper>();
				cfg.AddProfile<RatingMapper>();
				cfg.AddProfile<ReservationMapper>();
				cfg.AddProfile<SeatMapper>();
				cfg.AddProfile<UserMapper>();
			}
		)).As<AutoMapper.IConfigurationProvider>().SingleInstance();
	}
}
