using Autofac;
using Autofac.Extensions.DependencyInjection;
using AutoMapper;
using cinema_ticket_booking.api.Modules;
using cinema_ticket_booking.api.Services;
using Newtonsoft.Json;

var builder = WebApplication.CreateBuilder(args);
var configuration = builder.Configuration;

builder.Services.AddControllers().AddControllersAsServices();
builder.Services.ConfigureCors();

builder.Services.AddAuthentication();
builder.Services.ConfigureIdentity();
builder.Services.ConfigureJwt(configuration);

builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();
builder.Services.AddControllers().AddNewtonsoftJson(op =>
	op.SerializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore);

builder.Host.UseServiceProviderFactory(new AutofacServiceProviderFactory());
builder.Host.ConfigureContainer<ContainerBuilder>(containerBuilder =>
{
	containerBuilder.RegisterModule(new DalModule());
	containerBuilder.RegisterModule(new ModelModule());
	containerBuilder.RegisterModule(new RepositoryModule());
	containerBuilder.RegisterModule(new ServiceModule());
	containerBuilder.Register(c =>
		new Mapper(c.Resolve<AutoMapper.IConfigurationProvider>()))
		.As<IMapper>();
});

var app = builder.Build();

if (app.Environment.IsDevelopment())
{
	app.UseSwagger();
	app.UseSwaggerUI();
}

app.UseHttpsRedirection();
app.UseCors("React");
app.UseAuthentication();
app.UseRouting();
app.UseAuthorization();
app.MapControllers();
app.Run();
