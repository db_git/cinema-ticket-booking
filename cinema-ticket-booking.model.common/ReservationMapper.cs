using AutoMapper;
using cinema_ticket_booking.dal;
using cinema_ticket_booking.model.ReservationRest;
using cinema_ticket_booking.model.SeatRest;

namespace cinema_ticket_booking.model.common;

public sealed class ReservationMapper : Profile
{
	public ReservationMapper()
	{
		ReservationToReservationDto();
		CreateReservationDtoToReservation();
	}

	private void ReservationToReservationDto()
	{
		CreateMap<Reservation, ReservationDto>()
			.ForMember(x => x.Seats, opt
				=> opt.MapFrom((reservation, _) =>
				{
					var result = new List<SeatDto>();
					if (reservation.ReservationSeats != null)
					{
						result.AddRange(reservation.ReservationSeats
							.Select(seat => new SeatDto
							{
								Id = seat.SeatId,
								Number = seat.Seat.Number
							}));
					}

					return result;
				}));
	}

	private void CreateReservationDtoToReservation()
	{
		CreateMap<CreateReservationDto, Reservation>()
			.ForMember(r => r.ReservationSeats, opt
				=> opt.MapFrom((dto, _) =>
				{
					var result = new List<ReservationSeat>();
					if (dto.Seats != null)
					{
						result.AddRange(dto.Seats
							.Select(id => new ReservationSeat
							{
								SeatId = id
							}));
					}
					return result;
				}));
	}
}
