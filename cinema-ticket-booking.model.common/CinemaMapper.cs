using AutoMapper;
using cinema_ticket_booking.dal;
using cinema_ticket_booking.model.CinemaRest;

namespace cinema_ticket_booking.model.common;

public sealed class CinemaMapper : Profile
{
	public CinemaMapper()
	{
		CreateMap<Cinema, CinemaDto>();
		CreateMap<CreateCinemaDto, Cinema>();
		CreateMap<UpdateCinemaDto, Cinema>();
	}
}
