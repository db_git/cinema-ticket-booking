using AutoMapper;
using cinema_ticket_booking.dal.Identity;
using cinema_ticket_booking.model.UserRest;

namespace cinema_ticket_booking.model.common;

public sealed class UserMapper : Profile
{
	public UserMapper()
	{
		CreateMap<User, UserDto>()
			.ForMember(dto => dto.Roles, opt
				=> opt.MapFrom((user, _) =>
				{
					var result = new List<string>();
					if (user.UserRoles != null)
					{
						result.AddRange(user.UserRoles
							.Select(role => role.Role.Name)
						);
					}
					return result;
				}));
		CreateMap<UserRegisterDto, User>();
		CreateMap<UpdateUserDto, User>();
	}
}
