using AutoMapper;
using cinema_ticket_booking.dal;
using cinema_ticket_booking.model.GenreRest;

namespace cinema_ticket_booking.model.common;

public sealed class GenreMapper : Profile
{
	public GenreMapper()
	{
		CreateMap<Genre, GenreDto>();
		CreateMap<CreateGenreDto, Genre>();
		CreateMap<UpdateGenreDto, Genre>();
	}
}
