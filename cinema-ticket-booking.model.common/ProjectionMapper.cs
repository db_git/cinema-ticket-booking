using AutoMapper;
using cinema_ticket_booking.dal;
using cinema_ticket_booking.model.ProjectionRest;

namespace cinema_ticket_booking.model.common;

public sealed class ProjectionMapper : Profile
{
	public ProjectionMapper()
	{
		CreateMap<Projection, ProjectionDto>();
		CreateMap<CreateProjectionDto, Projection>();
		CreateMap<UpdateProjectionDto, Projection>();
	}
}
