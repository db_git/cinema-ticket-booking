using AutoMapper;
using cinema_ticket_booking.dal;
using cinema_ticket_booking.model.SeatRest;

namespace cinema_ticket_booking.model.common;

public sealed class SeatMapper : Profile
{
	public SeatMapper()
	{
		CreateMap<Seat, SeatDto>();
		CreateMap<CreateSeatDto, Seat>();
		CreateMap<UpdateSeatDto, Seat>().ReverseMap();
	}
}
