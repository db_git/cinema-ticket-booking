using AutoMapper;
using cinema_ticket_booking.dal;
using cinema_ticket_booking.model.RatingRest;

namespace cinema_ticket_booking.model.common;

public sealed class RatingMapper : Profile
{
	public RatingMapper()
	{
		CreateMap<Rating, RatingDto>();
		CreateMap<CreateRatingDto, Rating>();
		CreateMap<UpdateRatingDto, Rating>();
	}
}
