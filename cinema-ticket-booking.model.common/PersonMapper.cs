using AutoMapper;
using cinema_ticket_booking.dal;
using cinema_ticket_booking.model.PersonRest;

namespace cinema_ticket_booking.model.common;

public sealed class PersonMapper : Profile
{
	public PersonMapper()
	{
		CreateMap<Person, PersonDto>();
		CreateMap<CreatePersonDto, Person>();
		CreateMap<UpdatePersonDto, Person>();
	}
}
