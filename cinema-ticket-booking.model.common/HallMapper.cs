using AutoMapper;
using cinema_ticket_booking.dal;
using cinema_ticket_booking.model.HallRest;

namespace cinema_ticket_booking.model.common;

public sealed class HallMapper : Profile
{
	public HallMapper()
	{
		CreateMap<Hall, HallDto>();
		CreateMap<CreateHallDto, Hall>();
		CreateMap<UpdateHallDto, Hall>();
	}
}
