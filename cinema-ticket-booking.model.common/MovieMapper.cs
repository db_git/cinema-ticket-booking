using AutoMapper;
using cinema_ticket_booking.dal;
using cinema_ticket_booking.model.GenreRest;
using cinema_ticket_booking.model.MovieRest;
using cinema_ticket_booking.model.RatingRest;

namespace cinema_ticket_booking.model.common;

public sealed class MovieMapper : Profile
{
	public MovieMapper()
	{
		MovieToMovieDto();
		CreateMovieDtoToMovie();
	}

	private void MovieToMovieDto()
	{
		CreateMap<Movie, MovieDto>()
			.ForMember(x => x.Genres, opt
				=> opt.MapFrom((movie, _) =>
				{
					var result = new List<GenreDto>();
					if (movie.MovieGenres != null)
					{
						result.AddRange(movie.MovieGenres
							.Select(genre => new GenreDto
							{
								Id = genre.GenreId,
								Name = genre.Genre.Name
							}));
					}
					return result;
				}))
			.ForMember(x => x.Actors, opt
				=> opt.MapFrom((movie, _) =>
				{
					var result = new List<MoviePersonDto>();
					if (movie.MoviePeople != null)
					{
						result.AddRange(movie.MoviePeople.Where(x => x.Type == "Actor")
							.Select(person => new MoviePersonDto
							{
								Id = person.PersonId,
								Type = person.Type,
								FirstName = person.Person.FirstName,
								LastName = person.Person.LastName
							}));
					}
					return result;
				}))
			.ForMember(x => x.Directors, opt
				=> opt.MapFrom((movie, _) =>
				{
					var result = new List<MoviePersonDto>();
					if (movie.MoviePeople != null)
					{
						result.AddRange(movie.MoviePeople.Where(x => x.Type == "Director")
							.Select(person => new MoviePersonDto
							{
								Id = person.PersonId,
								Type = person.Type,
								FirstName = person.Person.FirstName,
								LastName = person.Person.LastName
							}));
					}
					return result;
				}))
			.ForMember(x => x.Ratings, opt
				=> opt.MapFrom((movie, _) =>
				{
					var result = new List<RatingDto>();
					if (movie.MovieRatings != null)
					{
						result.AddRange(movie.MovieRatings
							.Select(rating => new RatingDto
							{
								Id = rating.RatingId,
								Type = rating.Rating.Type
							}));
					}
					return result;
				}));
	}

	private void CreateMovieDtoToMovie()
	{
		CreateMap<CreateMovieDto, Movie>()
			.ForMember(m => m.MovieGenres, opt
				=> opt.MapFrom((dto, _) =>
				{
					var result = new List<MovieGenre>();
					if (dto.Genres != null)
					{
						result.AddRange(dto.Genres
							.Select(id => new MovieGenre
							{
								GenreId = id,
							}));
					}
					return result;
				}))
			.ForMember(m => m.MoviePeople, opt
				=> opt.MapFrom((dto, _) =>
				{
					var result = new List<MoviePerson>();
					if (dto.Actors != null)
					{
						result.AddRange(dto.Actors
							.Select(id => new MoviePerson
							{
								PersonId = id,
								Type = "Actor",
							}));
					}
					if (dto.Directors != null)
					{
						result.AddRange(dto.Directors
							.Select(id => new MoviePerson
							{
								PersonId = id,
								Type = "Director",
							}));
					}
					return result;
				}))
			.ForMember(m => m.MovieRatings, opt
				=> opt.MapFrom((dto, _) =>
				{
					var result = new List<MovieRating>();
					if (dto.Ratings != null)
					{
						result.AddRange(dto.Ratings
							.Select(id => new MovieRating
							{
								RatingId = id,
							}));
					}
					return result;
				}));
	}
}
