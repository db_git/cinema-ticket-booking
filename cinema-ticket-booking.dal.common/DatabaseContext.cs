using cinema_ticket_booking.dal.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace cinema_ticket_booking.dal.common;

public class DatabaseContext : IdentityDbContext<
	User, Role, Guid,
	UserClaim, UserRole, UserLogin,
	RoleClaim, UserToken>
{
	public DatabaseContext(DbContextOptions options) : base(options) {}

	protected override void OnModelCreating(ModelBuilder builder)
	{
		base.OnModelCreating(builder);
#region Hall
		builder.Entity<Hall>()
			.HasOne(h => h.Cinema)
			.WithMany(t => t.Halls);
#endregion
#region Projection
		builder.Entity<Projection>()
			.HasOne(p => p.Hall)
			.WithMany(h => h.Projections);
#endregion
#region Reservation
		builder.Entity<Reservation>()
			.HasOne(r => r.User)
			.WithMany(u => u.Reservations);
		builder.Entity<Reservation>()
			.HasOne(r => r.Projection)
			.WithMany(p => p.Reservations);
#endregion
#region Seat
		builder.Entity<Seat>()
			.HasOne(s => s.Hall)
			.WithMany(h => h.Seats);
#endregion
#region ReservationSeat
		builder.Entity<ReservationSeat>()
			.HasKey(rs => rs.Id);
		builder.Entity<ReservationSeat>()
			.HasOne(rs => rs.Reservation)
			.WithMany(r => r.ReservationSeats)
			.HasForeignKey(rs => rs.ReservationId);
		builder.Entity<ReservationSeat>()
			.HasOne(rs => rs.Seat)
			.WithMany(r => r.ReservationSeats)
			.HasForeignKey(rs => rs.SeatId);
#endregion
#region MovieGenre
		builder.Entity<MovieGenre>()
			.HasKey(mg => mg.Id);
		builder.Entity<MovieGenre>()
			.HasOne(mg => mg.Movie)
			.WithMany(m => m.MovieGenres)
			.HasForeignKey(mg => mg.MovieId);
		builder.Entity<MovieGenre>()
			.HasOne(mg => mg.Genre)
			.WithMany(g => g.MovieGenres)
			.HasForeignKey(mg => mg.GenreId);
#endregion
#region MoviePerson
		builder.Entity<MoviePerson>()
			.HasKey(mp => mp.Id);
		builder.Entity<MoviePerson>()
			.HasOne(mw => mw.Movie)
			.WithMany(m => m.MoviePeople)
			.HasForeignKey(mw => mw.MovieId);
		builder.Entity<MoviePerson>()
			.HasOne(mw => mw.Person)
			.WithMany(w => w.MoviePeople)
			.HasForeignKey(mw => mw.PersonId);
#endregion
#region MovieRating
		builder.Entity<MovieRating>()
			.HasKey(mr => mr.Id);
		builder.Entity<MovieRating>()
			.HasOne(mr => mr.Movie)
			.WithMany(m => m.MovieRatings)
			.HasForeignKey(mr => mr.MovieId);
		builder.Entity<MovieRating>()
			.HasOne(mr => mr.Rating)
			.WithMany(r => r.MovieRatings)
			.HasForeignKey(mr => mr.RatingId);
#endregion
#region Identity
		builder.Entity<User>(b =>
		{
			b.HasMany(e => e.Claims)
				.WithOne(e => e.User)
				.HasForeignKey(uc => uc.UserId)
				.IsRequired();

			b.HasMany(e => e.Logins)
				.WithOne(e => e.User)
				.HasForeignKey(ul => ul.UserId)
				.IsRequired();

			b.HasMany(e => e.Tokens)
				.WithOne(e => e.User)
				.HasForeignKey(ut => ut.UserId)
				.IsRequired();

			b.HasMany(e => e.UserRoles)
				.WithOne(e => e.User)
				.HasForeignKey(ur => ur.UserId)
				.IsRequired();
		});

		builder.Entity<Role>(b =>
		{
			b.HasMany(e => e.UserRoles)
				.WithOne(e => e.Role)
				.HasForeignKey(ur => ur.RoleId)
				.IsRequired();

			b.HasMany(e => e.RoleClaims)
				.WithOne(e => e.Role)
				.HasForeignKey(rc => rc.RoleId)
				.IsRequired();
		});
#endregion
	}

	public DbSet<Cinema> Cinemas { get; set; }
	public DbSet<Genre> Genres { get; set; }
	public DbSet<Hall> Halls { get; set; }
	public DbSet<Movie> Movies { get; set; }
	public DbSet<MovieGenre> MovieGenres { get; set; }
	public DbSet<MoviePerson> MoviePeople { get; set; }
	public DbSet<MovieRating> MovieRatings  { get; set; }
	public DbSet<Person> People { get; set; }
	public DbSet<Projection> Projections { get; set; }
	public DbSet<Rating> Ratings { get; set; }
	public DbSet<Reservation> Reservations { get; set; }
	public DbSet<Seat> Seats { get; set; }
	public DbSet<ReservationSeat> ReservationSeats { get; set; }
}
