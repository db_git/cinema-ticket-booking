using System.Linq.Expressions;
using cinema_ticket_booking.dal.Identity;
using cinema_ticket_booking.model.UserRest;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Configuration;

namespace cinema_ticket_booking.service.common;

public interface IUserManagerService: IGenericService<User, UpdateUserDto>
{
	public new Task<User?> Get(Expression<Func<User, bool>> expression);
	public new Task<ICollection<User>?> GetAll(string? search, int? sort);
	public Task<IdentityResult> CreateAsync(User user, string password);
	public Task<IdentityResult> DeleteAsync(Guid id);

	public Task<bool> ValidateUser(string username, string password);
	Task<string> CreateToken(IConfiguration configuration, string username);
}
