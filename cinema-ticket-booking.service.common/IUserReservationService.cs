using System.Linq.Expressions;
using cinema_ticket_booking.dal;
using cinema_ticket_booking.model.ReservationRest;

namespace cinema_ticket_booking.service.common;

public interface IUserReservationService
{
	public Task<ICollection<Reservation>?> UserGetAll(string token, string? search, int? sort);
	public Task<Reservation?> UserGet(string token, Expression<Func<Reservation, bool>> expression);
	public Task UserCreate(string token, Reservation reservation);
	public Task UserUpdate(string token, Guid id, UpdateReservationDto reservationDto);
	public Task UserDelete(string token, Guid id);
}
