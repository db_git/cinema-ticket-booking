using System.Linq.Expressions;

namespace cinema_ticket_booking.service.common;

public interface IGenericService<TEntity, in TUpdate>
	where TEntity : class
	where TUpdate : class
{
	public Task<ICollection<TEntity>?> GetAll(string? search, int? sort);
	public Task<TEntity?> Get(Expression<Func<TEntity, bool>> expression);
	public Task Create(TEntity entity);
	public Task Update(Guid id, TUpdate entity);
	public Task Delete(Guid id);
}
