using System.Linq.Expressions;

namespace cinema_ticket_booking.service.common;

public class ExpressionParameterReplacer<T> : ExpressionVisitor
{
	private IDictionary<ParameterExpression, ParameterExpression> ParameterReplacements { get; }

	public ExpressionParameterReplacer(
		IList<ParameterExpression> from,
		IList<ParameterExpression> to
	)
	{
		ParameterReplacements = new Dictionary<ParameterExpression, ParameterExpression>();

		for (var i = 0; i != from.Count && i != to.Count; i++)
		{
			ParameterReplacements.Add(from[i], to[i]);
		}
	}

	protected override Expression VisitParameter(ParameterExpression node)
	{
		if (ParameterReplacements.TryGetValue(node, out var replacement))
		{
			node = replacement;
		}
		return base.VisitParameter(node);
	}

	public static Expression<Func<T, bool>> OrElse(
		Expression<Func<T, bool>> left,
		Expression<Func<T, bool>> right
	)
	{
		return Expression.Lambda<Func<T, bool>>(Expression.OrElse(
			left.Body,
			new ExpressionParameterReplacer<T>(right.Parameters, left.Parameters).Visit(right.Body)),
			left.Parameters
		);
	}

	public static Expression<Func<T, bool>> AndAlso(
		Expression<Func<T, bool>> left,
		Expression<Func<T, bool>> right
	)
	{
		return Expression.Lambda<Func<T, bool>>(Expression.AndAlso(
			left.Body,
			new ExpressionParameterReplacer<T>(right.Parameters, left.Parameters).Visit(right.Body)),
			left.Parameters
		);
	}
}
