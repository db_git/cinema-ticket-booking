using System.Linq.Expressions;
using Microsoft.EntityFrameworkCore.Query;

namespace cinema_ticket_booking.repository.common;

public interface IGenericRepository<T> where T : class
{
	public Task<ICollection<T>?> GetAll(
		Expression<Func<T, bool>>? expression = null,
		Func<IQueryable<T>, IOrderedQueryable<T>>? orderBy = null,
		Func<IQueryable<T>, IIncludableQueryable<T, object>>? includes = null
	);

	public Task<T?> Get(
		Expression<Func<T, bool>> expression,
		Func<IQueryable<T>, IIncludableQueryable<T, object>>? includes = null
	);

	public Task Create(T entity);
	public void Update(T entity);
	public void Delete(T entity);
}
