using cinema_ticket_booking.dal;
using cinema_ticket_booking.dal.Identity;

namespace cinema_ticket_booking.repository.common;

public interface IRepositoryWork : IDisposable
{
	IGenericRepository<Cinema> CinemaRepository { get; }
	IGenericRepository<Genre> GenreRepository { get; }
	IGenericRepository<Hall> HallRepository { get; }
	IGenericRepository<Movie> MovieRepository { get; }
	IGenericRepository<Person> PersonRepository { get; }
	IGenericRepository<Projection> ProjectionRepository { get; }
	IGenericRepository<Rating> RatingRepository { get; }
	IGenericRepository<Reservation> ReservationRepository { get; }
	IGenericRepository<Seat> SeatRepository { get; }
	IGenericRepository<User> UserRepository { get; }

	Task Save();
}
