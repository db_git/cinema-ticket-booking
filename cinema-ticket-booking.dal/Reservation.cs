using System.ComponentModel.DataAnnotations.Schema;
using cinema_ticket_booking.dal.Identity;

namespace cinema_ticket_booking.dal;

public class Reservation
{
	public Guid Id { get; set; }

	[ForeignKey(nameof(User))]
	public Guid UserId { get; set; }
	public User User { get; set; }
	[ForeignKey(nameof(Projection))]
	public Guid ProjectionId { get; set; }
	public Projection Projection { get; set; }

	public ICollection<ReservationSeat> ReservationSeats { get; set; }
}
