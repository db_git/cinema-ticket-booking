using System.ComponentModel.DataAnnotations.Schema;

namespace cinema_ticket_booking.dal;

public class MoviePerson
{
	public Guid Id { get; set; }
	public string Type { get; set; }

	[ForeignKey(nameof(Movie))]
	public Guid MovieId { get; set; }
	public Movie Movie { get; set; }
	[ForeignKey(nameof(Person))]
	public Guid PersonId { get; set; }
	public Person Person { get; set; }
}
