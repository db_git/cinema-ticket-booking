namespace cinema_ticket_booking.dal;

public class Genre
{
	public Guid Id { get; set; }

	public string Name { get; set; }

	public ICollection<MovieGenre> MovieGenres { get; set; }
}
