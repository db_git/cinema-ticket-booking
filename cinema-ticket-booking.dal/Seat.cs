using System.ComponentModel.DataAnnotations.Schema;

namespace cinema_ticket_booking.dal;

public class Seat
{
	public Guid Id { get; set; }
	public int Number { get; set; }

	[ForeignKey(nameof(Hall))]
	public Guid HallId { get; set; }
	public Hall Hall { get; set; }

	public ICollection<ReservationSeat> ReservationSeats { get; set; }
}
