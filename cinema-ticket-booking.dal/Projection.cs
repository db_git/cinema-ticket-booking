using System.ComponentModel.DataAnnotations.Schema;

namespace cinema_ticket_booking.dal;

public class Projection
{
	public Guid Id { get; set; }

	[ForeignKey(nameof(Movie))]
	public Guid MovieId { get; set; }
	public Movie Movie { get; set; }
	[ForeignKey(nameof(Hall))]
	public Guid HallId { get; set; }
	public Hall Hall { get; set; }

	public DateTime Time { get; set; }

	public ICollection<Reservation> Reservations { get; set; }
}
