using Microsoft.AspNetCore.Identity;

namespace cinema_ticket_booking.dal.Identity;

public class User : IdentityUser<Guid>
{
	public string FirstName { get; set; }
	public string LastName { get; set; }

	public ICollection<Reservation> Reservations { get; set; }

	public virtual ICollection<UserClaim> Claims { get; set; }
	public virtual ICollection<UserLogin> Logins { get; set; }
	public virtual ICollection<UserToken> Tokens { get; set; }
	public virtual ICollection<UserRole> UserRoles { get; set; }
}
