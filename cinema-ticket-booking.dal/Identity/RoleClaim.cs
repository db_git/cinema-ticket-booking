using Microsoft.AspNetCore.Identity;

namespace cinema_ticket_booking.dal.Identity;

public class RoleClaim : IdentityRoleClaim<Guid>
{
	public virtual Role Role { get; set; }
}
