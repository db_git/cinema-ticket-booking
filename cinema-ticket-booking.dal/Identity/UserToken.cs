using Microsoft.AspNetCore.Identity;

namespace cinema_ticket_booking.dal.Identity;

public class UserToken : IdentityUserToken<Guid>
{
	public virtual User User { get; set; }
}
