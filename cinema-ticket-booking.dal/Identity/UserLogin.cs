using Microsoft.AspNetCore.Identity;

namespace cinema_ticket_booking.dal.Identity;

public class UserLogin : IdentityUserLogin<Guid>
{
	public virtual User User { get; set; }
}
