using Microsoft.AspNetCore.Identity;

namespace cinema_ticket_booking.dal.Identity;

public class UserRole : IdentityUserRole<Guid>
{
	public virtual User User { get; set; }
	public virtual Role Role { get; set; }
}
