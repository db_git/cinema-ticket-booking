using Microsoft.AspNetCore.Identity;

namespace cinema_ticket_booking.dal.Identity;

public class Role : IdentityRole<Guid>
{
	public virtual ICollection<UserRole> UserRoles { get; set; }
	public virtual ICollection<RoleClaim> RoleClaims { get; set; }
}
