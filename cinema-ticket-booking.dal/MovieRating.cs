using System.ComponentModel.DataAnnotations.Schema;

namespace cinema_ticket_booking.dal;

public sealed class MovieRating
{
	public Guid Id { get; set; }

	[ForeignKey(nameof(Movie))]
	public Guid MovieId { get; set; }
	public Movie Movie { get; set; }
	[ForeignKey(nameof(Rating))]
	public Guid RatingId { get; set; }
	public Rating Rating { get; set; }
}
