using System.ComponentModel.DataAnnotations.Schema;

namespace cinema_ticket_booking.dal;

public class MovieGenre
{
	public Guid Id { get; set; }

	[ForeignKey(nameof(Movie))]
	public Guid MovieId { get; set; }
	public Movie Movie { get; set; }
	[ForeignKey(nameof(Genre))]
	public Guid GenreId { get; set; }
	public Genre Genre { get; set; }
}
