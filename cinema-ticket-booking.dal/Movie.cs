namespace cinema_ticket_booking.dal;

public class Movie
{
	public Guid Id { get; set; }

	public string Title { get; set; }
	public int Duration { get; set; }
	public int Year { get; set; }
	public string Tagline { get; set; }
	public string Summary { get; set; }
	public string PosterUrl { get; set; }
	public string BackdropUrl { get; set; }
	public string TrailerUrl { get; set; }

	public ICollection<MovieGenre> MovieGenres { get; set; }
	public ICollection<MoviePerson> MoviePeople { get; set; }
	public ICollection<MovieRating> MovieRatings { get; set; }
}
