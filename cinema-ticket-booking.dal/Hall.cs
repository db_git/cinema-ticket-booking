using System.ComponentModel.DataAnnotations.Schema;

namespace cinema_ticket_booking.dal;

public class Hall
{
	public Guid Id { get; set; }

	[ForeignKey(nameof(Cinema))]
	public Guid CinemaId { get; set; }
	public Cinema Cinema { get; set; }

	public string Name { get; set; }

	public ICollection<Projection> Projections { get; set; }
	public ICollection<Seat> Seats { get; set; }
}
