namespace cinema_ticket_booking.dal;

public class Cinema
{
	public Guid Id { get; set; }

	public string Name { get; set; }
	public string City { get; set; }
	public string Street { get; set; }
	public double TicketPrice { get; set; }

	public ICollection<Hall> Halls { get; set; }
}
