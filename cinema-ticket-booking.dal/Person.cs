namespace cinema_ticket_booking.dal;

public class Person
{
	public Guid Id { get; set; }

	public string FirstName { get; set; }
	public string LastName { get; set; }

	public ICollection<MoviePerson> MoviePeople { get; set; }
}
