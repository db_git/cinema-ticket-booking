namespace cinema_ticket_booking.dal;

public class Rating
{
	public Guid Id { get; set; }

	public string Type { get; set; }

	public ICollection<MovieRating> MovieRatings { get; set; }
}
