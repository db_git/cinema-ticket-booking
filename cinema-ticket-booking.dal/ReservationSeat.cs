using System.ComponentModel.DataAnnotations.Schema;

namespace cinema_ticket_booking.dal;

public class ReservationSeat
{
	public Guid Id { get; set; }
	[ForeignKey(nameof(Reservation))]
	public Guid ReservationId { get; set; }
	public Reservation Reservation { get; set; }
	[ForeignKey(nameof(Seat))]
	public Guid SeatId { get; set; }
	public Seat Seat { get; set; }
}
