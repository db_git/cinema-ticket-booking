using System.ComponentModel.DataAnnotations;

namespace cinema_ticket_booking.model.UserRest;

public class UserRegisterDto : UserLoginDto
{
	[Required(ErrorMessage = "This field is required.")]
	public string FirstName { get; set; }

	[Required(ErrorMessage = "This field is required.")]
	public string LastName { get; set; }
}
