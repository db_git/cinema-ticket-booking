using System.ComponentModel.DataAnnotations;

namespace cinema_ticket_booking.model.UserRest;

public class UserLoginDto
{
	[EmailAddress(ErrorMessage = "Please enter a valid e-mail address.")]
	[Required(ErrorMessage = "This field is required.")]
	public string Email { get; set; }

	[Required(ErrorMessage = "This field is required.")]
	public string Password { get; set; }
}
