using cinema_ticket_booking.model.ReservationRest;

namespace cinema_ticket_booking.model.UserRest;

public sealed class UserDto
{
	public Guid Id { get; set; }
	public string FirstName { get; set; }
	public string LastName { get; set; }
	public string Email { get; set; }
	public ICollection<ReservationDto> Reservations { get; set; }
	public ICollection<string> Roles { get; set; }
}
