namespace cinema_ticket_booking.model.GenreRest;

public sealed class GenreDto : CreateGenreDto
{
	public Guid Id { get; set; }
}
