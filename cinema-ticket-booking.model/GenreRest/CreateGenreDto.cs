using System.ComponentModel.DataAnnotations;
using cinema_ticket_booking.model.Validations;

namespace cinema_ticket_booking.model.GenreRest;

public class CreateGenreDto
{
	[NoWhitespace]
	[ForbidNumbers]
	[FirstLetterUppercase]
	[MinLength(3, ErrorMessage = "Minimum length is {1} characters.")]
	[MaxLength(25, ErrorMessage = "Maximum length must not exceed {1} characters.")]
	[Required(ErrorMessage = "This field is required.")]
	public string Name { get; set; }
}
