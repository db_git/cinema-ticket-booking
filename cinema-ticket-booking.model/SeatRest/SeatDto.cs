using cinema_ticket_booking.model.HallRest;

namespace cinema_ticket_booking.model.SeatRest;

public sealed class SeatDto
{
	public Guid Id { get; set; }
	public int Number { get; set; }

	public HallDto Hall { get; set; }
}
