using System.ComponentModel.DataAnnotations;
using cinema_ticket_booking.model.Validations;

namespace cinema_ticket_booking.model.SeatRest;

public class CreateSeatDto
{
	[Range(1, 500, ErrorMessage = "Value must be between {1} and {2}.")]
	[Required(ErrorMessage = "This field is required.")]
	public int Number { get; set; }

	[Guid(ErrorMessage = "Please select a hall.")]
	public Guid HallId { get; set; }
}
