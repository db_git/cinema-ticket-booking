using System.ComponentModel.DataAnnotations;
using cinema_ticket_booking.model.Validations;

namespace cinema_ticket_booking.model.PersonRest;

public class CreatePersonDto
{
	[FirstLetterUppercase]
	[MinLength(1, ErrorMessage = "Minimum length is {1} character.")]
	[MaxLength(50, ErrorMessage = "Maximum length must not exceed {1} characters.")]
	[Required(ErrorMessage = "This field is required.")]
	public string FirstName { get; set; }

	[MinLength(1, ErrorMessage = "Minimum length is {1} character.")]
	[MaxLength(50, ErrorMessage = "Maximum length must not exceed {1} characters.")]
	[Required(ErrorMessage = "This field is required.")]
	public string LastName { get; set; }
}
