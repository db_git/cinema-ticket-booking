namespace cinema_ticket_booking.model.PersonRest;

public class PersonDto : CreatePersonDto
{
	public Guid Id { get; set; }
}
