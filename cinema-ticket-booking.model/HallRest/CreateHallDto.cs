using System.ComponentModel.DataAnnotations;
using cinema_ticket_booking.model.Validations;

namespace cinema_ticket_booking.model.HallRest;

public class CreateHallDto
{
	[Guid(ErrorMessage = "Please select a cinema.")]
	public Guid CinemaId { get; set; }

	[MinLength(2, ErrorMessage = "Minimum length is {1} characters.")]
	[MaxLength(50, ErrorMessage = "Maximum length must not exceed {1} characters.")]
	[Required(ErrorMessage = "This field is required.")]
	public string Name { get; set; }
}
