using cinema_ticket_booking.model.CinemaRest;
using cinema_ticket_booking.model.ProjectionRest;
using cinema_ticket_booking.model.SeatRest;

namespace cinema_ticket_booking.model.HallRest;

public sealed class HallDto
{
	public Guid Id { get; set; }
	public CinemaDto Cinema { get; set; }
	public string Name { get; set; }

	public ICollection<ProjectionDto> Projections { get; set; }
	public ICollection<SeatDto> Seats { get; set; }
}
