namespace cinema_ticket_booking.model.RatingRest;

public sealed class RatingDto : CreateRatingDto
{
	public Guid Id { get; set; }
}
