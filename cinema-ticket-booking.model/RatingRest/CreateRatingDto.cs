using System.ComponentModel.DataAnnotations;

namespace cinema_ticket_booking.model.RatingRest;

public class CreateRatingDto
{
	[Required(ErrorMessage = "This field is required.")]
	public string Type { get; set; }
}
