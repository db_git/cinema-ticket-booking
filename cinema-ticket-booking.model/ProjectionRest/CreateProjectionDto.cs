using System.ComponentModel.DataAnnotations;
using cinema_ticket_booking.model.Validations;

namespace cinema_ticket_booking.model.ProjectionRest;

public class CreateProjectionDto
{
	[Guid(ErrorMessage = "Please select a movie.")]
	public Guid MovieId { get; set; }

	[Guid(ErrorMessage = "Please select a hall.")]
	public Guid HallId { get; set; }

	[Required]
	public DateTime Time { get; set; }
}
