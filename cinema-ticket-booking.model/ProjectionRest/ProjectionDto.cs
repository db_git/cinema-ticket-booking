using cinema_ticket_booking.model.HallRest;
using cinema_ticket_booking.model.MovieRest;
using cinema_ticket_booking.model.ReservationRest;

namespace cinema_ticket_booking.model.ProjectionRest;

public sealed class ProjectionDto
{
	public Guid Id { get; set; }
	public MovieDto Movie { get; set; }
	public HallDto Hall { get; set; }
	public DateTime Time { get; set; }

	public ICollection<ReservationDto> Reservations { get; set; }
}
