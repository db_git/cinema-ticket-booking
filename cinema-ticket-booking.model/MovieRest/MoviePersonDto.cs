using cinema_ticket_booking.model.PersonRest;

namespace cinema_ticket_booking.model.MovieRest;

public sealed class MoviePersonDto : PersonDto
{
	public string Type { get; set; }
}
