using System.ComponentModel.DataAnnotations;
using cinema_ticket_booking.model.Validations;

namespace cinema_ticket_booking.model.MovieRest;

public class CreateMovieDto
{
	[FirstCharacterUppercaseOrNumber]
	[Required(ErrorMessage = "This field is required.")]
	public string Title { get; set; }

	[Range(30, 360, ErrorMessage = "Value must be between {1} and {2}.")]
	[Required(ErrorMessage = "This field is required.")]
	public int Duration { get; set; }

	[Range(1899, 2099, ErrorMessage = "Value must be between {1} and {2}.")]
	[Required(ErrorMessage = "This field is required.")]
	public int Year { get; set; }

	[FirstCharacterUppercaseOrNumber]
	[MinLength(10, ErrorMessage = "Minimum length is {1} characters.")]
	[MaxLength(255, ErrorMessage = "Maximum length must not exceed {1} characters.")]
	[Required(ErrorMessage = "This field is required.")]
	public string Tagline { get; set; }

	[FirstCharacterUppercaseOrNumber]
	[MinLength(50, ErrorMessage = "Minimum length is {1} characters.")]
	[Required(ErrorMessage = "This field is required.")]
	public string Summary { get; set; }

	[HttpsUrl]
	[MinLength(20, ErrorMessage = "Minimum length is {1} characters.")]
	[MaxLength(250, ErrorMessage = "Maximum length must not exceed {1} characters.")]
	[Required(ErrorMessage = "This field is required.")]
	public string PosterUrl { get; set; }

	[HttpsUrl]
	[MinLength(20, ErrorMessage = "Minimum length is {1} characters.")]
	[MaxLength(250, ErrorMessage = "Maximum length must not exceed {1} characters.")]
	[Required(ErrorMessage = "This field is required.")]
	public string BackdropUrl { get; set; }

	[HttpsUrl]
	[MinLength(20, ErrorMessage = "Minimum length is {1} characters.")]
	[MaxLength(250, ErrorMessage = "Maximum length must not exceed {1} characters.")]
	[Required(ErrorMessage = "This field is required.")]
	public string TrailerUrl { get; set; }

	[Required(ErrorMessage = "Please select at least one genre.")]
	public ICollection<Guid> Genres { get; set; }

	[Required(ErrorMessage = "Please select at least one actor.")]
	public ICollection<Guid> Actors { get; set; }

	[Required(ErrorMessage = "Please select at least one director.")]
	public ICollection<Guid> Directors { get; set; }

	[Required(ErrorMessage = "Please select a rating.")]
	public ICollection<Guid> Ratings { get; set; }
}
