using cinema_ticket_booking.model.GenreRest;
using cinema_ticket_booking.model.RatingRest;

namespace cinema_ticket_booking.model.MovieRest;

public sealed class MovieDto
{
	public Guid Id { get; set; }
	public string Title { get; set; }
	public int Duration { get; set; }
	public int Year { get; set; }
	public string Tagline { get; set; }
	public string Summary { get; set; }
	public string PosterUrl { get; set; }
	public string BackdropUrl { get; set; }
	public string TrailerUrl { get; set; }

	public ICollection<GenreDto> Genres { get; set; }
	public ICollection<MoviePersonDto> Actors { get; set; }
	public ICollection<MoviePersonDto> Directors { get; set; }
	public ICollection<RatingDto> Ratings { get; set; }
}
