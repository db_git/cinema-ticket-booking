using System.ComponentModel.DataAnnotations;

namespace cinema_ticket_booking.model.Validations;

public class FirstCharacterUppercaseOrNumberAttribute : ValidationAttribute
{
	protected override ValidationResult? IsValid(object? value, ValidationContext validationContext)
	{
		if (value == null || string.IsNullOrEmpty(value.ToString()))
			return ValidationResult.Success;

		var firstLetter = value.ToString()![0];

		return firstLetter == char.ToUpper(firstLetter) || char.IsDigit(firstLetter)
			? ValidationResult.Success
			: new ValidationResult("First character must be an uppercase or a number.");
	}
}
