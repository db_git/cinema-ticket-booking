using System.ComponentModel.DataAnnotations;

namespace cinema_ticket_booking.model.Validations;

public class FirstLetterUppercaseAttribute : ValidationAttribute
{
	protected override ValidationResult? IsValid(object? value, ValidationContext validationContext)
	{
		if (value == null || string.IsNullOrEmpty(value.ToString()))
			return ValidationResult.Success;

		var firstLetter = value.ToString()![0].ToString();

		return firstLetter == firstLetter.ToUpper()
			? ValidationResult.Success
			: new ValidationResult("First letter must be uppercase.");
	}
}
