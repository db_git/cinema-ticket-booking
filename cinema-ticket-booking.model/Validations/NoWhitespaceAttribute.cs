using System.ComponentModel.DataAnnotations;

namespace cinema_ticket_booking.model.Validations;

public class NoWhitespaceAttribute : ValidationAttribute
{
	protected override ValidationResult? IsValid(object? value, ValidationContext validationContext)
	{
		if (value == null || string.IsNullOrEmpty(value.ToString()))
			return ValidationResult.Success;

		return value.ToString()!.Any(char.IsWhiteSpace)
			? new ValidationResult("Whitespace is not allowed.")
			: ValidationResult.Success;
	}
}
