using System.ComponentModel.DataAnnotations;

namespace cinema_ticket_booking.model.Validations;

public class HttpsUrlAttribute : ValidationAttribute
{
	protected override ValidationResult? IsValid(object? value, ValidationContext validationContext)
	{
		if (value == null || string.IsNullOrEmpty(value.ToString()))
			return ValidationResult.Success;

		var url = value.ToString();

		if (Uri.TryCreate(url, UriKind.Absolute, out var validatedUri)
		    && validatedUri.Scheme == Uri.UriSchemeHttps)
			return ValidationResult.Success;

		return new ValidationResult("Not a valid HTTPS URL.");
	}
}
