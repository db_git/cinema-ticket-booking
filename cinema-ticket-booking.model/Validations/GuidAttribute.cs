using System.ComponentModel.DataAnnotations;

namespace cinema_ticket_booking.model.Validations;

public class GuidAttribute : ValidationAttribute
{
	private const string DefaultMessage = "Value is not a valid GUID.";

	protected override ValidationResult? IsValid(object? value, ValidationContext validationContext)
	{
		var returnMessage = ErrorMessage ?? DefaultMessage;

		if (value == null || string.IsNullOrEmpty(value.ToString()))
			return new ValidationResult(returnMessage);

		if (!Guid.TryParse(value.ToString(), out var guid))
			return new ValidationResult(returnMessage);

		return guid != new Guid()
			? ValidationResult.Success
			: new ValidationResult(returnMessage);
	}
}
