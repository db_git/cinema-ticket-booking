using cinema_ticket_booking.model.HallRest;

namespace cinema_ticket_booking.model.CinemaRest;

public sealed class CinemaDto : CreateCinemaDto
{
	public Guid Id { get; set; }
	public ICollection<HallDto> Halls { get; set; }
}
