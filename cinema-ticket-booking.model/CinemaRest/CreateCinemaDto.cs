using System.ComponentModel.DataAnnotations;
using cinema_ticket_booking.model.Validations;

namespace cinema_ticket_booking.model.CinemaRest;

public class CreateCinemaDto
{
	[FirstLetterUppercase]
	[MinLength(5, ErrorMessage = "Minimum length is {1} characters.")]
	[MaxLength(50, ErrorMessage = "Maximum length must not exceed {1} characters.")]
	[Required(ErrorMessage = "This field is required.")]
	public string Name { get; set; }

	[ForbidNumbers]
	[FirstLetterUppercase]
	[MinLength(2, ErrorMessage = "Minimum length is {1} characters.")]
	[MaxLength(50, ErrorMessage = "Maximum length must not exceed {1} characters.")]
	[Required(ErrorMessage = "This field is required.")]
	public string City { get; set; }

	[FirstLetterUppercase]
	[MinLength(5, ErrorMessage = "Minimum length is {1} characters.")]
	[MaxLength(100, ErrorMessage = "Maximum length must not exceed {1} characters.")]
	[Required(ErrorMessage = "This field is required.")]
	public string Street { get; set; }

	[DataType(DataType.Currency, ErrorMessage = "Invalid data type.")]
	[Range(1.0, 499.99, ErrorMessage = "Value must be between {1} and {2}.")]
	[Required(ErrorMessage = "This field is required.")]
	public decimal TicketPrice { get; set; }
}
