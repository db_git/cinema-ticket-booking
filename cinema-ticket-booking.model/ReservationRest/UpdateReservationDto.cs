using System.ComponentModel.DataAnnotations;

namespace cinema_ticket_booking.model.ReservationRest;

public class UpdateReservationDto
{
	[Required]
	public Guid ProjectionId { get; set; }

	[Required(ErrorMessage = "Please select at least one seat.")]
	public ICollection<Guid> Seats { get; set; }
}
