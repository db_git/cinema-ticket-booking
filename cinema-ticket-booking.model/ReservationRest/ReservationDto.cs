using cinema_ticket_booking.model.ProjectionRest;
using cinema_ticket_booking.model.SeatRest;
using cinema_ticket_booking.model.UserRest;

namespace cinema_ticket_booking.model.ReservationRest;

public sealed class ReservationDto
{
	public Guid Id { get; set; }
	public UserDto User { get; set; }
	public ProjectionDto Projection { get; set; }

	public ICollection<SeatDto> Seats { get; set; }
}
