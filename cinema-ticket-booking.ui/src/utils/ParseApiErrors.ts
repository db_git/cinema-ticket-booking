import type ResponseError from "./type/ResponseError";

const ParseApiErrors = (errors: ResponseError[], response: never): void => {
	if (typeof response === "undefined") return;

	const entries = Object.entries(response) as unknown as [
		string,
		Array<{
			code: string;
			description: string;
		}>
	] &
		[string[], string[]];

	if (Array.isArray(response)) {
		for (const errorMessage of entries) {
			if (
				Array.isArray(errorMessage) &&
				typeof errorMessage === "object"
			) {
				errors.push({
					field: errorMessage[1].code,
					message: errorMessage[1].description
				});
			}
		}
	} else {
		for (const error of entries) {
			for (const errorMessage of error[1]) {
				errors.push({ field: error[0], message: errorMessage });
			}
		}
	}
};

export default ParseApiErrors;
