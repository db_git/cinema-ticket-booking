interface IRegistrationStore {
	readonly loginEndpoint: string;
	readonly registerEndpoint: string;
}

export default IRegistrationStore;
