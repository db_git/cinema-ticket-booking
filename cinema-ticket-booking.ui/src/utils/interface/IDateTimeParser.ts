interface IDateTimeParser {
	addHours: (hours: number) => void;

	addMinutes: (minutes: number) => void;

	addSeconds: (seconds: number) => void;

	getDate: () => string;

	getIsoDate: () => string;
}

export default IDateTimeParser;
