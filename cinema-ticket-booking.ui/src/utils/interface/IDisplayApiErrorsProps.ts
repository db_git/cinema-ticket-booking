import type ResponseError from "../type/ResponseError";

interface IDisplayApiErrorsProps {
	errors: ResponseError[];
	filter: string;
}

export default IDisplayApiErrorsProps;
