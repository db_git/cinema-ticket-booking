import type ApiRequest from "../enum/ApiRequest";
import type HttpMethod from "../enum/HttpMethod";
import type ApiErrors from "../type/ApiErrors";
import type ApiResponse from "../type/ApiResponse";
import type ApiStatus from "../type/ApiStatus";
import type Request from "../type/Request";
import type ResponseError from "../type/ResponseError";
import type IAuthStore from "./IAuthStore";

interface IApiStore<T> {
	readonly authStore: IAuthStore;

	create(form: FormData): Promise<void>;

	delete(id: string): Promise<void>;

	readonly endpoint: string;

	get(id?: string): Promise<void>;

	get getErrors(): ApiErrors;

	get getRecords(): number;

	get getRequest(): Request;

	get getResponse(): ApiResponse<T>;

	get getStatus(): ApiStatus;

	setErrors(type: HttpMethod, error: ResponseError[]): void;

	setRecords(records: number): void;

	setRequest(type: ApiRequest, value: number | string | undefined): void;

	setResponse(response: ApiResponse<T>): void;

	setStatus(type: HttpMethod, status: number): void;

	update(id: string, form: FormData): Promise<void>;
}

export default IApiStore;
