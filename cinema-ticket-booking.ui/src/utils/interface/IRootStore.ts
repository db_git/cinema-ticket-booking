import type UserReservationStore from "../../stores/UserReservationStore";
import type ApiStore from "../../stores/common/ApiStore";
import type ApiStoreGet from "../../stores/common/ApiStoreGet";
import type Cinema from "../type/Cinema";
import type Genre from "../type/Genre";
import type Hall from "../type/Hall";
import type Movie from "../type/Movie";
import type Person from "../type/Person";
import type Projection from "../type/Projection";
import type Rating from "../type/Rating";
import type Reservation from "../type/Reservation";
import type Seat from "../type/Seat";
import type User from "../type/User";
import type IAuthStore from "./IAuthStore";

interface IRootStore {
	authStore: IAuthStore;
	cinemaStore: ApiStore<Cinema>;
	genreStore: ApiStore<Genre>;
	hallStore: ApiStore<Hall>;
	movieCarouselStore: ApiStoreGet<Movie>;
	movieGetStore: ApiStoreGet<Movie>;
	movieStore: ApiStore<Movie>;
	personStore: ApiStore<Person>;
	projectionStore: ApiStore<Projection>;
	ratingStore: ApiStore<Rating>;
	reservationStore: ApiStore<Reservation>;
	seatStore: ApiStore<Seat>;
	userReservationStore: UserReservationStore;
	userStore: ApiStore<User>;
}

export default IRootStore;
