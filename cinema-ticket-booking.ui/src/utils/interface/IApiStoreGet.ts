import type ApiRequest from "../enum/ApiRequest";
import type ApiResponse from "../type/ApiResponse";
import type Request from "../type/Request";
import type ResponseError from "../type/ResponseError";

interface IApiStoreGet<T> {
	readonly endpoint: string;

	get(id?: string): Promise<void>;

	get getErrors(): ResponseError[];

	get getRequest(): Request;

	get getResponse(): ApiResponse<T>;

	get getStatus(): number;

	setErrors(error: ResponseError[]): void;

	setRequest(type: ApiRequest, value: number | string | undefined): void;

	setResponse(response: ApiResponse<T>): void;

	setStatus(status: number): void;
}

export default IApiStoreGet;
