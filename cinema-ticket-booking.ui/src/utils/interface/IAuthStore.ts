import type { FormEvent } from "react";
import type ResponseError from "../type/ResponseError";
import type IRegistrationStore from "./IRegistrationStore";

interface IAuthStore {
	readonly api: IRegistrationStore;
	errors: ResponseError[];

	get getEmail(): string;

	get getErrors(): ResponseError[];

	get getHeader(): { Authorization: string };

	get getStatus(): number;

	get getToken(): string | null;

	get getUserId(): string | null;

	header: { Authorization: string };

	get isAdmin(): boolean;

	get isLoggedIn(): boolean;

	login(event: FormEvent<HTMLFormElement>): Promise<void>;

	logout(): void;

	register(event: FormEvent<HTMLFormElement>): Promise<void>;

	setErrors(errors: ResponseError[]): void;

	setHeader(token: string): void;

	setStatus(status: number): void;

	setToken(token: string): void;

	status: number;

	token: string;
}

export default IAuthStore;
