enum ApiRequest {
	Page = 0,
	Size = 1,
	Sort = 2,
	Search = 3
}

export default ApiRequest;
