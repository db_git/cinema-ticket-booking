enum Pages {
	About = "/about",
	Admin = "/admin",
	AdminCinema = "/admin/cinema",
	AdminGenre = "/admin/genre",
	AdminHall = "/admin/hall",
	AdminMovie = "/admin/movie",
	AdminPerson = "/admin/person",
	AdminProjection = "/admin/projection",
	AdminRating = "/admin/rating",
	AdminReservation = "/admin/reservation",
	AdminSeat = "/admin/seat",
	AdminUser = "/admin/user",
	Home = "/",
	Registration = "/registration",
	Reservations = "/reservations"
}

export default Pages;
