enum HttpMethod {
	Get = 0,
	Post = 1,
	Put = 2,
	Delete = 3
}

export default HttpMethod;
