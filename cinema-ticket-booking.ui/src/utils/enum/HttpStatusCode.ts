enum HttpStatusCode {
	OK = 200,
	Created = 201,
	Accepted = 202,
	NoContent = 204,
	BadRequst = 400,
	Unauthorized = 401,
	NotFound = 404,
	InternalServerError = 500
}

export default HttpStatusCode;
