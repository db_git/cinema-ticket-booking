import { observer } from "mobx-react-lite";
import type IDisplayApiErrorsProps from "./interface/IDisplayApiErrorsProps";

const DisplayApiErrors = ({ errors, filter }: IDisplayApiErrorsProps) => {
	if (!errors.length) return <ul />;

	return (
		<ul>
			{errors
				.filter((error) => {
					return error.field.includes(filter);
				})
				.map((error) => {
					return (
						<li className="text-danger" key={error.message}>
							{error.message}
						</li>
					);
				})}
		</ul>
	);
};

export default observer(DisplayApiErrors);
