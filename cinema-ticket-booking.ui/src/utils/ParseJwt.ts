import { Buffer } from "buffer";

const ParseJwt = (token: string | null, field?: string): string | null => {
	if (!token) return null;

	const data = JSON.parse(
		Buffer.from(token.split(".")[1], "base64").toString()
	);

	if (typeof field === "undefined") return data;
	return data[field];
};

export default ParseJwt;
