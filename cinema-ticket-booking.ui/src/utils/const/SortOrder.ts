const SortOrder = {
	AdminCinema: new Map([
		[1, "Name, ascending"],
		[2, "Name, descending"],
		[3, "City, ascending"],
		[4, "City, descending"],
		[5, "Street, ascending"],
		[6, "Street, descending"],
		[7, "Ticket price, ascending"],
		[8, "Ticket price, descending"]
	]),

	AdminGenre: new Map([
		[1, "Name, ascending"],
		[2, "Name, descending"]
	]),

	AdminHall: new Map([
		[1, "Name, ascending"],
		[2, "Name, descending"],
		[3, "Cinema, ascending"],
		[4, "Cinema, descending"]
	]),

	AdminMovie: new Map([
		[1, "Title, ascending"],
		[2, "Title, descending"],
		[3, "Year, ascending"],
		[4, "Year, descending"],
		[5, "Duration, ascending"],
		[6, "Duration, descending"]
	]),

	AdminPerson: new Map([
		[1, "First name, ascending"],
		[2, "First name, descending"],
		[3, "Last name, ascending"],
		[4, "Last name, descending"]
	]),

	AdminProjection: new Map([
		[1, "Time, ascending"],
		[2, "Time, descending"]
	]),

	AdminRating: new Map([
		[1, "Type, ascending"],
		[2, "Type, descending"]
	]),

	AdminReservation: new Map([
		[1, "First name, ascending"],
		[2, "First name, descending"],
		[3, "Last name, ascending"],
		[4, "Last name, descending"],
		[5, "User name, ascending"],
		[6, "User name, descending"],
		[7, "Movie, ascending"],
		[8, "Movie, descending"],
		[9, "Hall, ascending"],
		[10, "Hall, descending"],
		[11, "Time, ascending"],
		[12, "Time, descending"]
	]),

	AdminSeat: new Map([
		[1, "Number, ascending"],
		[2, "Number, descending"]
	]),

	AdminUser: new Map([
		[1, "First name, ascending"],
		[2, "First name, descending"],
		[3, "Last name, ascending"],
		[4, "Last name, descending"],
		[5, "Email, ascending"],
		[6, "Email, descending"]
	])
};

export default SortOrder;
