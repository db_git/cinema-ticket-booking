import type Hall from "./Hall";

type Seat = {
	hall: Hall;
	id: string;
	isAvailable: boolean;
	number: number;
};

export default Seat;
