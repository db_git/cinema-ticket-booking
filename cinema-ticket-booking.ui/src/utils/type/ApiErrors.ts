import type ResponseError from "./ResponseError";

type ApiErrors = {
	delete: ResponseError[];
	get: ResponseError[];
	post: ResponseError[];
	put: ResponseError[];
};

export default ApiErrors;
