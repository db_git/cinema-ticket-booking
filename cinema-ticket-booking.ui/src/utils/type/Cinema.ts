import type Hall from "./Hall";

type Cinema = {
	city: string;
	halls: Hall[];
	id: string;
	name: string;
	street: string;
	ticketPrice: number;
};

export default Cinema;
