import type Projection from "./Projection";
import type Seat from "./Seat";
import type User from "./User";

type Reservation = {
	id: "reservation";
	projection: Projection;
	seats: Seat[];
	user: User;
};

export default Reservation;
