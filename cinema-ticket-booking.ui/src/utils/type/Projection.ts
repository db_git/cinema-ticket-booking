import type Hall from "./Hall";
import type Movie from "./Movie";
import type Reservation from "./Reservation";

type Projection = {
	hall: Hall;
	id: string;
	movie: Movie;
	reservations: Reservation[];
	time: Date;
};

export default Projection;
