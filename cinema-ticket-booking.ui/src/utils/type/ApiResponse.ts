type ApiResponse<T> = T | T[] | undefined;

export default ApiResponse;
