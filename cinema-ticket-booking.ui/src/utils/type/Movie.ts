import type Genre from "./Genre";
import type Person from "./Person";
import type Rating from "./Rating";

type Movie = {
	actors: Person[];
	backdropUrl: string;
	directors: Person[];
	duration: number;
	genres: Genre[];
	id: string;
	posterUrl: string;
	ratings: Rating[];
	summary: string;
	tagline: string;
	title: string;
	trailerUrl: string;
	year: number;
};

export default Movie;
