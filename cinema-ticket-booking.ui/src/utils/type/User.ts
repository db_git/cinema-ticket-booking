import type Reservation from "./Reservation";

type User = {
	email: string;
	firstName: string;
	id: string;
	lastName: string;
	reservations: Reservation[];
	roles: string[];
};

export default User;
