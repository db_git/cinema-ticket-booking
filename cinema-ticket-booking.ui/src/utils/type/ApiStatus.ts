type ApiStatus = {
	delete: number;
	get: number;
	post: number;
	put: number;
};

export default ApiStatus;
