type ResponseError = {
	field: string;
	message: string;
};

export default ResponseError;
