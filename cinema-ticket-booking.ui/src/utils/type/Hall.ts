import type Cinema from "./Cinema";
import type Projection from "./Projection";
import type Seat from "./Seat";

type Hall = {
	cinema: Cinema;
	id: string;
	name: string;
	projections: Projection[];
	seats: Seat[];
};

export default Hall;
