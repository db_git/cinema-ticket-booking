type Request = {
	page: number;
	search?: string;
	size: number;
	sort: number;
};

export default Request;
