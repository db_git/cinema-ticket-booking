type Person = {
	firstName: string;
	id: string;
	lastName: string;
};

export default Person;
