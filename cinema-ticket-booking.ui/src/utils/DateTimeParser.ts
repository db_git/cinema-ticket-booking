import type IDateTimeParser from "./interface/IDateTimeParser";

class DateTimeParser implements IDateTimeParser {
	private dateTime: Date;

	public constructor(dateTime: Date) {
		this.dateTime = new Date(dateTime);
	}

	public setNewDate(dateTime: Date) {
		this.dateTime = new Date(dateTime);
		return this;
	}

	public addSeconds = (seconds: number) => {
		this.dateTime.setUTCSeconds(this.dateTime.getUTCSeconds() + seconds);
		return this;
	};

	public addMinutes = (minutes: number) => {
		this.dateTime.setUTCMinutes(this.dateTime.getUTCMinutes() + minutes);
		return this;
	};

	public addHours = (hours: number) => {
		this.dateTime.setUTCHours(this.dateTime.getUTCHours() + hours);
		return this;
	};

	public getDateOnly = () => {
		const year = this.dateTime.getUTCFullYear().toString();
		let month = (this.dateTime.getUTCMonth() + 1).toString();
		let day = this.dateTime.getUTCDate().toString();

		if (month.length < 2) month = "0" + month;
		if (day.length < 2) day = "0" + day;

		return [year, month, day].join("/");
	};

	public getTimeOnly = () => {
		let hours = this.dateTime.getUTCHours().toString();
		let minutes = this.dateTime.getUTCMinutes().toString();

		if (hours.length < 2) hours = "0" + hours;
		if (minutes.length < 2) minutes = "0" + minutes;

		return [hours, minutes].join(":");
	};

	public getDate = () => {
		return this.getDateOnly() + " " + this.getTimeOnly() + "h";
	};

	public getIsoDate = () => {
		return this.dateTime.toISOString();
	};
}

export default DateTimeParser;
