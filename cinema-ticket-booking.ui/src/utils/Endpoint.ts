const ip = "localhost";
const port = ":7124";

const Endpoint = (name: string): string => {
	return "https://" + ip + port + "/api/" + name;
};

export default Endpoint;
