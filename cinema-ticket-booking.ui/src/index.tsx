import React from "react";
import ReactDOM from "react-dom";
import App from "./App";
import { StoreProvider } from "./stores/common/StoreProvider";

import "./pages/common/styles/Default.scss";
import "./pages/common/styles/Overrides.scss";

ReactDOM.render(
	<React.StrictMode>
		<StoreProvider>
			<App />
		</StoreProvider>
	</React.StrictMode>,
	document.querySelector("#root")
);
