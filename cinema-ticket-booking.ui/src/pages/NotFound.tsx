import { useEffect, useState } from "react";
import { Button, Container } from "react-bootstrap";
import { Navigate } from "react-router-dom";
import Pages from "../utils/enum/Pages";

import "./common/styles/NotFound.scss";

const NotFound = () => {
	const [redirect, setRedirect] = useState(false);

	useEffect(() => {
		document.title = "Not Found Page";
	}, []);

	const handleOnClick = () => {
		setRedirect(true);
	};

	if (redirect) return <Navigate replace to={Pages.Home} />;
	return (
		<Container>
			<div className="mt-5 text-center">
				<h1 className="text-danger fw-bolder" id="text-404">
					404
				</h1>
				<h5 id="text-404-msg">There is nothing to see here...</h5>
				<Button
					className="link-warning"
					id="text-404-btn"
					onClick={handleOnClick}
					variant="link"
				>
					Go to home page.
				</Button>
			</div>
		</Container>
	);
};

export default NotFound;
