import { runInAction } from "mobx";
import { observer } from "mobx-react-lite";
import { Pagination as Paginate } from "react-bootstrap";
import ApiRequest from "../../../utils/enum/ApiRequest";
import type IEntity from "../interface/IEntity";
import type IPaginationProps from "../interface/IPaginationProps";

const range = (start: number, end: number) => {
	return Array.from({ length: end - start + 1 }, (_, index) => {
		return index + start;
	});
};

const Pagination = <T extends IEntity>({ store }: IPaginationProps<T>) => {
	const pageNumbers = Math.ceil(store.getRecords / store.getRequest.size);

	const changePage = async (page: number): Promise<void> => {
		await runInAction(async () => {
			if (page < 1 || page > pageNumbers) return;
			if (store.getRequest.page !== page) {
				store.setRequest(ApiRequest.Page, page);
				await store.get();
			}
		});
	};

	const previousPage = async () => {
		await runInAction(async () => {
			await changePage(store.getRequest.page - 1);
		});
	};

	const nextPage = async () => {
		await runInAction(async () => {
			await changePage(store.getRequest.page + 1);
		});
	};

	const calculatePages = () => {
		const sides = 2;
		const pageLimit = 5 + sides;
		const current = store.getRequest.page;

		if (pageLimit >= pageNumbers) {
			return range(1, pageNumbers);
		}

		const leftIndex = Math.max(current - sides, 1);
		const rightIndex = Math.min(current + sides, pageNumbers);
		const showLeft = leftIndex > 2;
		const showRight = rightIndex < pageNumbers - 2;

		if (!showLeft && showRight) {
			return [...range(1, 3 + 2 * sides), "r"];
		}

		if (showLeft && !showRight) {
			return [
				"l",
				...range(pageNumbers - (3 + 2 * sides) + 1, pageNumbers)
			];
		}

		return ["ll", ...range(leftIndex, rightIndex), "rr"];
	};

	const generatePages = () => {
		const pages: JSX.Element[] = [];

		for (const page of calculatePages()) {
			if (typeof page === "number") {
				pages.push(
					<Paginate.Item
						active={store.getRequest.page === page}
						key={page}
						onClick={async () => {
							await changePage(page);
						}}
					>
						{page}
					</Paginate.Item>
				);
			} else {
				pages.push(<Paginate.Ellipsis disabled key={page} />);
			}
		}

		return pages;
	};

	if (pageNumbers < 2) return null;
	return (
		<Paginate className="justify-content-center justify-content-lg-end mt-5">
			<Paginate.First
				onClick={async () => {
					await changePage(1);
				}}
			/>
			<Paginate.Prev onClick={previousPage} />
			{generatePages()}
			<Paginate.Next onClick={nextPage} />
			<Paginate.Last
				onClick={async () => {
					await changePage(pageNumbers);
				}}
			/>
		</Paginate>
	);
};

export default observer(Pagination);
