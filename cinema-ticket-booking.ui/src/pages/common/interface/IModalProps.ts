import type { ReactNode } from "react";

interface IModalProps {
	children: ReactNode;
	show: boolean;

	toggle(): void;
}

export default IModalProps;
