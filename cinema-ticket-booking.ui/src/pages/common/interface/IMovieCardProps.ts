import type Movie from "../../../utils/type/Movie";

interface IMovieCardProps {
	movie: Movie;
}

export default IMovieCardProps;
