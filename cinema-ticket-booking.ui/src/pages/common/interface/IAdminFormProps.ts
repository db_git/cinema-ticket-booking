import type ApiStore from "../../../stores/common/ApiStore";

interface IAdminFormProps<T> {
	data?: any;
	store: ApiStore<T>;

	toggleCreateModal?(): void;

	toggleUpdateModal?(): void;
}

export default IAdminFormProps;
