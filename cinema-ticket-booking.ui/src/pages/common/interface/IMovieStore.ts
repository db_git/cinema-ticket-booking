import type MovieStore from "../../../stores/MovieStore";

interface IMovieStore {
	store: MovieStore;
}

export default IMovieStore;
