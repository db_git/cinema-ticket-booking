interface IRegistrationFormProps {
	setShowLoginForm(x: boolean): void;
}

export default IRegistrationFormProps;
