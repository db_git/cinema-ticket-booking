import type IApiStore from "../../../utils/interface/IApiStore";

interface ISortProps<T> {
	changeSort(sort: number): Promise<void>;
	sortItems: Map<number, string>;

	store: IApiStore<T>;
}

export default ISortProps;
