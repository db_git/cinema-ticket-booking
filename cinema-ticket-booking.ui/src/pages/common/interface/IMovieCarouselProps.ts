import type Movie from "../../../utils/type/Movie";

interface IMovieCarouselProps {
	carouselMovies: Movie[];
}

export default IMovieCarouselProps;
