import type { SetStateAction } from "react";

interface IReservationModalProps {
	projectionId: string;

	setShowProjectionModal(state: SetStateAction<boolean>): void;

	setShowReservationModal(state: SetStateAction<boolean>): void;

	setShowReservationSuccessAlert(state: SetStateAction<boolean>): void;

	showReservationModal: boolean;
}

export default IReservationModalProps;
