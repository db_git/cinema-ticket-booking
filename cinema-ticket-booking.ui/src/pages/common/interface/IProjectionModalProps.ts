import type { SetStateAction } from "react";
import type Movie from "../../../utils/type/Movie";

interface IProjectionModalProps {
	movie: Movie;

	setShowMovieModal(state: SetStateAction<boolean>): void;

	setShowProjectionModal(state: SetStateAction<boolean>): void;

	showProjectionModal: boolean;
}

export default IProjectionModalProps;
