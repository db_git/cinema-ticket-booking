import type IApiStore from "../../../utils/interface/IApiStore";

interface IPaginationProps<T> {
	store: IApiStore<T>;
}

export default IPaginationProps;
