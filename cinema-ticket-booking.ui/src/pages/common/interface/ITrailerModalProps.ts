import type { SetStateAction } from "react";
import type Movie from "../../../utils/type/Movie";

interface ITrailerModalProps {
	movie: Movie;
	setShowMovieModal(state: SetStateAction<boolean>): void;

	setShowTrailerModal(state: SetStateAction<boolean>): void;

	showTrailerModal: boolean;
}

export default ITrailerModalProps;
