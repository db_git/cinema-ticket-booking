import type ApiStore from "../../../stores/common/ApiStore";
import type { Columns } from "./AdminTable";
import type IEntity from "./IEntity";

interface IAdminPageProps<T extends IEntity> {
	columns: Array<Columns<T, keyof T>>;
	showCreate: boolean;
	showUpdate: boolean;
	store: ApiStore<T>;
}

export default IAdminPageProps;
