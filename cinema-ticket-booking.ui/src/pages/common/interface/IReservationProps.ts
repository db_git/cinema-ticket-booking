import type Reservation from "../../../utils/type/Reservation";

interface IReservationProps {
	reservations: Reservation[];
}

export default IReservationProps;
