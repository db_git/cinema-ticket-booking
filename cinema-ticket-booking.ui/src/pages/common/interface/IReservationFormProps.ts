import type Projection from "../../../utils/type/Projection";

interface IReservationFormProps {
	projection: Projection;

	toggleCreateModal(): void;
}

export default IReservationFormProps;
