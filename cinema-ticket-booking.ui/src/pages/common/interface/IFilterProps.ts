import type IApiStore from "../../../utils/interface/IApiStore";

interface IFilterProps {
	recordsPerPage: number[];
	sortItems: Map<number, string>;
	store: IApiStore<any>;
}

export default IFilterProps;
