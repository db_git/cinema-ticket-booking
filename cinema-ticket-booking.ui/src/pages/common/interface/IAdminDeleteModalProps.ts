interface IAdminDeleteModalProps {
	id: string;
	show: boolean;

	storeDelete(id: string): Promise<void>;

	toggleDeleteModal(): void;
}

export default IAdminDeleteModalProps;
