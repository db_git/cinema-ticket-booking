import type IApiStore from "../../../utils/interface/IApiStore";

interface IRecordProps<T> {
	changeRecords(size: number): Promise<void>;
	recordsPerPage: number[];

	store: IApiStore<T>;
}

export default IRecordProps;
