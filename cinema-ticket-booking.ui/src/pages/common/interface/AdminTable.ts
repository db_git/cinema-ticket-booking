import type { SetStateAction } from "react";
import type IEntity from "./IEntity";

type Columns<T extends IEntity, K extends keyof T> = {
	display: string;
	key: K;
	secondKey?: string;
	thirdKey?: string;
};

interface IAdminTableProps<T extends IEntity, K extends keyof T> {
	body: T[];
	headers: Array<Columns<T, K>>;

	setModalData(data: SetStateAction<T | undefined>): void;

	toggleDeleteModal(): void;

	toggleUpdateModal?(): void;
}

export type { Columns, IAdminTableProps };
