import type { SetStateAction } from "react";

interface IMovieModalProps {
	movieId: string;

	setShowMovieModal(state: SetStateAction<boolean>): void;

	setShowProjectionModal(state: SetStateAction<boolean>): void;

	setShowTrailerModal(state: SetStateAction<boolean>): void;

	showMovieModal: boolean;

	showProjectionModal: boolean;

	showTrailerModal: boolean;
}

export default IMovieModalProps;
