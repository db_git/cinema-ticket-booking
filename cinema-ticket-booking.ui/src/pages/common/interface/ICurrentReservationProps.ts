import type Reservation from "../../../utils/type/Reservation";

interface ICurrentReservationProps {
	reservations: Reservation[];

	storeDelete(id: string): Promise<void>;
}

export default ICurrentReservationProps;
