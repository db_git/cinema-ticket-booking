import type ResponseError from "../../../utils/type/ResponseError";

interface IFormProps<T> {
	data?: T;
	errors: ResponseError[];

	formCreate(form: FormData): void;

	formUpdate(id: string, form: FormData): void;
}

export default IFormProps;
