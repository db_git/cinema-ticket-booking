import { observer } from "mobx-react-lite";
import {
	Offcanvas,
	Container,
	Nav,
	Navbar,
	NavDropdown
} from "react-bootstrap";
import {
	AiOutlineBars,
	AiOutlineHome,
	AiOutlineInfoCircle
} from "react-icons/ai";
import {
	BiChair,
	BiBookmark,
	BiFilm,
	BiLogIn,
	BiLogOut,
	BiStar,
	BiUser
} from "react-icons/bi";
import { BsPersonFill } from "react-icons/bs";
import { FaFilm, FaTheaterMasks } from "react-icons/fa";
import { GiFilmProjector, GiFilmSpool, GiTheater } from "react-icons/gi";
import { RiAdminLine } from "react-icons/ri";
import { LinkContainer } from "react-router-bootstrap";
import { useStore } from "../../../stores/common/StoreProvider";
import Pages from "../../../utils/enum/Pages";

const Sidebar = () => {
	const { authStore } = useStore();

	const handleLogout = () => {
		authStore.logout();
	};

	return (
		<Navbar
			bg="dark"
			collapseOnSelect
			expand={false}
			sticky="top"
			variant="dark"
		>
			<Container fluid>
				<Navbar.Toggle>
					<AiOutlineBars />
				</Navbar.Toggle>
				<Navbar.Offcanvas placement="start" scroll>
					<Offcanvas.Header closeButton>
						<Offcanvas.Title>Maxi Cinema</Offcanvas.Title>
					</Offcanvas.Header>
					<Offcanvas.Body>
						<Nav variant="pills">
							{authStore.isLoggedIn && authStore.isAdmin && (
								<NavDropdown
									menuVariant="dark"
									title={
										<span className="ms-3">
											<RiAdminLine />
											&nbsp; Admin
										</span>
									}
								>
									<LinkContainer to={Pages.AdminCinema}>
										<NavDropdown.Item>
											<span className="ms-3">
												<BiFilm />
												&nbsp; Cinema
											</span>
										</NavDropdown.Item>
									</LinkContainer>

									<LinkContainer to={Pages.AdminGenre}>
										<NavDropdown.Item>
											<span className="ms-3">
												<FaTheaterMasks />
												&nbsp; Genre
											</span>
										</NavDropdown.Item>
									</LinkContainer>

									<LinkContainer to={Pages.AdminHall}>
										<NavDropdown.Item>
											<span className="ms-3">
												<GiTheater />
												&nbsp; Hall
											</span>
										</NavDropdown.Item>
									</LinkContainer>

									<LinkContainer to={Pages.AdminMovie}>
										<NavDropdown.Item>
											<span className="ms-3">
												<GiFilmSpool />
												&nbsp; Movie
											</span>
										</NavDropdown.Item>
									</LinkContainer>

									<LinkContainer to={Pages.AdminPerson}>
										<NavDropdown.Item>
											<span className="ms-3">
												<BsPersonFill />
												&nbsp; Person
											</span>
										</NavDropdown.Item>
									</LinkContainer>

									<LinkContainer to={Pages.AdminProjection}>
										<NavDropdown.Item>
											<span className="ms-3">
												<GiFilmProjector />
												&nbsp; Projection
											</span>
										</NavDropdown.Item>
									</LinkContainer>

									<LinkContainer to={Pages.AdminRating}>
										<NavDropdown.Item>
											<span className="ms-3">
												<BiStar />
												&nbsp; Rating
											</span>
										</NavDropdown.Item>
									</LinkContainer>

									<LinkContainer to={Pages.AdminReservation}>
										<NavDropdown.Item>
											<span className="ms-3">
												<BiBookmark />
												&nbsp; Reservation
											</span>
										</NavDropdown.Item>
									</LinkContainer>

									<LinkContainer to={Pages.AdminSeat}>
										<NavDropdown.Item>
											<span className="ms-3">
												<BiChair />
												&nbsp; Seat
											</span>
										</NavDropdown.Item>
									</LinkContainer>

									<LinkContainer to={Pages.AdminUser}>
										<NavDropdown.Item>
											<span className="ms-3">
												<BiUser />
												&nbsp; User
											</span>
										</NavDropdown.Item>
									</LinkContainer>
								</NavDropdown>
							)}
							<LinkContainer to={Pages.Home}>
								<Nav.Link>
									<span className="ms-3">
										<AiOutlineHome />
										&nbsp; Home
									</span>
								</Nav.Link>
							</LinkContainer>

							{!authStore.isLoggedIn && (
								<LinkContainer to={Pages.Registration}>
									<Nav.Link>
										<span className="ms-3">
											<BiLogIn />
											&nbsp; Registration
										</span>
									</Nav.Link>
								</LinkContainer>
							)}

							<LinkContainer to={Pages.About}>
								<Nav.Link>
									<span className="ms-3">
										<AiOutlineInfoCircle />
										&nbsp; About
									</span>
								</Nav.Link>
							</LinkContainer>
						</Nav>

						{authStore.isLoggedIn && (
							<>
								<hr />
								<Nav variant="pills">
									<p className="text-break text-muted">
										<span className="ms-3">
											&nbsp;{authStore.getEmail}
										</span>
									</p>
									{!authStore.isAdmin && (
										<LinkContainer to={Pages.Reservations}>
											<Nav.Link>
												<span className="ms-3">
													<BiBookmark />
													&nbsp; Reservations
												</span>
											</Nav.Link>
										</LinkContainer>
									)}
								</Nav>

								<Nav>
									<LinkContainer to={Pages.Home}>
										<Nav.Link onClick={handleLogout}>
											<span className="ms-3">
												<BiLogOut />
												&nbsp; Log out
											</span>
										</Nav.Link>
									</LinkContainer>
								</Nav>
							</>
						)}
					</Offcanvas.Body>
				</Navbar.Offcanvas>

				<LinkContainer to={Pages.Home}>
					<Navbar.Brand>
						<FaFilm />
						&nbsp; Maxi Cinema
					</Navbar.Brand>
				</LinkContainer>
			</Container>
		</Navbar>
	);
};

export default observer(Sidebar);
