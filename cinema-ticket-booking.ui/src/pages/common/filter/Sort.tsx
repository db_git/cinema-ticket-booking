import { observer } from "mobx-react-lite";
import { Dropdown, DropdownButton } from "react-bootstrap";
import { AiOutlineSortAscending } from "react-icons/ai";
import type IEntity from "../interface/IEntity";
import type ISortProps from "../interface/ISortProps";

const Sort = <T extends IEntity>({
	store,
	sortItems,
	changeSort
}: ISortProps<T>) => {
	return (
		<DropdownButton
			drop="end"
			menuVariant="dark"
			title={<AiOutlineSortAscending />}
			variant="dark"
		>
			{(() => {
				const generated: JSX.Element[] = [];

				for (const [key, value] of sortItems) {
					generated.push(
						<Dropdown.Item
							active={store.getRequest.sort === key}
							key={key}
							onClick={async () => {
								await changeSort(key);
							}}
						>
							{value}
						</Dropdown.Item>
					);
				}

				return generated;
			})()}
		</DropdownButton>
	);
};

export default observer(Sort);
