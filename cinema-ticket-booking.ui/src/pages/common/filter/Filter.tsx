import debounce from "lodash.debounce";
import { runInAction } from "mobx";
import { observer } from "mobx-react-lite";
import type { ChangeEvent, ChangeEventHandler } from "react";
import { Container, FormControl, InputGroup } from "react-bootstrap";
import ApiRequest from "../../../utils/enum/ApiRequest";
import type IFilterProps from "../interface/IFilterProps";
import Record from "./Record";
import Sort from "./Sort";

const Filter = ({ store, sortItems, recordsPerPage }: IFilterProps) => {
	const changeRecords = async (size: number): Promise<void> => {
		await runInAction(async () => {
			if (store.getRequest.size !== size) {
				if (store.getRequest.size < size) store.getRequest.page = 1;
				store.setRequest(ApiRequest.Size, size);
				await store.get();
			}
		});
	};

	const changeSort = async (sort: number): Promise<void> => {
		await runInAction(async () => {
			if (store.getRequest.sort !== sort) {
				store.setRequest(ApiRequest.Sort, sort);
				await store.get();
			}
		});
	};

	const handleChange = (): ChangeEventHandler<HTMLInputElement> => {
		return debounce(
			async (event: ChangeEvent<HTMLInputElement>): Promise<void> => {
				runInAction(() => {
					store.setRequest(ApiRequest.Search, event.target.value);
					store.setRequest(ApiRequest.Page, 1);
				});
				await store.get();
			},
			400
		);
	};

	return (
		<Container>
			<InputGroup>
				<FormControl
					className="rounded-pill rounded-end border-2 border-dark"
					defaultValue={store.getRequest.search}
					onChange={handleChange()}
					placeholder="Search"
				/>
				<Record
					changeRecords={changeRecords}
					recordsPerPage={recordsPerPage}
					store={store}
				/>
				<Sort
					changeSort={changeSort}
					sortItems={sortItems}
					store={store}
				/>
			</InputGroup>
		</Container>
	);
};

Filter.defaultProps = {
	recordsPerPage: [5, 10, 15, 20, 25, 30]
};

export default observer(Filter);
