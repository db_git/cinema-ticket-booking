import { observer } from "mobx-react-lite";
import { Dropdown, DropdownButton } from "react-bootstrap";
import { BsListOl } from "react-icons/bs";
import type IEntity from "../interface/IEntity";
import type IRecordProps from "../interface/IRecordProps";

const Record = <T extends IEntity>({
	store,
	changeRecords,
	recordsPerPage
}: IRecordProps<T>) => {
	return (
		<DropdownButton
			drop="end"
			menuVariant="dark"
			title={<BsListOl />}
			variant="dark"
		>
			{recordsPerPage.map((record) => {
				return (
					<Dropdown.Item
						active={store.getRequest.size === record}
						key={record}
						onClick={async () => {
							await changeRecords(record);
						}}
					>
						{record}
					</Dropdown.Item>
				);
			})}
		</DropdownButton>
	);
};

export default observer(Record);
