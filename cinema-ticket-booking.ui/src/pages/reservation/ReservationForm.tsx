import { runInAction } from "mobx";
import { observer } from "mobx-react-lite";
import type { FormEvent } from "react";
import { useEffect, useState } from "react";
import { Form } from "react-bootstrap";
import { useStore } from "../../stores/common/StoreProvider";
import DisplayApiErrors from "../../utils/DisplayApiErrors";
import ApiRequest from "../../utils/enum/ApiRequest";
import HttpStatusCode from "../../utils/enum/HttpStatusCode";
import type Seat from "../../utils/type/Seat";
import type IReservationFormProps from "../common/interface/IReservationFormProps";

const ReservationForm = ({
	toggleCreateModal,
	projection
}: IReservationFormProps) => {
	const { authStore, seatStore, reservationStore, userReservationStore } =
		useStore();
	const [seats, setSeats] = useState<Seat[]>();

	useEffect(() => {
		seatStore.setRequest(ApiRequest.Page, 1);
		seatStore.setRequest(ApiRequest.Size, 500);
		seatStore.setRequest(ApiRequest.Search, projection.hall.id);
		reservationStore.setRequest(ApiRequest.Page, 1);
		reservationStore.setRequest(ApiRequest.Size, 500);
		reservationStore.setRequest(ApiRequest.Search, projection.id);

		reservationStore.get().then(() => {
			seatStore.get().then(() => {
				if (Array.isArray(seatStore.getResponse)) {
					if (!Array.isArray(reservationStore.getResponse)) {
						setSeats(seatStore.getResponse);
						return;
					}

					const availableSeats: Seat[] = [];

					let reservedSeats: Seat[] = [];
					for (const reservation of reservationStore.getResponse)
						reservedSeats = [
							...reservedSeats,
							...reservation.seats
						];

					for (const seat of seatStore.getResponse) {
						if (
							!reservedSeats.some((reservedSeat) => {
								return reservedSeat.id === seat.id;
							})
						) {
							availableSeats.push(seat);
						}
					}

					setSeats(availableSeats);
				}
			});
		});

		return () => {
			seatStore.setRequest(ApiRequest.Page, 1);
			seatStore.setRequest(ApiRequest.Size, 10);
			seatStore.setRequest(ApiRequest.Search, "");
			reservationStore.setRequest(ApiRequest.Page, 1);
			reservationStore.setRequest(ApiRequest.Size, 10);
			reservationStore.setRequest(ApiRequest.Search, "");
		};
	}, [projection, reservationStore, seatStore]);

	const handleSubmit = (event: FormEvent<HTMLFormElement>) => {
		runInAction(() => {
			if (Array.isArray(seats)) {
				event.preventDefault();
				const form = new FormData(event.currentTarget);
				userReservationStore.create(form).then(() => {
					if (
						userReservationStore.getStatus.post ===
							HttpStatusCode.Created &&
						toggleCreateModal
					) {
						toggleCreateModal();
					}
				});
			}
		});
	};

	if (!seats || seats.length === 0) {
		return (
			<h5>
				Unfortunately, all seats are taken for hall{" "}
				{projection.hall.name}...
			</h5>
		);
	}

	return (
		<Form id="reservationForm" noValidate onSubmit={handleSubmit}>
			<Form.Control
				hidden
				name="userId"
				readOnly
				type="text"
				value={String(authStore.getUserId)}
			/>
			<Form.Control
				hidden
				name="projectionId"
				readOnly
				type="text"
				value={projection.id}
			/>

			<Form.Group>
				<Form.Label>Seats</Form.Label>
				<ul>
					{seats.map((seat) => {
						return (
							<Form.Check
								inline
								key={seat.id}
								label={seat.number}
								name="seats"
								type="checkbox"
								value={seat.id}
							/>
						);
					})}
				</ul>
				<Form.Text>
					{userReservationStore.getStatus.post ===
						HttpStatusCode.BadRequst && (
						<DisplayApiErrors
							errors={userReservationStore.getErrors.post}
							filter="Seats"
						/>
					)}
				</Form.Text>
			</Form.Group>
		</Form>
	);
};

export default observer(ReservationForm);
