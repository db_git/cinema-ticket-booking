import { observer } from "mobx-react-lite";
import { useEffect } from "react";
import { Container, Spinner } from "react-bootstrap";
import { Navigate } from "react-router-dom";
import { useStore } from "../../stores/common/StoreProvider";
import DateTimeParser from "../../utils/DateTimeParser";
import HttpStatusCode from "../../utils/enum/HttpStatusCode";
import Pages from "../../utils/enum/Pages";
import CurrentReservations from "./CurrentReservations";
import PreviousReservations from "./PreviousReservations";

const Reservations = () => {
	const { authStore, userReservationStore } = useStore();

	useEffect(() => {
		document.title = "Reservations";
	}, []);

	useEffect(() => {
		userReservationStore.get().then();
	}, [userReservationStore]);

	if (authStore.isAdmin || !authStore.isLoggedIn)
		return <Navigate replace to={Pages.Home} />;

	if (
		userReservationStore.getStatus.get ===
		HttpStatusCode.InternalServerError
	) {
		return (
			<Container>
				<div className="mt-3">
					<h1 className="text-center">
						A network error has occurred. Please try again later.
					</h1>
				</div>
			</Container>
		);
	}

	if (Array.isArray(userReservationStore.getResponse)) {
		return (
			<Container>
				<div className="mt-3">
					<PreviousReservations
						reservations={userReservationStore.getResponse.filter(
							(reservation) => {
								const today = new DateTimeParser(new Date());
								today.addHours(1);

								return (
									reservation.projection.time.toString() <
									today.getIsoDate()
								);
							}
						)}
					/>
					<CurrentReservations
						reservations={userReservationStore.getResponse.filter(
							(reservation) => {
								const today = new DateTimeParser(new Date());
								today.addHours(1);

								return (
									reservation.projection.time.toString() >=
									today.getIsoDate()
								);
							}
						)}
						storeDelete={userReservationStore.delete}
					/>
				</div>
			</Container>
		);
	} else if (
		userReservationStore.getStatus.get === HttpStatusCode.NoContent
	) {
		return (
			<Container>
				<div className="mt-3">
					<h3 className="mt-5 fw-bold text-center">
						You have no reservations!
					</h3>
				</div>
			</Container>
		);
	} else {
		return (
			<Container>
				<div className="mt-3">
					<Spinner animation="border" as="span" />
					<h4 className="ms-4 d-inline-block">
						Loading reservations...
					</h4>
				</div>
			</Container>
		);
	}
};

export default observer(Reservations);
