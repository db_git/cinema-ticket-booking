import { runInAction } from "mobx";
import { observer } from "mobx-react-lite";
import { useState } from "react";
import { Button, ButtonGroup, Image, Table } from "react-bootstrap";
import DateTimeParser from "../../utils/DateTimeParser";
import DeleteModal from "../admin/common/DeleteModal";
import type ICurrentReservationProps from "../common/interface/ICurrentReservationProps";

const CurrentReservations = ({
	reservations,
	storeDelete
}: ICurrentReservationProps) => {
	const [id, setId] = useState("");
	const [showDeleteModal, setShowDeleteModal] = useState(false);

	const toggleDeleteModal = () => {
		setShowDeleteModal(!showDeleteModal);
	};

	if (!reservations || reservations.length === 0) return null;

	return (
		<>
			<DeleteModal
				id={id}
				show={showDeleteModal}
				storeDelete={storeDelete}
				toggleDeleteModal={toggleDeleteModal}
			/>
			<Table responsive variant="dark">
				<thead className="text-center">
					<tr>
						<th>&nbsp;</th>
						<th>Title</th>
						<th>Hall</th>
						<th>Time</th>
						<th>Seats</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody>
					{reservations.map((reservation) => {
						return (
							<tr
								className="reservation-row text-center"
								key={reservation.id}
							>
								<td>
									<Image
										alt={reservation.projection.movie.title}
										className="reservation-image"
										fluid
										src={
											reservation.projection.movie
												.posterUrl
										}
										thumbnail
									/>
								</td>
								<td>{reservation.projection.movie.title}</td>
								<td>{reservation.projection.hall.name}</td>
								<td>
									{new DateTimeParser(
										reservation.projection.time
									).getDate()}
								</td>
								<td>
									{reservation.seats
										.slice()
										.sort((seatA, seatB) => {
											return seatA.number - seatB.number;
										})
										.map((seat) => {
											return seat.number;
										})
										.join(", ")}
								</td>
								<td>
									<ButtonGroup>
										<Button variant="success">
											Buy ticket
										</Button>
										<Button
											onClick={() => {
												runInAction(() => {
													setId(reservation.id);
												});
												toggleDeleteModal();
											}}
											variant="outline-danger"
										>
											Delete
										</Button>
									</ButtonGroup>
								</td>
							</tr>
						);
					})}
				</tbody>
			</Table>
		</>
	);
};

export default observer(CurrentReservations);
