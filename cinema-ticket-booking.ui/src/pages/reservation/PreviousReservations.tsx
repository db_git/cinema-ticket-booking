import { observer } from "mobx-react-lite";
import type { FC } from "react";
import { Accordion, Table, Image } from "react-bootstrap";
import DateTimeParser from "../../utils/DateTimeParser";
import type Reservation from "../../utils/type/Reservation";

import "../common/styles/Reservation.scss";

interface IPreviousReservationsProps {
	reservations: Reservation[];
}

const ReservationAccordion: FC = ({ children }) => {
	return (
		<Accordion flush>
			<Accordion.Item eventKey="0">
				<Accordion.Header>Previous reservations</Accordion.Header>
				<Accordion.Body>{children}</Accordion.Body>
			</Accordion.Item>
		</Accordion>
	);
};

const PreviousReservations = ({ reservations }: IPreviousReservationsProps) => {
	if (!reservations || reservations.length === 0) return null;

	return (
		<ReservationAccordion>
			<Table responsive size="md" variant="dark">
				<thead className="text-center">
					<tr>
						<th>&nbsp;</th>
						<th>Title</th>
						<th>Hall</th>
						<th>Time</th>
						<th>Seats</th>
					</tr>
				</thead>
				<tbody>
					{reservations.map((reservation) => {
						return (
							<tr
								className="reservation-row text-center"
								key={reservation.id}
							>
								<td>
									<Image
										alt={reservation.projection.movie.title}
										className="reservation-image"
										fluid
										src={
											reservation.projection.movie
												.posterUrl
										}
										thumbnail
									/>
								</td>
								<td>{reservation.projection.movie.title}</td>
								<td>{reservation.projection.hall.name}</td>
								<td>
									{new DateTimeParser(
										reservation.projection.time
									).getDate()}
								</td>
								<td>
									{reservation.seats
										.slice()
										.sort((seatA, seatB) => {
											return seatA.number - seatB.number;
										})
										.map((seat) => {
											return seat.number;
										})
										.join(", ")}
								</td>
							</tr>
						);
					})}
				</tbody>
			</Table>
		</ReservationAccordion>
	);
};

export default observer(PreviousReservations);
