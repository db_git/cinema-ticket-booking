import { observer } from "mobx-react-lite";
import { useEffect, useState } from "react";
import { Navigate } from "react-router-dom";
import { useStore } from "../../stores/common/StoreProvider";
import Pages from "../../utils/enum/Pages";
import LoginForm from "./LoginForm";
import RegisterForm from "./RegisterForm";

import "../common/styles/Registration.scss";

const Registration = () => {
	const { authStore } = useStore();
	const [showLoginForm, setShowLoginForm] = useState(true);

	useEffect(() => {
		if (showLoginForm) document.title = "Login";
		else document.title = "Register";
	}, [showLoginForm]);

	if (authStore.isLoggedIn) return <Navigate replace to={Pages.Home} />;
	return (
		<div className="mx-auto mt-3" id="registration-form">
			{showLoginForm ? (
				<LoginForm setShowLoginForm={setShowLoginForm} />
			) : (
				<RegisterForm setShowLoginForm={setShowLoginForm} />
			)}
		</div>
	);
};

export default observer(Registration);
