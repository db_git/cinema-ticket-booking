import { runInAction } from "mobx";
import { observer } from "mobx-react-lite";
import type { FormEvent } from "react";
import { useEffect, useState } from "react";
import { Button, Form, FloatingLabel } from "react-bootstrap";
import { useStore } from "../../stores/common/StoreProvider";
import DisplayApiErrors from "../../utils/DisplayApiErrors";
import HttpStatusCode from "../../utils/enum/HttpStatusCode";
import type IRegistrationFormProps from "../common/interface/IRegistrationFormProps";

const RegisterForm = ({ setShowLoginForm }: IRegistrationFormProps) => {
	const { authStore } = useStore();
	const [firstNameValid, setFirstNameValid] = useState(false);
	const [lastNameValid, setLastNameValid] = useState(false);
	const [emailValid, setEmailValid] = useState(false);
	const [passwordValid, setPasswordValid] = useState(false);
	const [formSubmitted, setFormSubmitted] = useState(false);
	const [passwordsMatch, setPasswordsMatch] = useState<JSX.Element | null>();
	const [status, setStatus] = useState(0);

	const showLogin = () => {
		setShowLoginForm(true);
	};

	useEffect(() => {
		runInAction(() => {
			setStatus(authStore.getStatus);

			setFirstNameValid(
				status !== HttpStatusCode.InternalServerError &&
					!authStore.getErrors.filter((error) => {
						return error.field === "FirstName";
					}).length
			);
			setLastNameValid(
				status !== HttpStatusCode.InternalServerError &&
					!authStore.getErrors.filter((error) => {
						return error.field === "LastName";
					}).length
			);
			setPasswordValid(
				status !== HttpStatusCode.InternalServerError &&
					!authStore.getErrors.filter((error) => {
						return error.field.includes("Password");
					}).length
			);
			setEmailValid(
				status !== HttpStatusCode.InternalServerError &&
					!authStore.getErrors.filter((error) => {
						return error.field.includes("Email");
					}).length &&
					!authStore.getErrors.filter((error) => {
						return error.field.includes("Duplicate");
					}).length
			);
		});
	}, [status, authStore.getStatus, authStore.getErrors]);

	const register = async (event: FormEvent<HTMLFormElement>) => {
		event.preventDefault();
		const formElements = event.currentTarget
			.elements as unknown as HTMLFormElement;

		if (formElements.RepeatPassword.value === formElements.Password.value) {
			setFormSubmitted(true);
			setPasswordsMatch(null);
			await authStore.register(event);
		} else {
			setPasswordsMatch(
				<ul>
					<li className="text-danger">Passwords don't match.</li>
				</ul>
			);
		}
	};

	return (
		<Form noValidate onSubmit={register}>
			<h2 className="text-center mt-5 mb-3">Sign up</h2>

			<Form.Group>
				<FloatingLabel label="Enter your first name">
					<Form.Control
						isInvalid={formSubmitted && !firstNameValid}
						isValid={formSubmitted && firstNameValid}
						name="FirstName"
						placeholder="Enter your first name"
						type="text"
					/>
				</FloatingLabel>

				<Form.Text>
					<DisplayApiErrors
						errors={authStore.getErrors}
						filter="LastName"
					/>
				</Form.Text>
			</Form.Group>

			<Form.Group>
				<FloatingLabel label="Enter your last name">
					<Form.Control
						isInvalid={formSubmitted && !lastNameValid}
						isValid={formSubmitted && lastNameValid}
						name="LastName"
						placeholder="Enter your last name"
						type="text"
					/>
				</FloatingLabel>

				<Form.Text>
					<DisplayApiErrors
						errors={authStore.getErrors}
						filter="LastName"
					/>
				</Form.Text>
			</Form.Group>

			<Form.Group>
				<FloatingLabel label="Enter your email address">
					<Form.Control
						isInvalid={formSubmitted && !emailValid}
						isValid={formSubmitted && emailValid}
						name="Email"
						placeholder="Enter your email address"
						type="email"
					/>
				</FloatingLabel>

				<Form.Text>
					<DisplayApiErrors
						errors={authStore.getErrors}
						filter="Email"
					/>
					<DisplayApiErrors
						errors={authStore.getErrors}
						filter="Duplicate"
					/>
				</Form.Text>
			</Form.Group>

			<Form.Group>
				<FloatingLabel label="Enter your password">
					<Form.Control
						isInvalid={formSubmitted && !passwordValid}
						isValid={formSubmitted && passwordValid}
						name="Password"
						placeholder="Enter your password"
						type="password"
					/>
				</FloatingLabel>

				<Form.Text>
					<DisplayApiErrors
						errors={authStore.getErrors}
						filter="Password"
					/>
				</Form.Text>
			</Form.Group>

			<Form.Group>
				<FloatingLabel label="Repeat your password">
					<Form.Control
						isInvalid={formSubmitted && Boolean(passwordsMatch)}
						isValid={
							formSubmitted && passwordValid && !passwordsMatch
						}
						name="RepeatPassword"
						placeholder="Repeat your password"
						type="password"
					/>
				</FloatingLabel>

				<Form.Text>{passwordsMatch}</Form.Text>
			</Form.Group>

			{status === HttpStatusCode.InternalServerError && (
				<Form.Group>
					<Form.Text>
						<ul>
							<li className="text-danger">
								A network error has occurred. Please try again
								later.
							</li>
						</ul>
					</Form.Text>
				</Form.Group>
			)}

			<div className="text-center mt-3">
				<Button
					className="w-50 text-white"
					type="submit"
					variant="primary"
				>
					Register
				</Button>

				<p className="mt-2">
					Already have an account?&nbsp;
					<a
						className="text-decoration-none"
						onClick={showLogin}
						type="button"
					>
						Login instead!
					</a>
				</p>
			</div>
		</Form>
	);
};

export default observer(RegisterForm);
