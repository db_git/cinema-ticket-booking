import { runInAction } from "mobx";
import { observer } from "mobx-react-lite";
import type { FormEvent } from "react";
import { useEffect, useState } from "react";
import { Button, FloatingLabel, Form } from "react-bootstrap";
import { useStore } from "../../stores/common/StoreProvider";
import DisplayApiErrors from "../../utils/DisplayApiErrors";
import HttpStatusCode from "../../utils/enum/HttpStatusCode";
import type IRegistrationFormProps from "../common/interface/IRegistrationFormProps";

const LoginForm = ({ setShowLoginForm }: IRegistrationFormProps) => {
	const { authStore } = useStore();
	const [emailValid, setEmailValid] = useState(false);
	const [passwordValid, setPasswordValid] = useState(false);
	const [incorrectCredentials, setIncorrectCredentials] = useState(false);
	const [formSubmitted, setFormSubmitted] = useState(false);
	const [status, setStatus] = useState(0);

	const showLogin = () => {
		setShowLoginForm(false);
	};

	useEffect(() => {
		runInAction(() => {
			setStatus(authStore.getStatus);

			setIncorrectCredentials(
				status === HttpStatusCode.Unauthorized ||
					status === HttpStatusCode.NotFound
			);

			setEmailValid(
				status !== HttpStatusCode.InternalServerError &&
					!authStore.getErrors.filter((error) => {
						return error.field.includes("Email");
					}).length
			);

			setPasswordValid(
				status !== HttpStatusCode.InternalServerError &&
					!authStore.getErrors.filter((error) => {
						return error.field.includes("Password");
					}).length
			);
		});
	}, [status, authStore.getStatus, authStore.getErrors]);

	const login = async (event: FormEvent<HTMLFormElement>) => {
		event.preventDefault();
		setFormSubmitted(true);
		await authStore.login(event);
	};

	return (
		<Form noValidate onSubmit={login}>
			<h2 className="text-start mt-5 mb-3">Enter your login details</h2>

			<Form.Group>
				<FloatingLabel label="Enter your email address">
					<Form.Control
						isInvalid={
							formSubmitted &&
							(incorrectCredentials || !emailValid)
						}
						isValid={formSubmitted && emailValid}
						name="Email"
						placeholder="Enter your email address"
						type="email"
					/>
				</FloatingLabel>
				<Form.Text>
					<DisplayApiErrors
						errors={authStore.getErrors}
						filter="Email"
					/>
				</Form.Text>
			</Form.Group>

			<Form.Group>
				<FloatingLabel label="Enter your password">
					<Form.Control
						isInvalid={
							formSubmitted &&
							(incorrectCredentials || !passwordValid)
						}
						isValid={formSubmitted && passwordValid}
						name="Password"
						placeholder="Enter your password"
						type="password"
					/>
				</FloatingLabel>
				<Form.Text>
					<DisplayApiErrors
						errors={authStore.getErrors}
						filter="Password"
					/>
				</Form.Text>
			</Form.Group>

			{incorrectCredentials && (
				<Form.Group>
					<Form.Text>
						<ul>
							<li className="text-danger">
								Incorrect e-mail or password.
							</li>
						</ul>
					</Form.Text>
				</Form.Group>
			)}

			{status === HttpStatusCode.InternalServerError && (
				<Form.Group>
					<Form.Text>
						<ul>
							<li className="text-danger">
								A network error has occurred. Please try again
								later.
							</li>
						</ul>
					</Form.Text>
				</Form.Group>
			)}

			<div className="text-center mt-3">
				<Button
					className="w-50 text-white"
					type="submit"
					variant="success"
				>
					Login
				</Button>

				<p className="mt-2">
					Don't have an account?&nbsp;
					<a
						className="text-decoration-none"
						onClick={showLogin}
						type="button"
					>
						Sign up!
					</a>
				</p>
			</div>
		</Form>
	);
};

export default observer(LoginForm);
