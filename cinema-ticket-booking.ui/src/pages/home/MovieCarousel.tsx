import { runInAction } from "mobx";
import { Carousel } from "react-bootstrap";
import type IMovieCarouselProps from "../common/interface/IMovieCarouselProps";

const MovieCarousel = ({ carouselMovies }: IMovieCarouselProps) => {
	return (
		<Carousel controls={false} fade indicators={false} interval={7_500}>
			{runInAction(() => {
				return carouselMovies.map((movie) => {
					return (
						<Carousel.Item key={movie.id}>
							<img
								alt={movie.title}
								className="w-100"
								src={movie.backdropUrl}
							/>
							<Carousel.Caption id="movie-carousel-caption">
								<div id="movie-carousel-overlay" />
								{movie.tagline}
							</Carousel.Caption>
						</Carousel.Item>
					);
				});
			})}
		</Carousel>
	);
};

export default MovieCarousel;
