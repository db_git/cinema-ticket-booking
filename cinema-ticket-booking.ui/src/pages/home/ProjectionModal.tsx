import { runInAction } from "mobx";
import { observer } from "mobx-react-lite";
import { useEffect, useState } from "react";
import { Alert, Button, ButtonGroup, Col, Modal, Row } from "react-bootstrap";
import "react-calendar/dist/Calendar.css";
// eslint-disable-next-line import/no-named-as-default
import Calendar from "react-calendar";
import { Link } from "react-router-dom";
import { useStore } from "../../stores/common/StoreProvider";
import DateTimeParser from "../../utils/DateTimeParser";
import ApiRequest from "../../utils/enum/ApiRequest";
import Pages from "../../utils/enum/Pages";
import type Projection from "../../utils/type/Projection";
import type IProjectionModalProps from "../common/interface/IProjectionModalProps";
import ReservationModal from "./ReservationModal";

const ProjectionModal = ({
	setShowMovieModal,
	setShowProjectionModal,
	showProjectionModal,
	movie
}: IProjectionModalProps) => {
	const { authStore, projectionStore } = useStore();
	const [projectionTimes, setProjectionTimes] =
		useState<Map<string, string>>();
	const [selectedProjectionId, setSelectedProjectionId] = useState("");
	const [showReservationModal, setShowReservationModal] = useState(false);
	const [showNotLoggedInAlert, setShowNotLoggedInAlert] = useState(false);
	const [showReservationSuccessAlert, setShowReservationSuccessAlert] =
		useState(false);

	useEffect(() => {
		projectionStore.setRequest(ApiRequest.Search, movie.id);
		projectionStore.setRequest(ApiRequest.Size, 50);
		projectionStore.setRequest(ApiRequest.Page, 1);
		projectionStore.setRequest(ApiRequest.Sort, 1);
		projectionStore.get().then();

		return () => {
			setSelectedProjectionId("");
			setProjectionTimes(new Map<string, string>());
		};
	}, [movie, projectionStore]);

	const close = () => {
		setShowReservationSuccessAlert(false);
		setShowNotLoggedInAlert(false);
		setShowProjectionModal(false);
	};

	const closeProjectionModal = () => {
		setShowMovieModal(true);
		setShowProjectionModal(false);
		setShowReservationSuccessAlert(false);
	};

	const showReservations = (id: string) => {
		runInAction(() => {
			if (authStore.isLoggedIn && !authStore.isAdmin) {
				setShowNotLoggedInAlert(false);
				setSelectedProjectionId(id);
				setShowReservationModal(true);
				setShowProjectionModal(false);
			} else {
				setShowNotLoggedInAlert(true);
			}
		});
	};

	const shouldDateBeSelected = (date: Date): boolean => {
		const dateTimeParser = new DateTimeParser(date);
		const projectionDate = dateTimeParser.getDateOnly();

		return runInAction(() => {
			if (Array.isArray(projectionStore.getResponse)) {
				return projectionStore.getResponse.some((projection) => {
					return (
						dateTimeParser
							.setNewDate(projection.time)
							.addHours(-24)
							.getDateOnly() === projectionDate &&
						projectionDate >=
							dateTimeParser.setNewDate(new Date()).getDateOnly()
					);
				});
			} else if (projectionStore.getResponse) {
				return (
					dateTimeParser
						.setNewDate(projectionStore.getResponse.time)
						.addHours(-24)
						.getDateOnly() === projectionDate &&
					projectionDate >=
						dateTimeParser.setNewDate(new Date()).getDateOnly()
				);
			}

			return false;
		});
	};

	const getProjectionTimes = (date: Date) => {
		const dateTimeParser = new DateTimeParser(date);
		const projectionDate = dateTimeParser.getDateOnly();
		const dates: Map<string, string> = new Map<string, string>();

		runInAction(() => {
			let filteredDates: Projection[] = [];

			if (Array.isArray(projectionStore.getResponse)) {
				filteredDates = projectionStore.getResponse.filter(
					(projection) => {
						return (
							dateTimeParser
								.setNewDate(projection.time)
								.addHours(-24)
								.getDateOnly() === projectionDate &&
							projectionDate >=
								dateTimeParser
									.setNewDate(new Date())
									.getDateOnly()
						);
					}
				);
			} else if (
				projectionStore.getResponse &&
				dateTimeParser
					.setNewDate(projectionStore.getResponse.time)
					.addHours(-24)
					.getDateOnly() === projectionDate &&
				projectionDate >=
					dateTimeParser.setNewDate(new Date()).getDateOnly()
			) {
				filteredDates.push(projectionStore.getResponse);
			}

			for (const projection of filteredDates) {
				dates.set(
					projection.id,
					dateTimeParser.setNewDate(projection.time).getTimeOnly() +
						" - " +
						dateTimeParser
							.setNewDate(projection.time)
							.addMinutes(movie.duration)
							.getTimeOnly() +
						" (" +
						projection.hall.name +
						")"
				);
			}
		});
		setProjectionTimes(dates);
	};

	const generateProjectionTimes = (): JSX.Element | JSX.Element[] => {
		if (projectionTimes !== undefined && projectionTimes.size !== 0) {
			const times: JSX.Element[] = [];
			times.push(
				<p className="fs-5" key="select-time">
					Select time to make a reservation.
				</p>
			);

			for (const [id, projectionTime] of projectionTimes) {
				times.push(
					<Button
						className="mx-auto btn-outline-warning"
						key={projectionTime}
						onClick={() => {
							showReservations(id);
						}}
						style={{
							minWidth: "15rem"
						}}
						variant="dark"
					>
						{projectionTime}
					</Button>
				);
			}

			return times;
		}

		return (
			<p className="fs-5" key="select-date">
				Select a date to view available projections.
			</p>
		);
	};

	return (
		<>
			<ReservationModal
				projectionId={selectedProjectionId}
				setShowProjectionModal={setShowProjectionModal}
				setShowReservationModal={setShowReservationModal}
				setShowReservationSuccessAlert={setShowReservationSuccessAlert}
				showReservationModal={showReservationModal}
			/>
			<Modal
				backdrop="static"
				centered
				fullscreen="sm-down"
				show={showProjectionModal}
			>
				<Modal.Header>
					<Modal.Title>
						Available projections for {movie.title}
					</Modal.Title>
				</Modal.Header>
				<Modal.Body>
					<Row>
						<Col>
							<Calendar
								className="mx-auto bg-dark text-warning"
								maxDetail="month"
								onChange={(date: Date) => {
									setProjectionTimes(
										new Map<string, string>()
									);
									if (shouldDateBeSelected(date)) {
										getProjectionTimes(date);
									}
								}}
								tileClassName={({ date }) => {
									if (
										shouldDateBeSelected(date) &&
										date >= new Date()
									) {
										return "react-calendar__tile--active";
									} else if (shouldDateBeSelected(date)) {
										return "disabled";
									}

									return "";
								}}
								value={null}
							/>
						</Col>
					</Row>
					<Row className="p-3" />
					<Row>
						<div className="text-center d-grid gap-2">
							{generateProjectionTimes()}
						</div>
					</Row>
				</Modal.Body>
				{showNotLoggedInAlert && (
					<Alert
						dismissible
						onClose={() => {
							setShowNotLoggedInAlert(false);
						}}
						variant="danger"
					>
						You must be logged in!
					</Alert>
				)}
				{showReservationSuccessAlert && (
					<Alert
						dismissible
						onClose={() => {
							setShowReservationSuccessAlert(false);
						}}
						variant="success"
					>
						Reservation successful. You can view all your
						reservations on&nbsp;
						<Link
							className="d-inline-block"
							to={Pages.Reservations}
						>
							this page
						</Link>
						.
					</Alert>
				)}
				<Modal.Footer>
					<ButtonGroup>
						<Button
							onClick={closeProjectionModal}
							variant="outline-warning"
						>
							Back
						</Button>
						<Button
							className="float-end border-0 text-decoration-underline"
							onClick={close}
							variant="outline-danger"
						>
							Close
						</Button>
					</ButtonGroup>
				</Modal.Footer>
			</Modal>
		</>
	);
};

export default observer(ProjectionModal);
