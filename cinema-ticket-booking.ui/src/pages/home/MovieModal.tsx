import { runInAction } from "mobx";
import { observer } from "mobx-react-lite";
import { useEffect, useState } from "react";
import { Button, Col, Modal, Row, Image, Stack } from "react-bootstrap";
import {
	BsPeopleFill,
	BsPersonCircle,
	BsCameraVideoFill,
	BsCameraReelsFill
} from "react-icons/bs";
import { FaCalendarAlt, FaClock, FaTheaterMasks, FaInfo } from "react-icons/fa";
import { RiParentFill } from "react-icons/ri";
import { useStore } from "../../stores/common/StoreProvider";
import type Movie from "../../utils/type/Movie";
import type IMovieModalProps from "../common/interface/IMovieModalProps";
import ProjectionModal from "./ProjectionModal";
import TrailerModal from "./TrailerModal";

import "../common/styles/Movie.scss";

const MovieModal = ({
	showMovieModal,
	showTrailerModal,
	movieId,
	setShowMovieModal,
	setShowTrailerModal,
	setShowProjectionModal,
	showProjectionModal
}: IMovieModalProps) => {
	const { movieGetStore } = useStore();
	const [movie, setMovie] = useState<Movie>();

	useEffect(() => {
		if (showMovieModal)
			movieGetStore.get(movieId).then(() => {
				runInAction(() => {
					if (
						movieGetStore.getResponse !== undefined &&
						!Array.isArray(movieGetStore.getResponse)
					)
						setMovie(movieGetStore.getResponse);
				});
			});
	}, [showMovieModal, movieId, movieGetStore]);

	const close = () => {
		setShowMovieModal(false);
	};

	const showTrailer = () => {
		setShowMovieModal(false);
		setShowTrailerModal(true);
	};

	const showProjections = () => {
		setShowMovieModal(false);
		setShowProjectionModal(true);
	};

	if (!movie) return null;

	return (
		<>
			<TrailerModal
				movie={movie}
				setShowMovieModal={setShowMovieModal}
				setShowTrailerModal={setShowTrailerModal}
				showTrailerModal={showTrailerModal}
			/>
			<ProjectionModal
				movie={movie}
				setShowMovieModal={setShowMovieModal}
				setShowProjectionModal={setShowProjectionModal}
				showProjectionModal={showProjectionModal}
			/>
			<Modal
				backdrop="static"
				centered
				dialogClassName="movie-modal"
				fullscreen="lg-down"
				show={showMovieModal}
			>
				<span id="movie-modal-overlay" />
				<span
					style={{
						backgroundImage: `url(${movie.backdropUrl})`,
						backgroundPosition: "center center",
						backgroundRepeat: "no-repeat",
						backgroundSize: "cover",
						height: "100%",
						objectFit: "cover",
						position: "absolute",
						width: "100%"
					}}
				/>
				<Modal.Body style={{ zIndex: 1 }}>
					<h3 className="text-center fw-bold">{movie.tagline}</h3>
					<hr />
					<Row>
						<Col className="text-center" lg>
							<Image
								alt={movie.title}
								className="movie-modal-poster"
								fluid
								rounded
								src={movie.posterUrl}
								thumbnail
							/>
						</Col>
						<Col lg="auto">&nbsp;</Col>
						<Col lg>
							<Stack className="text-start " gap={2}>
								<h4 className="text-info">
									<FaInfo />
									&nbsp; Summary:
								</h4>
								<p className="fw-bold">{movie.summary}</p>

								<h4 className="text-info">
									<BsPeopleFill />
									&nbsp; Actors:
								</h4>
								<p className="fw-bold">
									{movie.actors
										.map((actor) => {
											return [
												actor.firstName,
												actor.lastName
											].join(" ");
										})
										.join(", ")}
								</p>

								<h4 className="text-info">
									<BsPersonCircle />
									&nbsp; Director
									{movie.directors.length > 1 && "s"}:
								</h4>
								<p className="fw-bold">
									{movie.directors
										.map((director) => {
											return [
												director.firstName,
												director.lastName
											].join(" ");
										})
										.join(", ")}
								</p>

								<Row>
									<Col>
										<h4 className="text-info">
											<FaCalendarAlt />
											&nbsp; Release year:
										</h4>
										<p className="fw-bold">{movie.year}</p>
									</Col>

									<Col>
										<h4 className="text-info">
											<FaClock />
											&nbsp; Duration:
										</h4>
										<p className="fw-bold">
											{Math.floor(movie.duration / 60) +
												"h " +
												(movie.duration % 60) +
												"m"}
										</p>
									</Col>
								</Row>

								<Row>
									<Col>
										<h4 className="text-info">
											<FaTheaterMasks />
											&nbsp; Genre
											{movie.genres.length > 1 && "s"}:
										</h4>
										<p className="fw-bold">
											{movie.genres
												.map((genre) => {
													return genre.name;
												})
												.join(", ")}
										</p>
									</Col>

									<Col>
										<h4 className="text-info">
											<RiParentFill />
											&nbsp; Rating:
										</h4>
										<p className="fw-bold">
											{movie.ratings.map((rating) => {
												return rating.type;
											})}
										</p>
									</Col>
								</Row>

								<Row>
									<Col>
										<h4 className="text-info">
											<BsCameraVideoFill />
											&nbsp;&nbsp;
											<Button
												onClick={showTrailer}
												variant="outline-primary"
											>
												Watch trailer
											</Button>
										</h4>
									</Col>

									<Col>
										<h4 className="text-info">
											<BsCameraReelsFill />
											&nbsp;&nbsp;
											<Button
												onClick={showProjections}
												variant="outline-success"
											>
												View projections
											</Button>
										</h4>
									</Col>
								</Row>
							</Stack>
						</Col>
					</Row>
					<hr />
					<Button
						className="float-end border-0 text-decoration-underline"
						onClick={close}
						variant="outline-danger"
					>
						Close
					</Button>
				</Modal.Body>
			</Modal>
		</>
	);
};

export default observer(MovieModal);
