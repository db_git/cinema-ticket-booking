import { observer } from "mobx-react-lite";
import { Button, ButtonGroup, Modal, Ratio } from "react-bootstrap";
import type ITrailerModalProps from "../common/interface/ITrailerModalProps";

const TrailerModal = ({
	showTrailerModal,
	setShowTrailerModal,
	setShowMovieModal,
	movie
}: ITrailerModalProps) => {
	const backToMovie = () => {
		setShowMovieModal(true);
		setShowTrailerModal(false);
	};

	const close = () => {
		setShowTrailerModal(false);
	};

	return (
		<Modal
			backdrop="static"
			centered
			dialogClassName="movie-modal"
			show={showTrailerModal}
		>
			<Modal.Header>
				<Modal.Title>{movie.title} Trailer</Modal.Title>{" "}
			</Modal.Header>
			<Modal.Body>
				<Ratio aspectRatio={9 / 16}>
					<iframe
						allowFullScreen
						src={movie.trailerUrl}
						title={movie.title + "Trailer"}
					/>
				</Ratio>
			</Modal.Body>
			<Modal.Footer>
				<ButtonGroup>
					<Button onClick={backToMovie} variant="warning">
						Back
					</Button>
					<Button onClick={close} variant="outline-danger">
						Close
					</Button>
				</ButtonGroup>
			</Modal.Footer>
		</Modal>
	);
};

export default observer(TrailerModal);
