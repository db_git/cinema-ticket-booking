import { runInAction } from "mobx";
import { observer } from "mobx-react-lite";
import { useEffect, useState } from "react";
import { Button, Row, Spinner } from "react-bootstrap";
import ApiRequest from "../../utils/enum/ApiRequest";
import HttpStatusCode from "../../utils/enum/HttpStatusCode";
import type Movie from "../../utils/type/Movie";
import type IMovieStore from "../common/interface/IMovieStore";
import MovieCard from "./MovieCard";

const MovieAlbum = ({ store }: IMovieStore) => {
	const [movies, setMovies] = useState<Movie[]>([]);
	const [totalPages, setTotalPages] = useState(0);
	const [page, setPage] = useState(1);
	const size = 30;

	useEffect(() => {
		store.setRequest(ApiRequest.Page, page);
		store.setRequest(ApiRequest.Size, size);
		store.setRequest(ApiRequest.Sort, 4);

		store.get().then(() => {
			runInAction(() => {
				setTotalPages(Math.ceil(store.getRecords / size));
				if (Array.isArray(store.getResponse)) {
					setMovies((current) => {
						return current.concat(store.getResponse as Movie[]);
					});
				}
			});
		});
	}, [store, page]);

	if (movies.length) {
		return (
			<>
				<Row lg={4} md={3} sm={2} xl={5} xs={2} xxl={5}>
					{movies.map((movie) => {
						return <MovieCard key={movie.id} movie={movie} />;
					})}
				</Row>
				<div className="m-3 text-center">
					{page + 1 <= totalPages && (
						<Button
							className="w-75"
							id="movie-load-more"
							onClick={() => {
								setPage(page + 1);
							}}
							variant="info"
						>
							Load more
						</Button>
					)}
				</div>
			</>
		);
	} else if (
		(store.getRequest.search === undefined &&
			store.getStatus.get !== HttpStatusCode.NoContent) ||
		!movies.length
	) {
		return (
			<>
				<Spinner animation="border" as="span" />
				<h4 className="ms-4 d-inline-block">Loading movies...</h4>
			</>
		);
	} else {
		return <h3>Nothing found!</h3>;
	}
};

export default observer(MovieAlbum);
