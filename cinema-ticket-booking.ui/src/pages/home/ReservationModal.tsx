import { runInAction } from "mobx";
import { useEffect, useState } from "react";
import { Button, ButtonGroup, Modal, Col } from "react-bootstrap";
import { useStore } from "../../stores/common/StoreProvider";
import type Projection from "../../utils/type/Projection";
import type IReservationModalProps from "../common/interface/IReservationModalProps";
import ReservationForm from "../reservation/ReservationForm";

const ReservationModal = ({
	projectionId,
	showReservationModal,
	setShowProjectionModal,
	setShowReservationModal,
	setShowReservationSuccessAlert
}: IReservationModalProps) => {
	const { projectionStore } = useStore();
	const [projection, setProjection] = useState<Projection>();

	useEffect(() => {
		runInAction(() => {
			projectionStore.get(projectionId).then(() => {
				if (
					projectionStore.getResponse !== undefined &&
					!Array.isArray(projectionStore.getResponse)
				) {
					setProjection(projectionStore.getResponse);
				}
			});
		});

		return () => {
			setProjection(undefined);
		};
	}, [projectionStore, projectionId]);

	const closeReservationModal = () => {
		setShowProjectionModal(true);
		setShowReservationModal(false);
	};

	const closeReservationModalSuccess = () => {
		setShowReservationSuccessAlert(true);
		setShowProjectionModal(true);
		setShowReservationModal(false);
	};

	const close = () => {
		setShowReservationModal(false);
	};

	if (typeof projection === "undefined") return null;

	return (
		<Modal
			backdrop="static"
			centered
			dialogClassName="movie-modal"
			show={showReservationModal}
		>
			<Modal.Header>
				<Modal.Title>Reservation</Modal.Title>
			</Modal.Header>
			<Modal.Body>
				<ReservationForm
					projection={projection}
					toggleCreateModal={closeReservationModalSuccess}
				/>
			</Modal.Body>
			<Modal.Footer>
				<Col>
					<Button
						className="float-start"
						form="reservationForm"
						type="submit"
						variant="success"
					>
						Make reservation
					</Button>
					<ButtonGroup className="float-end">
						<Button
							onClick={closeReservationModal}
							variant="outline-warning"
						>
							Back
						</Button>
						<Button
							className="border-0 text-decoration-underline"
							onClick={close}
							variant="outline-danger"
						>
							Close
						</Button>
					</ButtonGroup>
				</Col>
			</Modal.Footer>
		</Modal>
	);
};

export default ReservationModal;
