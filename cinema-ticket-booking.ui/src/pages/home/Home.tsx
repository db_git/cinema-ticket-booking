import { observer } from "mobx-react-lite";
import { useEffect } from "react";
import { Container } from "react-bootstrap";
import { useStore } from "../../stores/common/StoreProvider";
import HttpStatusCode from "../../utils/enum/HttpStatusCode";
import type IMovieCarouselProps from "../common/interface/IMovieCarouselProps";
import MovieAlbum from "./MovieAlbum";
import MovieCarousel from "./MovieCarousel";

import "../common/styles/Movie.scss";

const Home = ({ carouselMovies }: IMovieCarouselProps) => {
	const { movieStore } = useStore();

	useEffect(() => {
		document.title = "Home";
	}, []);

	if (movieStore.getStatus.get === HttpStatusCode.InternalServerError) {
		return (
			<Container>
				<div className="mt-3">
					<h1 className="text-center">
						A network error has occurred. Please try again later.
					</h1>
				</div>
			</Container>
		);
	}

	return (
		<>
			<MovieCarousel carouselMovies={carouselMovies} />
			<h1 className="mt-3 text-center">All movies</h1>
			<Container>
				<div className="mt-3">
					<MovieAlbum store={movieStore} />
				</div>
			</Container>
		</>
	);
};

export default observer(Home);
