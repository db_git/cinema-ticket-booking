import { observer } from "mobx-react-lite";
import { useState } from "react";
import { Card, Col } from "react-bootstrap";
import type IMovieCardProps from "../common/interface/IMovieCardProps";
import MovieModal from "./MovieModal";

const MovieCard = ({ movie }: IMovieCardProps) => {
	const [showMovieModal, setShowMovieModal] = useState(false);
	const [showTrailerModal, setShowTrailerModal] = useState(false);
	const [showProjectionModal, setShowProjectionModal] = useState(false);

	const handleOnClick = () => {
		setShowMovieModal(true);
	};

	return (
		<>
			<MovieModal
				movieId={movie.id}
				setShowMovieModal={setShowMovieModal}
				setShowProjectionModal={setShowProjectionModal}
				setShowTrailerModal={setShowTrailerModal}
				showMovieModal={showMovieModal}
				showProjectionModal={showProjectionModal}
				showTrailerModal={showTrailerModal}
			/>
			<Col className="p-2">
				<Card className="mb-4 bg-transparent border-0 movie-card">
					<Card.Img
						className="movie-card-img"
						onClick={handleOnClick}
						src={movie.posterUrl}
					/>
					<Card.Body className="bg-transparent">
						<hr className="m-0 mb-1" />
						<Card.Text className="text-center">
							<span
								className="movie-card-title py-2"
								onClick={handleOnClick}
							>
								{movie.title}
							</span>
						</Card.Text>
					</Card.Body>
				</Card>
			</Col>
		</>
	);
};

export default observer(MovieCard);
