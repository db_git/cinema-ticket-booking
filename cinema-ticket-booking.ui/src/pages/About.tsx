import { useEffect } from "react";
import { Container } from "react-bootstrap";

const About = () => {
	useEffect(() => {
		document.title = "About";
	}, []);

	return (
		<Container fluid>
			<h3 className="m-5">&#169; Denis Bošnjaković - 2021.</h3>
		</Container>
	);
};

export default About;
