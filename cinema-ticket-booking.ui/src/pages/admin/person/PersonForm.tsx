import { runInAction } from "mobx";
import { useEffect, useState } from "react";
import { Col, FloatingLabel, Form, Row } from "react-bootstrap";
import DisplayApiErrors from "../../../utils/DisplayApiErrors";
import type Person from "../../../utils/type/Person";
import type IFormProps from "../../common/interface/IFormProps";

const PersonForm = ({
	data,
	errors,
	formCreate,
	formUpdate
}: IFormProps<Person>) => {
	const [firstNameValid, setFirstNameValid] = useState(false);
	const [lastNameValid, setLastNameValid] = useState(false);
	const [formSubmitted, setFormSubmitted] = useState(false);

	useEffect(() => {
		runInAction(() => {
			setFirstNameValid(
				!errors.filter((error) => {
					return error.field === "FirstName";
				}).length
			);
			setLastNameValid(
				!errors.filter((error) => {
					return error.field === "LastName";
				}).length
			);
		});
	}, [errors]);

	return (
		<Form
			id={data ? "updateForm" : "createForm"}
			noValidate
			onSubmit={(event) => {
				event.preventDefault();
				setFormSubmitted(true);
				const form = new FormData(event.currentTarget);
				runInAction(() => {
					if (data) formUpdate(data.id, form);
					else formCreate(form);
				});
			}}
		>
			<Row>
				<Col>
					<Form.Group>
						<FloatingLabel label="First name">
							<Form.Control
								defaultValue={runInAction(() => {
									return data?.firstName;
								})}
								isInvalid={formSubmitted && !firstNameValid}
								isValid={formSubmitted && firstNameValid}
								name="FirstName"
								placeholder="First Name"
								type="text"
							/>
						</FloatingLabel>

						<Form.Text>
							<DisplayApiErrors
								errors={errors}
								filter="FirstName"
							/>
						</Form.Text>
					</Form.Group>
				</Col>

				<Col>
					<Form.Group>
						<FloatingLabel label="Last name">
							<Form.Control
								defaultValue={runInAction(() => {
									return data?.lastName;
								})}
								isInvalid={formSubmitted && !lastNameValid}
								isValid={formSubmitted && lastNameValid}
								name="LastName"
								placeholder="LastName"
								type="text"
							/>
						</FloatingLabel>

						<Form.Text>
							<DisplayApiErrors
								errors={errors}
								filter="LastName"
							/>
						</Form.Text>
					</Form.Group>
				</Col>
			</Row>
		</Form>
	);
};

export default PersonForm;
