import { observer } from "mobx-react-lite";
import { useEffect } from "react";
import { Container } from "react-bootstrap";
import { useStore } from "../../../stores/common/StoreProvider";
import SortOrder from "../../../utils/const/SortOrder";
import HttpStatusCode from "../../../utils/enum/HttpStatusCode";
import type Person from "../../../utils/type/Person";
import Filter from "../../common/filter/Filter";
import type { Columns } from "../../common/interface/AdminTable";
import AdminPage from "../common/AdminPage";

const AdminPerson = () => {
	const { personStore } = useStore();

	useEffect(() => {
		document.title = "Admin | Person";
	}, []);

	if (personStore.getStatus.get === HttpStatusCode.InternalServerError) {
		return (
			<Container>
				<div className="mt-3">
					<h1 className="text-center">
						A network error has occurred. Please try again later.
					</h1>
				</div>
			</Container>
		);
	}

	const columns: Array<Columns<Person, keyof Person>> = [
		{
			display: "First name",
			key: "firstName"
		},
		{
			display: "Last name",
			key: "lastName"
		}
	];

	return (
		<Container fluid>
			<h1 className="text-center mt-2">People</h1>
			<Filter sortItems={SortOrder.AdminPerson} store={personStore} />
			<AdminPage columns={columns} store={personStore} />
		</Container>
	);
};

export default observer(AdminPerson);
