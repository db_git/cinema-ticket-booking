import { observer } from "mobx-react-lite";
import { Button, ButtonGroup, Table } from "react-bootstrap";
import type { IAdminTableProps } from "../../common/interface/AdminTable";
import type IEntity from "../../common/interface/IEntity";

import "../../common/styles/AdminTable.scss";

const AdminTable = <T extends IEntity, K extends keyof T>({
	body,
	headers,
	setModalData,
	toggleUpdateModal,
	toggleDeleteModal
}: IAdminTableProps<T, K>): JSX.Element => {
	return (
		<Table
			bordered
			className="mt-3 admin-table"
			hover
			responsive
			size="md"
			variant="dark"
		>
			<thead className="table-secondary">
				<tr>
					{headers.map((header) => {
						return (
							<th
								className="admin-table-row"
								key={header.display}
							>
								{header.display}
							</th>
						);
					})}
					<th className="admin-table-row" key="Action">
						Action
					</th>
				</tr>
			</thead>
			<tbody>
				{body.map((row) => {
					return (
						<tr key={row.id}>
							{headers.map((header, index) => {
								if (
									typeof row[header.key] === "object" &&
									header.secondKey &&
									header.thirdKey
								) {
									return (
										<td
											className="admin-table-row"
											key={index}
										>
											{
												row[header.key][
													header.secondKey as keyof unknown
												][
													header.thirdKey as keyof unknown
												]
											}
										</td>
									);
								} else if (
									typeof row[header.key] === "object" &&
									header.secondKey
								) {
									return (
										<td
											className="admin-table-row"
											key={index}
										>
											{
												row[header.key][
													header.secondKey as keyof unknown
												]
											}
										</td>
									);
								} else {
									return (
										<td
											className="admin-table-row"
											key={index}
										>
											{row[header.key]}
										</td>
									);
								}
							})}
							<td className="admin-table-row d-flex justify-content-end">
								<ButtonGroup className="me-5">
									{toggleUpdateModal && (
										<Button
											onClick={() => {
												setModalData(row);
												toggleUpdateModal();
											}}
											variant="outline-warning"
										>
											Edit
										</Button>
									)}
									<Button
										onClick={() => {
											setModalData(row);
											toggleDeleteModal();
										}}
										variant="outline-danger"
									>
										Delete
									</Button>
								</ButtonGroup>
							</td>
						</tr>
					);
				})}
			</tbody>
		</Table>
	);
};

export default observer(AdminTable);
