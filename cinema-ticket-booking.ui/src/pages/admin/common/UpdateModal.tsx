import { Button, ButtonGroup, Modal } from "react-bootstrap";
import type IModalProps from "../../common/interface/IModalProps";

import "../../common/styles/AdminModal.scss";

const UpdateModal = ({ show, children, toggle }: IModalProps) => {
	return (
		<Modal
			backdrop="static"
			centered
			dialogClassName="admin-modal"
			fullscreen="lg-down"
			onHide={toggle}
			show={show}
		>
			<Modal.Header className="fw-bold" closeButton>
				Edit
			</Modal.Header>
			<Modal.Body>{children}</Modal.Body>
			<Modal.Footer>
				<ButtonGroup>
					<Button form="updateForm" type="submit" variant="success">
						Edit
					</Button>
					<Button
						className="text-white"
						onClick={toggle}
						variant="outline-secondary"
					>
						Close
					</Button>
				</ButtonGroup>
			</Modal.Footer>
		</Modal>
	);
};

export default UpdateModal;
