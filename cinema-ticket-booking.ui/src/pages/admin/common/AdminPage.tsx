import { observer } from "mobx-react-lite";
import { useEffect, useState } from "react";
import { Button } from "react-bootstrap";
import HttpMethod from "../../../utils/enum/HttpMethod";
import HttpStatusCode from "../../../utils/enum/HttpStatusCode";
import type IAdminPageProps from "../../common/interface/IAdminPageProps";
import type IEntity from "../../common/interface/IEntity";
import Pagination from "../../common/pagination/Pagination";
import AdminForm from "./AdminForm";
import AdminTable from "./AdminTable";
import CreateModal from "./CreateModal";
import DeleteModal from "./DeleteModal";
import UpdateModal from "./UpdateModal";

const AdminPage = <T extends IEntity>({
	showCreate,
	showUpdate,
	store,
	columns
}: IAdminPageProps<T>) => {
	const [showCreateModal, setShowCreateModal] = useState(false);
	const [showUpdateModal, setShowUpdateModal] = useState(false);
	const [showDeleteModal, setShowDeleteModal] = useState(false);
	const [modalData, setModalData] = useState<T>();

	useEffect(() => {
		store.get().then();
	}, [store]);

	const toggleCreateModal = () => {
		setShowCreateModal(!showCreateModal);
		if (!showCreateModal) {
			store.setErrors(HttpMethod.Post, []);
			store.setStatus(HttpMethod.Post, 0);
		}
	};

	const toggleUpdateModal = () => {
		setShowUpdateModal(!showUpdateModal);
		if (!showUpdateModal) {
			store.setErrors(HttpMethod.Put, []);
			store.setStatus(HttpMethod.Put, 0);
		}
	};

	const toggleDeleteModal = () => {
		setShowDeleteModal(!showDeleteModal);
	};

	if (Array.isArray(store.getResponse)) {
		return (
			<>
				<CreateModal show={showCreateModal} toggle={toggleCreateModal}>
					<AdminForm
						store={store}
						toggleCreateModal={toggleCreateModal}
					/>
				</CreateModal>

				<AdminTable
					body={store.getResponse}
					headers={columns}
					setModalData={setModalData}
					toggleDeleteModal={toggleDeleteModal}
					toggleUpdateModal={
						showUpdate ? toggleUpdateModal : undefined
					}
				/>

				{showCreate && (
					<Button
						onClick={toggleCreateModal}
						type="button"
						variant="outline-success"
					>
						Add new
					</Button>
				)}
				<Pagination store={store} />

				{modalData && (
					<>
						<DeleteModal
							id={modalData.id}
							show={showDeleteModal}
							storeDelete={store.delete}
							toggleDeleteModal={toggleDeleteModal}
						/>

						<UpdateModal
							show={showUpdateModal}
							toggle={toggleUpdateModal}
						>
							<AdminForm
								data={modalData}
								store={store}
								toggleUpdateModal={
									showUpdate ? toggleUpdateModal : undefined
								}
							/>
						</UpdateModal>
					</>
				)}
			</>
		);
	} else if (
		store.getRequest.search === undefined &&
		store.getStatus.get !== HttpStatusCode.NoContent
	) {
		return <h3>Loading...</h3>;
	} else {
		return (
			<>
				<CreateModal show={showCreateModal} toggle={toggleCreateModal}>
					<AdminForm
						store={store}
						toggleCreateModal={toggleCreateModal}
					/>
				</CreateModal>

				<h3 className="my-3">Nothing found!</h3>
				{showCreate && (
					<Button
						className="border border-warning"
						onClick={toggleCreateModal}
						type="button"
						variant="dark"
					>
						Add new
					</Button>
				)}
			</>
		);
	}
};

AdminPage.defaultProps = {
	showCreate: true,
	showUpdate: true
};

export default observer(AdminPage);
