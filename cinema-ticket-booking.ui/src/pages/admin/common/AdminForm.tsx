import { runInAction } from "mobx";
import { observer } from "mobx-react-lite";
import HttpStatusCode from "../../../utils/enum/HttpStatusCode";
import type IAdminFormProps from "../../common/interface/IAdminFormProps";
import type IEntity from "../../common/interface/IEntity";
import CinemaForm from "../cinema/CinemaForm";
import GenreForm from "../genre/GenreForm";
import HallForm from "../hall/HallForm";
import MovieForm from "../movie/MovieForm";
import PersonForm from "../person/PersonForm";
import ProjectionForm from "../projection/ProjectionForm";
import RatingForm from "../rating/RatingForm";
import SeatForm from "../seat/SeatForm";

const AdminForm = <T extends IEntity>({
	store,
	data,
	toggleCreateModal,
	toggleUpdateModal
}: IAdminFormProps<T>): JSX.Element | null => {
	const formCreate = (form: FormData) => {
		store.create(form).then(() => {
			runInAction(() => {
				if (
					store.getStatus.post === HttpStatusCode.Created &&
					toggleCreateModal
				) {
					toggleCreateModal();
				}
			});
		});
	};

	const formUpdate = (id: string, form: FormData) => {
		store.update(id, form).then(() => {
			runInAction(() => {
				if (
					store.getStatus.put === HttpStatusCode.OK &&
					toggleUpdateModal
				) {
					toggleUpdateModal();
				}
			});
		});
	};

	switch (store.constructor.name) {
		case "CinemaStore":
			return (
				<CinemaForm
					data={data}
					errors={data ? store.getErrors.put : store.getErrors.post}
					formCreate={formCreate}
					formUpdate={formUpdate}
				/>
			);

		case "GenreStore":
			return (
				<GenreForm
					data={data}
					errors={data ? store.getErrors.put : store.getErrors.post}
					formCreate={formCreate}
					formUpdate={formUpdate}
				/>
			);

		case "HallStore":
			return (
				<HallForm
					data={data}
					errors={data ? store.getErrors.put : store.getErrors.post}
					formCreate={formCreate}
					formUpdate={formUpdate}
				/>
			);

		case "MovieStore":
			return (
				<MovieForm
					data={data}
					errors={data ? store.getErrors.put : store.getErrors.post}
					formCreate={formCreate}
					formUpdate={formUpdate}
				/>
			);

		case "PersonStore":
			return (
				<PersonForm
					data={data}
					errors={data ? store.getErrors.put : store.getErrors.post}
					formCreate={formCreate}
					formUpdate={formUpdate}
				/>
			);

		case "ProjectionStore":
			return (
				<ProjectionForm
					data={data}
					errors={data ? store.getErrors.put : store.getErrors.post}
					formCreate={formCreate}
					formUpdate={formUpdate}
				/>
			);

		case "RatingStore":
			return (
				<RatingForm
					data={data}
					errors={data ? store.getErrors.put : store.getErrors.post}
					formCreate={formCreate}
					formUpdate={formUpdate}
				/>
			);

		case "SeatStore":
			return (
				<SeatForm
					data={data}
					errors={data ? store.getErrors.put : store.getErrors.post}
					formCreate={formCreate}
					formUpdate={formUpdate}
				/>
			);

		default:
			return null;
	}
};

export default observer(AdminForm);
