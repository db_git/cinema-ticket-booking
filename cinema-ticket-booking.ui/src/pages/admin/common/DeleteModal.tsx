import { Button, ButtonGroup, Modal } from "react-bootstrap";
import type IAdminDeleteModalProps from "../../common/interface/IAdminDeleteModalProps";

import "../../common/styles/AdminModal.scss";

const DeleteModal = ({
	id,
	show,
	storeDelete,
	toggleDeleteModal
}: IAdminDeleteModalProps) => {
	const handleDelete = async () => {
		toggleDeleteModal();
		await storeDelete(id);
	};

	return (
		<Modal
			backdrop="static"
			centered
			dialogClassName="admin-modal-delete"
			fullscreen="lg-down"
			onHide={toggleDeleteModal}
			show={show}
		>
			<Modal.Header className="fw-bold" closeButton>
				Delete
			</Modal.Header>
			<Modal.Body>
				Are you sure you want to delete selected item?
			</Modal.Body>
			<Modal.Footer>
				<ButtonGroup>
					<Button onClick={handleDelete} variant="danger">
						Delete
					</Button>
					<Button
						className="text-white"
						onClick={toggleDeleteModal}
						variant="outline-secondary"
					>
						Close
					</Button>
				</ButtonGroup>
			</Modal.Footer>
		</Modal>
	);
};

export default DeleteModal;
