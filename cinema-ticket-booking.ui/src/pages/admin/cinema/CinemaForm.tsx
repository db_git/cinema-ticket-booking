import { runInAction } from "mobx";
import { useEffect, useState } from "react";
import { Col, FloatingLabel, Form, Row } from "react-bootstrap";
import DisplayApiErrors from "../../../utils/DisplayApiErrors";
import type Cinema from "../../../utils/type/Cinema";
import type IFormProps from "../../common/interface/IFormProps";

const CinemaForm = ({
	data,
	errors,
	formCreate,
	formUpdate
}: IFormProps<Cinema>) => {
	const [nameValid, setNameValid] = useState(false);
	const [cityValid, setCityValid] = useState(false);
	const [streetValid, setStreetValid] = useState(false);
	const [priceValid, setPriceValid] = useState(false);
	const [formSubmitted, setFormSubmitted] = useState(false);

	useEffect(() => {
		runInAction(() => {
			setNameValid(
				!errors.filter((error) => {
					return error.field === "Name";
				}).length
			);
			setCityValid(
				!errors.filter((error) => {
					return error.field === "City";
				}).length
			);
			setStreetValid(
				!errors.filter((error) => {
					return error.field === "Street";
				}).length
			);
			setPriceValid(
				!errors.filter((error) => {
					return error.field === "TicketPrice";
				}).length
			);
		});
	}, [errors]);

	return (
		<Form
			id={data ? "updateForm" : "createForm"}
			noValidate
			onSubmit={(event) => {
				event.preventDefault();
				setFormSubmitted(true);
				const form = new FormData(event.currentTarget);
				runInAction(() => {
					if (data) formUpdate(data.id, form);
					else formCreate(form);
				});
			}}
		>
			<Row>
				<Col>
					<Form.Group>
						<FloatingLabel label="Name">
							<Form.Control
								defaultValue={runInAction(() => {
									return data?.name;
								})}
								isInvalid={formSubmitted && !nameValid}
								isValid={formSubmitted && nameValid}
								name="Name"
								placeholder="Name"
								type="text"
							/>
						</FloatingLabel>

						<Form.Text>
							<DisplayApiErrors errors={errors} filter="Name" />
						</Form.Text>
					</Form.Group>
				</Col>

				<Col>
					<Form.Group>
						<FloatingLabel label="City">
							<Form.Control
								defaultValue={runInAction(() => {
									return data?.city;
								})}
								isInvalid={formSubmitted && !cityValid}
								isValid={formSubmitted && cityValid}
								name="City"
								placeholder="City"
								type="text"
							/>
						</FloatingLabel>

						<Form.Text>
							<DisplayApiErrors errors={errors} filter="City" />
						</Form.Text>
					</Form.Group>
				</Col>
			</Row>

			<Row>
				<Col>
					<Form.Group>
						<FloatingLabel label="Street">
							<Form.Control
								defaultValue={runInAction(() => {
									return data?.street;
								})}
								isInvalid={formSubmitted && !streetValid}
								isValid={formSubmitted && streetValid}
								name="Street"
								placeholder="Street"
								type="text"
							/>
						</FloatingLabel>

						<Form.Text>
							<DisplayApiErrors errors={errors} filter="Street" />
						</Form.Text>
					</Form.Group>
				</Col>

				<Col>
					<Form.Group>
						<FloatingLabel label="Ticket price">
							<Form.Control
								defaultValue={runInAction(() => {
									return data?.ticketPrice
										.toString()
										.replace(".", ",");
								})}
								isInvalid={formSubmitted && !priceValid}
								isValid={formSubmitted && priceValid}
								name="TicketPrice"
								placeholder="TicketPrice"
							/>
						</FloatingLabel>

						<Form.Text>
							<DisplayApiErrors
								errors={errors}
								filter="TicketPrice"
							/>
						</Form.Text>
					</Form.Group>
				</Col>
			</Row>
		</Form>
	);
};

export default CinemaForm;
