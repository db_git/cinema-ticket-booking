import { observer } from "mobx-react-lite";
import { useEffect } from "react";
import { Container } from "react-bootstrap";
import { useStore } from "../../../stores/common/StoreProvider";
import SortOrder from "../../../utils/const/SortOrder";
import HttpStatusCode from "../../../utils/enum/HttpStatusCode";
import type Cinema from "../../../utils/type/Cinema";
import Filter from "../../common/filter/Filter";
import type { Columns } from "../../common/interface/AdminTable";
import AdminPage from "../common/AdminPage";

const AdminCinema = () => {
	const { cinemaStore } = useStore();

	useEffect(() => {
		document.title = "Admin | Cinema";
	}, []);

	if (cinemaStore.getStatus.get === HttpStatusCode.InternalServerError) {
		return (
			<Container>
				<div className="mt-3">
					<h1 className="text-center">
						A network error has occurred. Please try again later.
					</h1>
				</div>
			</Container>
		);
	}

	const columns: Array<Columns<Cinema, keyof Cinema>> = [
		{
			display: "Name",
			key: "name"
		},
		{
			display: "City",
			key: "city"
		},
		{
			display: "Street",
			key: "street"
		},
		{
			display: "Price",
			key: "ticketPrice"
		}
	];

	return (
		<Container fluid>
			<h1 className="text-center mt-2">Cinemas</h1>
			<Filter sortItems={SortOrder.AdminCinema} store={cinemaStore} />
			<AdminPage columns={columns} store={cinemaStore} />
		</Container>
	);
};

export default observer(AdminCinema);
