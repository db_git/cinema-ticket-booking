import { observer } from "mobx-react-lite";
import { useEffect } from "react";
import { Container } from "react-bootstrap";
import { useStore } from "../../../stores/common/StoreProvider";
import SortOrder from "../../../utils/const/SortOrder";
import HttpStatusCode from "../../../utils/enum/HttpStatusCode";
import type Rating from "../../../utils/type/Rating";
import Filter from "../../common/filter/Filter";
import type { Columns } from "../../common/interface/AdminTable";
import AdminPage from "../common/AdminPage";

const AdminRating = () => {
	const { ratingStore } = useStore();

	useEffect(() => {
		document.title = "Admin | Rating";
	}, []);

	if (ratingStore.getStatus.get === HttpStatusCode.InternalServerError) {
		return (
			<Container>
				<div className="mt-3">
					<h1 className="text-center">
						A network error has occurred. Please try again later.
					</h1>
				</div>
			</Container>
		);
	}

	const columns: Array<Columns<Rating, keyof Rating>> = [
		{
			display: "Type",
			key: "type"
		}
	];

	return (
		<Container fluid>
			<h1 className="text-center mt-2">Ratings</h1>
			<Filter sortItems={SortOrder.AdminRating} store={ratingStore} />
			<AdminPage columns={columns} store={ratingStore} />
		</Container>
	);
};

export default observer(AdminRating);
