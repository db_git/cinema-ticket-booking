import { runInAction } from "mobx";
import { useEffect, useState } from "react";
import { FloatingLabel, Form } from "react-bootstrap";
import DisplayApiErrors from "../../../utils/DisplayApiErrors";
import type Rating from "../../../utils/type/Rating";
import type IFormProps from "../../common/interface/IFormProps";

const RatingForm = ({
	data,
	errors,
	formCreate,
	formUpdate
}: IFormProps<Rating>) => {
	const [typeValid, setTypeValid] = useState(false);
	const [formSubmitted, setFormSubmitted] = useState(false);

	useEffect(() => {
		runInAction(() => {
			setTypeValid(
				!errors.filter((error) => {
					return error.field === "Type";
				}).length
			);
		});
	}, [errors]);

	return (
		<Form
			id={data ? "updateForm" : "createForm"}
			noValidate
			onSubmit={(event) => {
				event.preventDefault();
				setFormSubmitted(true);
				const form = new FormData(event.currentTarget);
				runInAction(() => {
					if (data) formUpdate(data.id, form);
					else formCreate(form);
				});
			}}
		>
			<Form.Group>
				<FloatingLabel label="Type">
					<Form.Control
						defaultValue={runInAction(() => {
							return data?.type;
						})}
						isInvalid={formSubmitted && !typeValid}
						isValid={formSubmitted && typeValid}
						name="Type"
						placeholder="Type"
						type="text"
					/>
				</FloatingLabel>

				<Form.Text>
					<DisplayApiErrors errors={errors} filter="Type" />
				</Form.Text>
			</Form.Group>
		</Form>
	);
};

export default RatingForm;
