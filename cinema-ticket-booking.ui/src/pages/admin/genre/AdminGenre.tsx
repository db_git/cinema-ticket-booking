import { observer } from "mobx-react-lite";
import { useEffect } from "react";
import { Container } from "react-bootstrap";
import { useStore } from "../../../stores/common/StoreProvider";
import SortOrder from "../../../utils/const/SortOrder";
import HttpStatusCode from "../../../utils/enum/HttpStatusCode";
import type Genre from "../../../utils/type/Genre";
import Filter from "../../common/filter/Filter";
import type { Columns } from "../../common/interface/AdminTable";
import AdminPage from "../common/AdminPage";

const AdminGenre = () => {
	const { genreStore } = useStore();

	useEffect(() => {
		document.title = "Admin | Genre";
	}, []);

	if (genreStore.getStatus.get === HttpStatusCode.InternalServerError) {
		return (
			<Container>
				<div className="mt-3">
					<h1 className="text-center">
						A network error has occurred. Please try again later.
					</h1>
				</div>
			</Container>
		);
	}

	const columns: Array<Columns<Genre, keyof Genre>> = [
		{
			display: "Name",
			key: "name"
		}
	];

	return (
		<Container fluid>
			<h1 className="text-center mt-2">Genres</h1>
			<Filter sortItems={SortOrder.AdminGenre} store={genreStore} />
			<AdminPage columns={columns} store={genreStore} />
		</Container>
	);
};

export default observer(AdminGenre);
