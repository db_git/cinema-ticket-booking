import { runInAction } from "mobx";
import { useEffect, useState } from "react";
import { FloatingLabel, Form } from "react-bootstrap";
import DisplayApiErrors from "../../../utils/DisplayApiErrors";
import type Genre from "../../../utils/type/Genre";
import type IFormProps from "../../common/interface/IFormProps";

const GenreForm = ({
	data,
	errors,
	formCreate,
	formUpdate
}: IFormProps<Genre>) => {
	const [nameValid, setNameValid] = useState(false);
	const [formSubmitted, setFormSubmitted] = useState(false);

	useEffect(() => {
		runInAction(() => {
			setNameValid(
				!errors.filter((error) => {
					return error.field === "Name";
				}).length
			);
		});
	}, [errors]);

	return (
		<Form
			id={data ? "updateForm" : "createForm"}
			noValidate
			onSubmit={(event) => {
				event.preventDefault();
				setFormSubmitted(true);
				const form = new FormData(event.currentTarget);
				runInAction(() => {
					if (data) formUpdate(data.id, form);
					else formCreate(form);
				});
			}}
		>
			<Form.Group>
				<FloatingLabel label="Name">
					<Form.Control
						defaultValue={runInAction(() => {
							return data?.name;
						})}
						isInvalid={formSubmitted && !nameValid}
						isValid={formSubmitted && nameValid}
						name="Name"
						placeholder="Name"
						type="text"
					/>
				</FloatingLabel>
				<Form.Text>
					<DisplayApiErrors errors={errors} filter="Name" />
				</Form.Text>
			</Form.Group>
		</Form>
	);
};

export default GenreForm;
