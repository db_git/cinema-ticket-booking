import { runInAction } from "mobx";
import { observer } from "mobx-react-lite";
import type { FormEvent } from "react";
import { useEffect, useState } from "react";
import { FloatingLabel, Col, Form, Row } from "react-bootstrap";
import AsyncSelect from "react-select";
import { useStore } from "../../../stores/common/StoreProvider";
import DisplayApiErrors from "../../../utils/DisplayApiErrors";
import ApiRequest from "../../../utils/enum/ApiRequest";
import type Genre from "../../../utils/type/Genre";
import type Movie from "../../../utils/type/Movie";
import type Person from "../../../utils/type/Person";
import type Rating from "../../../utils/type/Rating";
import type IFormProps from "../../common/interface/IFormProps";

const MovieForm = ({
	data,
	errors,
	formCreate,
	formUpdate
}: IFormProps<Movie>) => {
	const { genreStore, personStore, ratingStore } = useStore();
	const [selectedGenres, setSelectedGenres] = useState<string[]>([]);
	const [selectedActors, setSelectedActors] = useState<string[]>([]);
	const [selectedDirectors, setSelectedDirectors] = useState<string[]>([]);
	const [selectedRating, setSelectedRating] = useState("");
	const [titleValid, setTitleValid] = useState(false);
	const [durationValid, setDurationValid] = useState(false);
	const [yearValid, setYearValid] = useState(false);
	const [taglineValid, setTaglineValid] = useState(false);
	const [summaryValid, setSummaryValid] = useState(false);
	const [posterUrlValid, setPosterUrlValid] = useState(false);
	const [backdropUrlValid, setBackdropUrlValid] = useState(false);
	const [trailerUrlValid, setTrailerUrlValid] = useState(false);
	const [genresValid, setGenresValid] = useState(false);
	const [actorsValid, setActorsValid] = useState(false);
	const [directorsValid, setDirectorsValid] = useState(false);
	const [ratingValid, setRatingValid] = useState(false);
	const [formSubmitted, setFormSubmitted] = useState(false);

	useEffect(() => {
		if (data) {
			setSelectedGenres(
				data.genres.map((genre) => {
					return genre.id;
				})
			);
			setSelectedActors(
				data.actors.map((actor) => {
					return actor.id;
				})
			);
			setSelectedDirectors(
				data.directors.map((director) => {
					return director.id;
				})
			);
			setSelectedRating(data.ratings[0].id);
		}

		genreStore.setRequest(ApiRequest.Page, 1);
		genreStore.setRequest(ApiRequest.Size, 20);
		personStore.setRequest(ApiRequest.Page, 1);
		personStore.setRequest(ApiRequest.Size, 20);
		ratingStore.setRequest(ApiRequest.Page, 1);
		ratingStore.setRequest(ApiRequest.Size, 20);

		genreStore.get().then();
		personStore.get().then();
		ratingStore.get().then();
	}, [data, genreStore, personStore, ratingStore]);

	useEffect(() => {
		runInAction(() => {
			setTitleValid(
				!errors.filter((error) => {
					return error.field === "Title";
				}).length
			);
			setDurationValid(
				!errors.filter((error) => {
					return error.field === "Duration";
				}).length
			);
			setYearValid(
				!errors.filter((error) => {
					return error.field === "Year";
				}).length
			);
			setTaglineValid(
				!errors.filter((error) => {
					return error.field === "Tagline";
				}).length
			);
			setSummaryValid(
				!errors.filter((error) => {
					return error.field === "Summary";
				}).length
			);
			setPosterUrlValid(
				!errors.filter((error) => {
					return error.field === "PosterUrl";
				}).length
			);
			setBackdropUrlValid(
				!errors.filter((error) => {
					return error.field === "BackdropUrl";
				}).length
			);
			setTrailerUrlValid(
				!errors.filter((error) => {
					return error.field === "TrailerUrl";
				}).length
			);
			setGenresValid(
				!errors.filter((error) => {
					return error.field === "Genres";
				}).length
			);
			setActorsValid(
				!errors.filter((error) => {
					return error.field === "Actors";
				}).length
			);
			setDirectorsValid(
				!errors.filter((error) => {
					return error.field === "Directors";
				}).length
			);
			setRatingValid(
				!errors.filter((error) => {
					return error.field === "Ratings";
				}).length
			);
		});
	}, [errors]);

	const validGenresOption = () => {
		if (formSubmitted && genresValid) return "text-success";
		if (formSubmitted && !genresValid) return "text-danger";
		return "";
	};

	const validActorsOption = () => {
		if (formSubmitted && actorsValid) return "text-success";
		if (formSubmitted && !actorsValid) return "text-danger";
		return "";
	};

	const validDirectorsOption = () => {
		if (formSubmitted && directorsValid) return "text-success";
		if (formSubmitted && !directorsValid) return "text-danger";
		return "";
	};

	const validRatingOption = () => {
		if (formSubmitted && ratingValid) return "text-success";
		if (formSubmitted && !ratingValid) return "text-danger";
		return "";
	};

	const handleSubmit = (event: FormEvent<HTMLFormElement>) => {
		event.preventDefault();
		setFormSubmitted(true);

		const form = new FormData(event.currentTarget);
		for (const genre of selectedGenres) {
			form.append("Genres", genre);
		}

		for (const actor of selectedActors) {
			form.append("Actors", actor);
		}

		for (const director of selectedDirectors) {
			form.append("Directors", director);
		}

		if (selectedRating) form.append("Ratings", selectedRating);

		runInAction(() => {
			if (data) formUpdate(data.id, form);
			else formCreate(form);
		});
	};

	return (
		<Form
			id={data ? "updateForm" : "createForm"}
			noValidate
			onSubmit={handleSubmit}
		>
			<Form.Group>
				<FloatingLabel label="Title">
					<Form.Control
						defaultValue={data?.title}
						isInvalid={formSubmitted && !titleValid}
						isValid={formSubmitted && titleValid}
						name="Title"
						placeholder="Title"
						type="text"
					/>
				</FloatingLabel>

				<Form.Text>
					<DisplayApiErrors errors={errors} filter="Title" />
				</Form.Text>
			</Form.Group>

			<Row>
				<Col>
					<Form.Group>
						<FloatingLabel label="Duration">
							<Form.Control
								defaultValue={data?.duration}
								isInvalid={formSubmitted && !durationValid}
								isValid={formSubmitted && durationValid}
								name="Duration"
								placeholder="Duration"
								type="number"
							/>
						</FloatingLabel>

						<Form.Text>
							<DisplayApiErrors
								errors={errors}
								filter="Duration"
							/>
						</Form.Text>
					</Form.Group>
				</Col>

				<Col>
					<Form.Group>
						<FloatingLabel label="Year">
							<Form.Control
								defaultValue={data?.year}
								isInvalid={formSubmitted && !yearValid}
								isValid={formSubmitted && yearValid}
								name="Year"
								placeholder="Year"
								type="number"
							/>
						</FloatingLabel>

						<Form.Text>
							<DisplayApiErrors errors={errors} filter="Year" />
						</Form.Text>
					</Form.Group>
				</Col>
			</Row>

			<Form.Group>
				<FloatingLabel label="Tagline">
					<Form.Control
						defaultValue={data?.tagline}
						isInvalid={formSubmitted && !taglineValid}
						isValid={formSubmitted && taglineValid}
						name="Tagline"
						placeholder="Tagline"
						type="textarea"
					/>
				</FloatingLabel>

				<Form.Text>
					<DisplayApiErrors errors={errors} filter="Tagline" />
				</Form.Text>
			</Form.Group>

			<Form.Group>
				<FloatingLabel label="Summary">
					<Form.Control
						defaultValue={data?.summary}
						isInvalid={formSubmitted && !summaryValid}
						isValid={formSubmitted && summaryValid}
						name="Summary"
						placeholder="Summary"
						type="textarea"
					/>
				</FloatingLabel>

				<Form.Text>
					<DisplayApiErrors errors={errors} filter="Summary" />
				</Form.Text>
			</Form.Group>

			<Form.Group>
				<FloatingLabel label="Poster URL">
					<Form.Control
						defaultValue={data?.posterUrl}
						isInvalid={formSubmitted && !posterUrlValid}
						isValid={formSubmitted && posterUrlValid}
						name="PosterUrl"
						placeholder="Poster URL"
						type="textarea"
					/>
				</FloatingLabel>

				<Form.Text>
					<DisplayApiErrors errors={errors} filter="PosterUrl" />
				</Form.Text>
			</Form.Group>

			<Form.Group>
				<FloatingLabel label="Backdrop URL">
					<Form.Control
						defaultValue={data?.backdropUrl}
						isInvalid={formSubmitted && !backdropUrlValid}
						isValid={formSubmitted && backdropUrlValid}
						name="BackdropUrl"
						placeholder="Backdrop URL"
						type="textarea"
					/>
				</FloatingLabel>

				<Form.Text>
					<DisplayApiErrors errors={errors} filter="BackdropUrl" />
				</Form.Text>
			</Form.Group>

			<Form.Group>
				<FloatingLabel label="Trailer URL">
					<Form.Control
						defaultValue={data?.trailerUrl}
						isInvalid={formSubmitted && !trailerUrlValid}
						isValid={formSubmitted && trailerUrlValid}
						name="TrailerUrl"
						placeholder="Trailer URL"
						type="textarea"
					/>
				</FloatingLabel>

				<Form.Text>
					<DisplayApiErrors errors={errors} filter="TrailerUrl" />
				</Form.Text>
			</Form.Group>

			<Form.Group>
				<Form.Label className={validGenresOption()}>Genres</Form.Label>
				<AsyncSelect
					defaultValue={data?.genres}
					getOptionLabel={(genre: Genre) => {
						return genre.name;
					}}
					getOptionValue={(genre) => {
						return genre.id;
					}}
					isMulti
					onChange={(genres) => {
						if (genres) {
							setSelectedGenres(
								genres.map((genre) => {
									return genre.id;
								})
							);
						}
					}}
					onInputChange={(query) => {
						genreStore.setRequest(ApiRequest.Search, query);
						genreStore.get().then();
					}}
					options={
						Array.isArray(genreStore.getResponse)
							? genreStore.getResponse
							: []
					}
					theme={(theme) => {
						return {
							...theme,
							colors: {
								...theme.colors,
								neutral0: "#3c4144",
								neutral10: "#889298",
								neutral80: "black",
								primary: "#c79705",
								primary25: "#6d757a"
							}
						};
					}}
				/>

				<Form.Text>
					<DisplayApiErrors errors={errors} filter="Genres" />
				</Form.Text>
			</Form.Group>

			<Form.Group>
				<Form.Label className={validActorsOption()}>Actors</Form.Label>
				<AsyncSelect
					defaultValue={data?.actors}
					getOptionLabel={(actor: Person) => {
						return actor.firstName + " " + actor.lastName;
					}}
					getOptionValue={(actor) => {
						return actor.id;
					}}
					isMulti
					onChange={(actors) => {
						if (actors) {
							setSelectedActors(
								actors.map((actor) => {
									return actor.id;
								})
							);
						}
					}}
					onInputChange={(query) => {
						personStore.setRequest(ApiRequest.Search, query);
						personStore.get().then();
					}}
					options={
						Array.isArray(personStore.getResponse)
							? personStore.getResponse
							: []
					}
					theme={(theme) => {
						return {
							...theme,
							colors: {
								...theme.colors,
								neutral0: "#3c4144",
								neutral10: "#889298",
								neutral80: "black",
								primary: "#c79705",
								primary25: "#6d757a"
							}
						};
					}}
				/>

				<Form.Text>
					<DisplayApiErrors errors={errors} filter="Actors" />
				</Form.Text>
			</Form.Group>

			<Form.Group>
				<Form.Label className={validDirectorsOption()}>
					Directors
				</Form.Label>
				<AsyncSelect
					defaultValue={data?.directors}
					getOptionLabel={(director: Person) => {
						return director.firstName + " " + director.lastName;
					}}
					getOptionValue={(director) => {
						return director.id;
					}}
					isMulti
					onChange={(directors) => {
						if (directors) {
							setSelectedDirectors(
								directors.map((director) => {
									return director.id;
								})
							);
						}
					}}
					onInputChange={(query) => {
						personStore.setRequest(ApiRequest.Search, query);
						personStore.get().then();
					}}
					options={
						Array.isArray(personStore.getResponse)
							? personStore.getResponse
							: []
					}
					theme={(theme) => {
						return {
							...theme,
							colors: {
								...theme.colors,
								neutral0: "#3c4144",
								neutral10: "#889298",
								neutral80: "black",
								primary: "#c79705",
								primary25: "#6d757a"
							}
						};
					}}
				/>

				<Form.Text>
					<DisplayApiErrors errors={errors} filter="Directors" />
				</Form.Text>
			</Form.Group>

			<Form.Group>
				<Form.Label className={validRatingOption()}>Rating</Form.Label>
				<AsyncSelect
					defaultInputValue={data ? data.ratings[0].type : ""}
					getOptionLabel={(rating: Rating) => {
						return rating.type;
					}}
					getOptionValue={(rating) => {
						return rating.id;
					}}
					onChange={(rating) => {
						if (rating) {
							setSelectedRating(rating.id);
						}
					}}
					onInputChange={(query) => {
						ratingStore.setRequest(ApiRequest.Search, query);
						ratingStore.get().then();
					}}
					options={
						Array.isArray(ratingStore.getResponse)
							? ratingStore.getResponse
							: []
					}
					theme={(theme) => {
						return {
							...theme,
							colors: {
								...theme.colors,
								neutral0: "#3c4144",
								neutral50: "white",
								neutral80: "white",
								primary: "#c79705",
								primary25: "#6d757a"
							}
						};
					}}
				/>

				<Form.Text>
					<DisplayApiErrors errors={errors} filter="RatingId" />
				</Form.Text>
			</Form.Group>
		</Form>
	);
};

export default observer(MovieForm);
