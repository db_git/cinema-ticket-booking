import { observer } from "mobx-react-lite";
import { useEffect } from "react";
import { Container } from "react-bootstrap";
import { useStore } from "../../../stores/common/StoreProvider";
import SortOrder from "../../../utils/const/SortOrder";
import HttpStatusCode from "../../../utils/enum/HttpStatusCode";
import type Movie from "../../../utils/type/Movie";
import Filter from "../../common/filter/Filter";
import type { Columns } from "../../common/interface/AdminTable";
import AdminPage from "../common/AdminPage";

const AdminMovie = () => {
	const { movieStore } = useStore();

	useEffect(() => {
		document.title = "Admin | Movie";
	}, []);

	if (movieStore.getStatus.get === HttpStatusCode.InternalServerError) {
		return (
			<Container>
				<div className="mt-3">
					<h1 className="text-center">
						A network error has occurred. Please try again later.
					</h1>
				</div>
			</Container>
		);
	}

	const columns: Array<Columns<Movie, keyof Movie>> = [
		{
			display: "Title",
			key: "title"
		},
		{
			display: "Duration",
			key: "duration"
		},
		{
			display: "Year",
			key: "year"
		},
		{
			display: "Tagline",
			key: "tagline"
		},
		{
			display: "Summary",
			key: "summary"
		}
	];

	return (
		<Container fluid>
			<h1 className="text-center mt-2">Movies</h1>
			<Filter sortItems={SortOrder.AdminMovie} store={movieStore} />
			<AdminPage columns={columns} store={movieStore} />
		</Container>
	);
};

export default observer(AdminMovie);
