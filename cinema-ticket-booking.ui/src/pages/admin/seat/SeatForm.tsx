import { runInAction } from "mobx";
import { observer } from "mobx-react-lite";
import type { FormEvent } from "react";
import { useEffect, useState } from "react";
import { FloatingLabel, Form } from "react-bootstrap";
import AsyncSelect from "react-select";
import { useStore } from "../../../stores/common/StoreProvider";
import DisplayApiErrors from "../../../utils/DisplayApiErrors";
import ApiRequest from "../../../utils/enum/ApiRequest";
import type Hall from "../../../utils/type/Hall";
import type Seat from "../../../utils/type/Seat";
import type IFormProps from "../../common/interface/IFormProps";

const SeatForm = ({
	data,
	errors,
	formCreate,
	formUpdate
}: IFormProps<Seat>) => {
	const { hallStore } = useStore();
	const [selectedHall, setSelectedHall] = useState("");
	const [hallValid, setHallValid] = useState(false);
	const [numberValid, setNumberValid] = useState(false);
	const [formSubmitted, setFormSubmitted] = useState(false);

	useEffect(() => {
		if (data) setSelectedHall(data.hall.id);

		hallStore.setRequest(ApiRequest.Page, 1);
		hallStore.setRequest(ApiRequest.Size, 20);
		hallStore.get().then();
	}, [data, hallStore]);

	useEffect(() => {
		runInAction(() => {
			setNumberValid(
				!errors.filter((error) => {
					return error.field === "Number";
				}).length
			);
			setHallValid(
				!errors.filter((error) => {
					return error.field === "HallId";
				}).length
			);
		});
	}, [errors]);

	const validHallOption = () => {
		if (formSubmitted && hallValid) return "text-success";
		if (formSubmitted && !hallValid) return "text-danger";
		return "";
	};

	const handleSubmit = (event: FormEvent<HTMLFormElement>) => {
		event.preventDefault();
		setFormSubmitted(true);

		const form = new FormData(event.currentTarget);
		if (selectedHall) form.append("HallId", selectedHall);

		runInAction(() => {
			if (data) formUpdate(data.id, form);
			else formCreate(form);
		});
	};

	return (
		<Form
			id={data ? "updateForm" : "createForm"}
			noValidate
			onSubmit={handleSubmit}
		>
			<Form.Group>
				<FloatingLabel label="Number">
					<Form.Control
						defaultValue={data?.number}
						isInvalid={formSubmitted && !numberValid}
						isValid={formSubmitted && numberValid}
						name="Number"
						placeholder="Number"
						type="number"
					/>
				</FloatingLabel>

				<Form.Text>
					<DisplayApiErrors errors={errors} filter="Number" />
				</Form.Text>
			</Form.Group>

			<Form.Group>
				<Form.Label className={validHallOption()}>Hall</Form.Label>
				<AsyncSelect
					defaultInputValue={data ? data.hall.name : ""}
					getOptionLabel={(hall: Hall) => {
						return hall.name;
					}}
					getOptionValue={(hall) => {
						return hall.id;
					}}
					onChange={(hall) => {
						if (hall) {
							setSelectedHall(hall.id);
						}
					}}
					onInputChange={(query) => {
						hallStore.setRequest(ApiRequest.Search, query);
						hallStore.get().then();
					}}
					options={
						Array.isArray(hallStore.getResponse)
							? hallStore.getResponse
							: []
					}
					theme={(theme) => {
						return {
							...theme,
							colors: {
								...theme.colors,
								neutral0: "#3c4144",
								neutral50: "white",
								neutral80: "white",
								primary: "#c79705",
								primary25: "#6d757a"
							}
						};
					}}
				/>

				<Form.Text>
					<DisplayApiErrors errors={errors} filter="HallId" />
				</Form.Text>
			</Form.Group>
		</Form>
	);
};

export default observer(SeatForm);
