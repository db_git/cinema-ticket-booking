import { observer } from "mobx-react-lite";
import { useEffect } from "react";
import { Container } from "react-bootstrap";
import { useStore } from "../../../stores/common/StoreProvider";
import SortOrder from "../../../utils/const/SortOrder";
import HttpStatusCode from "../../../utils/enum/HttpStatusCode";
import type Seat from "../../../utils/type/Seat";
import Filter from "../../common/filter/Filter";
import type { Columns } from "../../common/interface/AdminTable";
import AdminPage from "../common/AdminPage";

const AdminSeat = () => {
	const { seatStore } = useStore();

	useEffect(() => {
		document.title = "Admin | Seat";
	}, []);

	if (seatStore.getStatus.get === HttpStatusCode.InternalServerError) {
		return (
			<Container>
				<div className="mt-3">
					<h1 className="text-center">
						A network error has occurred. Please try again later.
					</h1>
				</div>
			</Container>
		);
	}

	const columns: Array<Columns<Seat, keyof Seat>> = [
		{
			display: "Number",
			key: "number"
		},
		{
			display: "Hall",
			key: "hall",
			secondKey: "name"
		}
	];

	return (
		<Container fluid>
			<h1 className="text-center mt-2">Seats</h1>
			<Filter sortItems={SortOrder.AdminSeat} store={seatStore} />
			<AdminPage columns={columns} store={seatStore} />
		</Container>
	);
};

export default observer(AdminSeat);
