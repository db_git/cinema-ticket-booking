import { runInAction } from "mobx";
import { observer } from "mobx-react-lite";
import type { FormEvent } from "react";
import { useEffect, useState } from "react";
import { FloatingLabel, Form } from "react-bootstrap";
import AsyncSelect from "react-select";
import { useStore } from "../../../stores/common/StoreProvider";
import DisplayApiErrors from "../../../utils/DisplayApiErrors";
import ApiRequest from "../../../utils/enum/ApiRequest";
import type Cinema from "../../../utils/type/Cinema";
import type Hall from "../../../utils/type/Hall";
import type IFormProps from "../../common/interface/IFormProps";

const HallForm = ({
	data,
	errors,
	formCreate,
	formUpdate
}: IFormProps<Hall>) => {
	const { cinemaStore } = useStore();
	const [selectedCinema, setSelectedCinema] = useState("");
	const [cinemaValid, setCinemaValid] = useState(false);
	const [nameValid, setNameValid] = useState(false);
	const [formSubmitted, setFormSubmitted] = useState(false);

	useEffect(() => {
		if (data) setSelectedCinema(data.cinema.id);

		cinemaStore.setRequest(ApiRequest.Page, 1);
		cinemaStore.setRequest(ApiRequest.Size, 20);
		cinemaStore.setRequest(ApiRequest.Search, "");
		cinemaStore.get().then();
	}, [data, cinemaStore]);

	useEffect(() => {
		runInAction(() => {
			setNameValid(
				!errors.filter((error) => {
					return error.field === "Name";
				}).length
			);
			setCinemaValid(
				!errors.filter((error) => {
					return error.field === "CinemaId";
				}).length
			);
		});
	}, [errors]);

	const validCinemaOption = () => {
		if (formSubmitted && cinemaValid) return "text-success";
		if (formSubmitted && !cinemaValid) return "text-danger";
		return "";
	};

	const handleSubmit = (event: FormEvent<HTMLFormElement>) => {
		event.preventDefault();
		setFormSubmitted(true);

		const form = new FormData(event.currentTarget);
		if (selectedCinema) form.append("CinemaId", selectedCinema);

		runInAction(() => {
			if (data) formUpdate(data.id, form);
			else formCreate(form);
		});
	};

	return (
		<Form
			id={data ? "updateForm" : "createForm"}
			noValidate
			onSubmit={handleSubmit}
		>
			<Form.Group>
				<FloatingLabel label="Name">
					<Form.Control
						defaultValue={data?.name}
						isInvalid={formSubmitted && !nameValid}
						isValid={formSubmitted && nameValid}
						name="Name"
						placeholder="Name"
						type="text"
					/>
				</FloatingLabel>

				<Form.Text>
					<DisplayApiErrors errors={errors} filter="Name" />
				</Form.Text>
			</Form.Group>

			<Form.Group>
				<Form.Label className={validCinemaOption()}>Cinema</Form.Label>
				<AsyncSelect
					className="react-select"
					defaultInputValue={data ? data.cinema.name : ""}
					getOptionLabel={(cinema: Cinema) => {
						return cinema.name;
					}}
					getOptionValue={(cinema) => {
						return cinema.id;
					}}
					onChange={(cinema) => {
						if (cinema) {
							setSelectedCinema(cinema.id);
						}
					}}
					onInputChange={(query) => {
						cinemaStore.setRequest(ApiRequest.Search, query);
						cinemaStore.get().then();
					}}
					options={
						Array.isArray(cinemaStore.getResponse)
							? cinemaStore.getResponse
							: []
					}
					theme={(theme) => {
						return {
							...theme,
							colors: {
								...theme.colors,
								neutral0: "#3c4144",
								neutral50: "white",
								neutral80: "white",
								primary: "#c79705",
								primary25: "#6d757a"
							}
						};
					}}
				/>

				<Form.Text>
					<DisplayApiErrors errors={errors} filter="CinemaId" />
				</Form.Text>
			</Form.Group>
		</Form>
	);
};

export default observer(HallForm);
