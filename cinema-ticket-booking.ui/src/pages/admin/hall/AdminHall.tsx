import { observer } from "mobx-react-lite";
import { useEffect } from "react";
import { Container } from "react-bootstrap";
import { useStore } from "../../../stores/common/StoreProvider";
import SortOrder from "../../../utils/const/SortOrder";
import HttpStatusCode from "../../../utils/enum/HttpStatusCode";
import type Hall from "../../../utils/type/Hall";
import Filter from "../../common/filter/Filter";
import type { Columns } from "../../common/interface/AdminTable";
import AdminPage from "../common/AdminPage";

const AdminHall = () => {
	const { hallStore } = useStore();

	useEffect(() => {
		document.title = "Admin | Hall";
	}, []);

	if (hallStore.getStatus.get === HttpStatusCode.InternalServerError) {
		return (
			<Container>
				<div className="mt-3">
					<h1 className="text-center">
						A network error has occurred. Please try again later.
					</h1>
				</div>
			</Container>
		);
	}

	const columns: Array<Columns<Hall, keyof Hall>> = [
		{
			display: "Name",
			key: "name"
		},
		{
			display: "Cinema",
			key: "cinema",
			secondKey: "name"
		}
	];

	return (
		<Container fluid>
			<h1 className="text-center mt-2">Halls</h1>
			<Filter sortItems={SortOrder.AdminHall} store={hallStore} />
			<AdminPage columns={columns} store={hallStore} />
		</Container>
	);
};

export default observer(AdminHall);
