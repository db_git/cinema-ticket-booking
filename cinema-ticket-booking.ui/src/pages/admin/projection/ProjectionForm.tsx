import { runInAction } from "mobx";
import { observer } from "mobx-react-lite";
import type { FormEvent } from "react";
import { useEffect, useState } from "react";
import { FloatingLabel, Form } from "react-bootstrap";
import AsyncSelect from "react-select";
import { useStore } from "../../../stores/common/StoreProvider";
import DisplayApiErrors from "../../../utils/DisplayApiErrors";
import ApiRequest from "../../../utils/enum/ApiRequest";
import type Hall from "../../../utils/type/Hall";
import type Movie from "../../../utils/type/Movie";
import type Projection from "../../../utils/type/Projection";
import type IFormProps from "../../common/interface/IFormProps";

const ProjectionForm = ({
	data,
	errors,
	formCreate,
	formUpdate
}: IFormProps<Projection>) => {
	const { movieStore, hallStore } = useStore();
	const [selectedMovie, setSelectedMovie] = useState("");
	const [selectedHall, setSelectedHall] = useState("");
	const [movieValid, setMovieValid] = useState(false);
	const [hallValid, setHallValid] = useState(false);
	const [timeValid, setTimeValid] = useState(false);
	const [formSubmitted, setFormSubmitted] = useState(false);

	useEffect(() => {
		if (data) {
			setSelectedMovie(data.movie.id);
			setSelectedHall(data.hall.id);
		}

		movieStore.setRequest(ApiRequest.Page, 1);
		movieStore.setRequest(ApiRequest.Size, 20);
		hallStore.setRequest(ApiRequest.Page, 1);
		hallStore.setRequest(ApiRequest.Size, 20);

		movieStore.get().then();
		hallStore.get().then();
	}, [data, movieStore, hallStore]);

	useEffect(() => {
		runInAction(() => {
			setMovieValid(
				!errors.filter((error) => {
					return error.field === "MovieId";
				}).length
			);
			setHallValid(
				!errors.filter((error) => {
					return error.field === "HallId";
				}).length
			);
			setTimeValid(
				!errors.filter((error) => {
					return error.field === "Time";
				}).length
			);
		});
	}, [errors]);

	const validMovieOption = () => {
		if (formSubmitted && movieValid) return "text-success";
		if (formSubmitted && !movieValid) return "text-danger";
		return "";
	};

	const validHallOption = () => {
		if (formSubmitted && hallValid) return "text-success";
		if (formSubmitted && !hallValid) return "text-danger";
		return "";
	};

	const handleSubmit = (event: FormEvent<HTMLFormElement>) => {
		event.preventDefault();
		setFormSubmitted(true);

		const form = new FormData(event.currentTarget);
		if (selectedMovie) form.append("MovieId", selectedMovie);
		if (selectedHall) form.append("HallId", selectedHall);

		runInAction(() => {
			if (data) formUpdate(data.id, form);
			else formCreate(form);
		});
	};

	return (
		<Form
			id={data ? "updateForm" : "createForm"}
			noValidate
			onSubmit={handleSubmit}
		>
			<Form.Group>
				<Form.Label className={validMovieOption()}>Movie</Form.Label>
				<AsyncSelect
					defaultInputValue={data ? data.movie.title : ""}
					getOptionLabel={(movie: Movie) => {
						return movie.title;
					}}
					getOptionValue={(movie) => {
						return movie.id;
					}}
					onChange={(movie) => {
						if (movie) {
							setSelectedMovie(movie.id);
						}
					}}
					onInputChange={(query) => {
						movieStore.setRequest(ApiRequest.Search, query);
						movieStore.get().then();
					}}
					options={
						Array.isArray(movieStore.getResponse)
							? movieStore.getResponse
							: []
					}
					theme={(theme) => {
						return {
							...theme,
							colors: {
								...theme.colors,
								neutral0: "#3c4144",
								neutral50: "white",
								neutral80: "white",
								primary: "#c79705",
								primary25: "#6d757a"
							}
						};
					}}
				/>

				<Form.Text>
					<DisplayApiErrors errors={errors} filter="MovieId" />
				</Form.Text>
			</Form.Group>

			<Form.Group>
				<Form.Label className={validHallOption()}>Hall</Form.Label>
				<AsyncSelect
					defaultInputValue={data ? data.hall.name : ""}
					getOptionLabel={(hall: Hall) => {
						return hall.name;
					}}
					getOptionValue={(hall) => {
						return hall.id;
					}}
					onChange={(hall) => {
						if (hall) {
							setSelectedHall(hall.id);
						}
					}}
					onInputChange={(query) => {
						hallStore.setRequest(ApiRequest.Search, query);
						hallStore.get().then();
					}}
					options={
						Array.isArray(hallStore.getResponse)
							? hallStore.getResponse
							: []
					}
					theme={(theme) => {
						return {
							...theme,
							colors: {
								...theme.colors,
								neutral0: "#3c4144",
								neutral50: "white",
								neutral80: "white",
								primary: "#c79705",
								primary25: "#6d757a"
							}
						};
					}}
				/>

				<Form.Text>
					<DisplayApiErrors errors={errors} filter="HallId" />
				</Form.Text>
			</Form.Group>

			<Form.Group>
				<FloatingLabel label="Time">
					<Form.Control
						defaultValue={
							data &&
							new Date(data.time).toISOString().slice(0, 16)
						}
						isInvalid={formSubmitted && !timeValid}
						isValid={formSubmitted && timeValid}
						name="Time"
						type="datetime-local"
					/>
				</FloatingLabel>
			</Form.Group>
		</Form>
	);
};

export default observer(ProjectionForm);
