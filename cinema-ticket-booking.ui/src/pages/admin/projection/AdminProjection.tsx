import { observer } from "mobx-react-lite";
import { useEffect } from "react";
import { Container } from "react-bootstrap";
import { useStore } from "../../../stores/common/StoreProvider";
import SortOrder from "../../../utils/const/SortOrder";
import HttpStatusCode from "../../../utils/enum/HttpStatusCode";
import type Projection from "../../../utils/type/Projection";
import Filter from "../../common/filter/Filter";
import type { Columns } from "../../common/interface/AdminTable";
import AdminPage from "../common/AdminPage";

const AdminProjection = () => {
	const { projectionStore } = useStore();

	useEffect(() => {
		document.title = "Admin | Projection";
	}, []);

	if (projectionStore.getStatus.get === HttpStatusCode.InternalServerError) {
		return (
			<Container>
				<div className="mt-3">
					<h1 className="text-center">
						A network error has occurred. Please try again later.
					</h1>
				</div>
			</Container>
		);
	}

	const columns: Array<Columns<Projection, keyof Projection>> = [
		{
			display: "Movie",
			key: "movie",
			secondKey: "title"
		},
		{
			display: "Hall",
			key: "hall",
			secondKey: "name"
		},
		{
			display: "Time",
			key: "time"
		}
	];

	return (
		<Container fluid>
			<h1 className="text-center mt-2">Projections</h1>
			<Filter
				sortItems={SortOrder.AdminProjection}
				store={projectionStore}
			/>
			<AdminPage columns={columns} store={projectionStore} />
		</Container>
	);
};

export default observer(AdminProjection);
