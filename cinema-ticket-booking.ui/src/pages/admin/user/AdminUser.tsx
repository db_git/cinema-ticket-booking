import { observer } from "mobx-react-lite";
import { useEffect } from "react";
import { Container } from "react-bootstrap";
import { useStore } from "../../../stores/common/StoreProvider";
import SortOrder from "../../../utils/const/SortOrder";
import HttpStatusCode from "../../../utils/enum/HttpStatusCode";
import type User from "../../../utils/type/User";
import Filter from "../../common/filter/Filter";
import type { Columns } from "../../common/interface/AdminTable";
import AdminPage from "../common/AdminPage";

const AdminUser = () => {
	const { userStore } = useStore();

	useEffect(() => {
		document.title = "Admin | User";
	}, []);

	if (userStore.getStatus.get === HttpStatusCode.InternalServerError) {
		return (
			<Container>
				<div className="mt-3">
					<h1 className="text-center">
						A network error has occurred. Please try again later.
					</h1>
				</div>
			</Container>
		);
	}

	const columns: Array<Columns<User, keyof User>> = [
		{
			display: "First name",
			key: "firstName"
		},
		{
			display: "Last name",
			key: "lastName"
		},
		{
			display: "Email",
			key: "email"
		},
		{
			display: "Roles",
			key: "roles"
		}
	];

	return (
		<Container fluid>
			<h1 className="text-center mt-2">Users</h1>
			<Filter sortItems={SortOrder.AdminUser} store={userStore} />
			<AdminPage
				columns={columns}
				showCreate={false}
				showUpdate={false}
				store={userStore}
			/>
		</Container>
	);
};

export default observer(AdminUser);
