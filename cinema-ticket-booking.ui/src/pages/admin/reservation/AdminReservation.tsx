import { observer } from "mobx-react-lite";
import { useEffect } from "react";
import { Container } from "react-bootstrap";
import { useStore } from "../../../stores/common/StoreProvider";
import SortOrder from "../../../utils/const/SortOrder";
import HttpStatusCode from "../../../utils/enum/HttpStatusCode";
import type Reservation from "../../../utils/type/Reservation";
import Filter from "../../common/filter/Filter";
import type { Columns } from "../../common/interface/AdminTable";
import AdminPage from "../common/AdminPage";

const AdminReservation = () => {
	const { reservationStore } = useStore();

	useEffect(() => {
		document.title = "Admin | Reservation";
	}, []);

	if (reservationStore.getStatus.get === HttpStatusCode.InternalServerError) {
		return (
			<Container>
				<div className="mt-3">
					<h1 className="text-center">
						A network error has occurred. Please try again later.
					</h1>
				</div>
			</Container>
		);
	}

	const columns: Array<Columns<Reservation, keyof Reservation>> = [
		{
			display: "First name",
			key: "user",
			secondKey: "firstName"
		},
		{
			display: "Last name",
			key: "user",
			secondKey: "lastName"
		},
		{
			display: "Email",
			key: "user",
			secondKey: "email"
		},
		{
			display: "Movie",
			key: "projection",
			secondKey: "movie",
			thirdKey: "title"
		},
		{
			display: "Hall",
			key: "projection",
			secondKey: "hall",
			thirdKey: "name"
		},
		{
			display: "Time",
			key: "projection",
			secondKey: "time"
		}
	];

	return (
		<Container fluid>
			<h1 className="text-center mt-2">Reservations</h1>
			<Filter
				sortItems={SortOrder.AdminReservation}
				store={reservationStore}
			/>
			<AdminPage
				columns={columns}
				showCreate={false}
				showUpdate={false}
				store={reservationStore}
			/>
		</Container>
	);
};

export default observer(AdminReservation);
