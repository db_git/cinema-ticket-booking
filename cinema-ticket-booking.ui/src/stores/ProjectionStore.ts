import Endpoint from "../utils/Endpoint";
import type IAuthStore from "../utils/interface/IAuthStore";
import type Projection from "../utils/type/Projection";
import ApiStore from "./common/ApiStore";

class ProjectionStore extends ApiStore<Projection> {
	public constructor(authStore: IAuthStore) {
		super(authStore, Endpoint("projection"));
	}
}

export default ProjectionStore;
