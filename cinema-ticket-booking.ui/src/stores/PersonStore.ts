import Endpoint from "../utils/Endpoint";
import type IAuthStore from "../utils/interface/IAuthStore";
import type Person from "../utils/type/Person";
import ApiStore from "./common/ApiStore";

class PersonStore extends ApiStore<Person> {
	public constructor(authStore: IAuthStore) {
		super(authStore, Endpoint("person"));
	}
}

export default PersonStore;
