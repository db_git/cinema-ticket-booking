import Endpoint from "../utils/Endpoint";
import type IAuthStore from "../utils/interface/IAuthStore";
import type Seat from "../utils/type/Seat";
import ApiStore from "./common/ApiStore";

class SeatStore extends ApiStore<Seat> {
	public constructor(authStore: IAuthStore) {
		super(authStore, Endpoint("seat"));
	}
}

export default SeatStore;
