import Endpoint from "../utils/Endpoint";
import type IAuthStore from "../utils/interface/IAuthStore";
import type Cinema from "../utils/type/Cinema";
import ApiStore from "./common/ApiStore";

class CinemaStore extends ApiStore<Cinema> {
	public constructor(authStore: IAuthStore) {
		super(authStore, Endpoint("cinema"));
	}
}

export default CinemaStore;
