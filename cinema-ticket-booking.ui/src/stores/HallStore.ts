import Endpoint from "../utils/Endpoint";
import type IAuthStore from "../utils/interface/IAuthStore";
import type Hall from "../utils/type/Hall";
import ApiStore from "./common/ApiStore";

class HallStore extends ApiStore<Hall> {
	public constructor(authStore: IAuthStore) {
		super(authStore, Endpoint("hall"));
	}
}

export default HallStore;
