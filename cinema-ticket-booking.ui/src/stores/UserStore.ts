import Endpoint from "../utils/Endpoint";
import type IAuthStore from "../utils/interface/IAuthStore";
import type User from "../utils/type/User";
import ApiStore from "./common/ApiStore";

class UserStore extends ApiStore<User> {
	public constructor(authStore: IAuthStore) {
		super(authStore, Endpoint("user"));
	}
}

export default UserStore;
