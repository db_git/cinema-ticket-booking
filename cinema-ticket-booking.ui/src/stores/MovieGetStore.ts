import Endpoint from "../utils/Endpoint";
import type Movie from "../utils/type/Movie";
import ApiStoreGet from "./common/ApiStoreGet";

class MovieGetStore extends ApiStoreGet<Movie> {
	public constructor() {
		super(Endpoint("movie"));
	}
}

export default MovieGetStore;
