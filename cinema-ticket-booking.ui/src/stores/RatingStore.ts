import Endpoint from "../utils/Endpoint";
import type IAuthStore from "../utils/interface/IAuthStore";
import type Rating from "../utils/type/Rating";
import ApiStore from "./common/ApiStore";

class RatingStore extends ApiStore<Rating> {
	public constructor(authStore: IAuthStore) {
		super(authStore, Endpoint("rating"));
	}
}

export default RatingStore;
