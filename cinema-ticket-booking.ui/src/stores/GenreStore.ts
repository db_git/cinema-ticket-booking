import Endpoint from "../utils/Endpoint";
import type IAuthStore from "../utils/interface/IAuthStore";
import type Genre from "../utils/type/Genre";
import ApiStore from "./common/ApiStore";

class GenreStore extends ApiStore<Genre> {
	public constructor(authStore: IAuthStore) {
		super(authStore, Endpoint("genre"));
	}
}

export default GenreStore;
