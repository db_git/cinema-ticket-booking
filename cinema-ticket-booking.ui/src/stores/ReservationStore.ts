import Endpoint from "../utils/Endpoint";
import type IAuthStore from "../utils/interface/IAuthStore";
import type Reservation from "../utils/type/Reservation";
import ApiStore from "./common/ApiStore";

class ReservationStore extends ApiStore<Reservation> {
	public constructor(authStore: IAuthStore) {
		super(authStore, Endpoint("reservation"));
	}
}

export default ReservationStore;
