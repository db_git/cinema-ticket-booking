import Endpoint from "../utils/Endpoint";
import type IAuthStore from "../utils/interface/IAuthStore";
import type Movie from "../utils/type/Movie";
import ApiStore from "./common/ApiStore";

class MovieStore extends ApiStore<Movie> {
	public constructor(authStore: IAuthStore) {
		super(authStore, Endpoint("movie"));
	}
}

export default MovieStore;
