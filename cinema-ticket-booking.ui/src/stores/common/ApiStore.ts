import axios from "axios";
import {
	makeObservable,
	observable,
	action,
	computed,
	runInAction
} from "mobx";
import ParseApiErrors from "../../utils/ParseApiErrors";
import ApiRequest from "../../utils/enum/ApiRequest";
import HttpMethod from "../../utils/enum/HttpMethod";
import type IApiStore from "../../utils/interface/IApiStore";
import type IAuthStore from "../../utils/interface/IAuthStore";
import type ApiErrors from "../../utils/type/ApiErrors";
import type ApiResponse from "../../utils/type/ApiResponse";
import type ApiStatus from "../../utils/type/ApiStatus";
import type Request from "../../utils/type/Request";
import type ResponseError from "../../utils/type/ResponseError";

class ApiStore<T> implements IApiStore<T> {
	public readonly authStore: IAuthStore;

	public readonly endpoint: string;

	public request: Request;

	public records: number;

	public response: ApiResponse<T>;

	public status: ApiStatus;

	public errors: ApiErrors;

	public constructor(authStore: IAuthStore, endpoint: string) {
		this.authStore = authStore;
		this.endpoint = endpoint;
		this.request = { page: 1, size: 10, sort: 1 };
		this.records = 0;
		this.status = { delete: 0, get: 0, post: 0, put: 0 };
		this.errors = { delete: [], get: [], post: [], put: [] };

		makeObservable(this, {
			create: action,
			delete: action,
			errors: observable,
			get: action,
			getErrors: computed,
			getRecords: computed,
			getRequest: computed,
			getResponse: computed,
			getStatus: computed,
			records: observable,
			request: observable,
			response: observable,
			setErrors: action,
			setRecords: action,
			setRequest: action,
			setResponse: action,
			setStatus: action,
			status: observable,
			update: action
		});
	}

	public get getRecords(): number {
		return this.records;
	}

	public setRecords = (records: number): void => {
		this.records = records;
	};

	public get getRequest(): Request {
		return this.request;
	}

	public setRequest = (
		type: ApiRequest,
		value: number | string | undefined
	): void => {
		switch (type) {
			case ApiRequest.Page:
				if (typeof value === "number") this.request.page = value;
				break;

			case ApiRequest.Size:
				if (typeof value === "number") this.request.size = value;
				break;

			case ApiRequest.Sort:
				if (typeof value === "number") this.request.sort = value;
				break;

			case ApiRequest.Search:
				if (typeof value !== "number") this.request.search = value;
				break;

			default:
				break;
		}
	};

	public get getResponse(): ApiResponse<T> {
		return this.response;
	}

	public setResponse = (response: ApiResponse<T>): void => {
		this.response = response;
	};

	public get getStatus(): ApiStatus {
		return this.status;
	}

	public setStatus = (type: HttpMethod, status: number): void => {
		switch (type) {
			case HttpMethod.Get:
				this.status.get = status;
				break;

			case HttpMethod.Post:
				this.status.post = status;
				break;

			case HttpMethod.Put:
				this.status.put = status;
				break;

			case HttpMethod.Delete:
				this.status.delete = status;
				break;

			default:
				break;
		}
	};

	public get getErrors(): ApiErrors {
		return this.errors;
	}

	public setErrors = (type: HttpMethod, error: ResponseError[]): void => {
		switch (type) {
			case HttpMethod.Get:
				this.errors.get = error;
				if (!error) this.errors.get.length = 0;
				break;

			case HttpMethod.Post:
				this.errors.post = error;
				if (!error) this.errors.post.length = 0;
				break;

			case HttpMethod.Put:
				this.errors.put = error;
				if (!error) this.errors.put.length = 0;
				break;

			case HttpMethod.Delete:
				this.errors.delete = error;
				if (!error) this.errors.delete.length = 0;
				break;

			default:
				break;
		}
	};

	public get = async (id?: string) => {
		let endpoint: string;

		if (typeof id === "string") {
			endpoint = this.endpoint + "/" + id;
		} else {
			endpoint = this.endpoint;
		}

		await axios
			.get(endpoint, {
				headers: this.authStore.getHeader,
				params: this.getRequest
			})
			.then((response) => {
				this.setResponse(response.data);
				this.setRecords(Number(response.headers.records));
				this.setStatus(HttpMethod.Get, response.status);
				this.setErrors(HttpMethod.Get, []);
			})
			.catch((error) => {
				this.setStatus(HttpMethod.Get, error.response.status);
				this.setErrors(HttpMethod.Get, error.data);
			});
	};

	public create = async (form: FormData): Promise<void> => {
		await axios
			.post(this.endpoint, form, {
				headers: this.authStore.getHeader
			})
			.then(async (response) => {
				this.setStatus(HttpMethod.Post, response.status);
				this.setErrors(HttpMethod.Post, []);
				await this.get();
			})
			.catch((error) => {
				this.setStatus(HttpMethod.Post, error.response.status);
				this.setErrors(HttpMethod.Post, []);
				runInAction(() => {
					ParseApiErrors(
						this.getErrors.post,
						error.response.data.errors as never
					);
				});
			});
	};

	public update = async (id: string, form: FormData) => {
		await axios
			.put(this.endpoint + "/" + id, form, {
				headers: this.authStore.getHeader
			})
			.then(async (response) => {
				this.setStatus(HttpMethod.Put, response.status);
				this.setErrors(HttpMethod.Put, []);
				await this.get();
			})
			.catch((error) => {
				this.setStatus(HttpMethod.Put, error.response.status);
				this.setErrors(HttpMethod.Put, []);
				runInAction(() => {
					ParseApiErrors(
						this.getErrors.put,
						error.response.data.errors as never
					);
				});
			});
	};

	public delete = async (id: string) => {
		await axios
			.delete(this.endpoint + "/" + id, {
				headers: this.authStore.getHeader
			})
			.then(async (response) => {
				this.setStatus(HttpMethod.Delete, response.status);
				this.setErrors(HttpMethod.Delete, []);
				await this.get();
			})
			.catch((error) => {
				this.setStatus(HttpMethod.Delete, error.response.status);
				this.setErrors(HttpMethod.Delete, []);
				runInAction(() => {
					ParseApiErrors(
						this.getErrors.delete,
						error.response.data.errors as never
					);
				});
			});
	};
}

export default ApiStore;
