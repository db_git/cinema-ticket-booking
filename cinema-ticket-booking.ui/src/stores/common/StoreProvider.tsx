import type { FC } from "react";
import { createContext, useContext } from "react";
import RootStore from "./RootStore";

const store = RootStore.GetInstance();
const StoreContext = createContext<RootStore>(store);

const useStore = () => {
	return useContext(StoreContext);
};

const StoreProvider: FC = ({ children }) => {
	return (
		<StoreContext.Provider value={store}>{children}</StoreContext.Provider>
	);
};

export { useStore, StoreProvider };
