import axios from "axios";
import { action, computed, observable, makeObservable } from "mobx";
import type { FormEvent } from "react";
import ParseWebApiErrors from "../../utils/ParseApiErrors";
import ParseJwtToken from "../../utils/ParseJwt";
import type IAuthStore from "../../utils/interface/IAuthStore";
import type IRegistrationStore from "../../utils/interface/IRegistrationStore";
import type ResponseError from "../../utils/type/ResponseError";
import RegistrationStore from "./RegistrationStore";

class AuthStore implements IAuthStore {
	public readonly api: IRegistrationStore;

	public header: { Authorization: string };

	public token: string;

	public status: number;

	public errors: ResponseError[];

	public constructor() {
		this.api = new RegistrationStore();
		this.token = "";
		this.status = 0;
		this.errors = [];
		this.header = { Authorization: "" };

		makeObservable(this, {
			errors: observable,
			getEmail: computed,
			getErrors: computed,
			getHeader: computed,
			getStatus: computed,
			getToken: computed,
			getUserId: computed,
			handleError: action,
			handleLogin: action,
			header: observable,
			isAdmin: computed,
			isLoggedIn: computed,
			login: action,
			logout: action,
			register: action,
			setErrors: action,
			setHeader: action,
			setStatus: action,
			setToken: action,
			status: observable,
			token: observable
		});

		const token = sessionStorage.getItem("token");
		if (token !== null || token !== "") {
			this.handleLogin(token);
		}
	}

	public get getEmail(): string {
		const email = ParseJwtToken(this.getToken, "email");
		if (email !== null) return email;
		return "unknown";
	}

	public get isAdmin(): boolean {
		const role = ParseJwtToken(this.getToken, "role");
		return !(!role || role !== "Admin");
	}

	public get isLoggedIn(): boolean {
		return !(this.getToken === null || this.getToken === "");
	}

	public get getUserId(): string | null {
		return ParseJwtToken(this.getToken, "id");
	}

	public get getToken(): string | null {
		return this.token;
	}

	public setToken = (token: string) => {
		this.token = token;
	};

	public get getStatus(): number {
		return this.status;
	}

	public setStatus = (status: number) => {
		this.status = status;
	};

	public get getErrors(): ResponseError[] {
		return this.errors;
	}

	public setErrors = (errors: ResponseError[]) => {
		this.errors = errors;
		if (errors === undefined) this.errors.length = 0;
	};

	public get getHeader(): { Authorization: string } {
		return this.header;
	}

	public setHeader = (token: string): void => {
		this.header = { Authorization: "Bearer " + token };
	};

	public register = async (
		event: FormEvent<HTMLFormElement>
	): Promise<void> => {
		const form = new FormData(event.currentTarget);

		await axios
			.post(this.api.registerEndpoint, form)
			.then((response) => {
				this.setStatus(response.status);

				axios.post(this.api.loginEndpoint, form).then((result) => {
					this.setStatus(result.status);
					this.handleLogin(result.data.token);
				});
			})
			.catch((error) => {
				this.handleError(error);
			});
	};

	public login = async (event: FormEvent<HTMLFormElement>): Promise<void> => {
		const form = new FormData(event.currentTarget);

		await axios
			.post(this.api.loginEndpoint, form)
			.then((response) => {
				this.setStatus(response.status);
				this.handleLogin(response.data.token);
			})
			.catch((error) => {
				this.handleError(error);
			});
	};

	public logout = (): void => {
		this.setStatus(0);
		this.setToken("");
		sessionStorage.removeItem("token");
	};

	public handleLogin = (token: string | null): void => {
		if (token !== null && token !== "") {
			this.setErrors([]);
			this.setToken(token);
			sessionStorage.setItem("token", token);
			this.setHeader(token);
		}
	};

	public handleError = (errors: any): void => {
		this.setStatus(errors.response.status);
		this.setErrors([]);
		ParseWebApiErrors(this.getErrors, errors.response.data.errors as never);
	};
}

export default AuthStore;
