import { makeAutoObservable } from "mobx";
import type IAuthStore from "../../utils/interface/IAuthStore";
import type IRootStore from "../../utils/interface/IRootStore";
import CinemaStore from "../CinemaStore";
import GenreStore from "../GenreStore";
import HallStore from "../HallStore";
import MovieCarouselStore from "../MovieCarouselStore";
import MovieGetStore from "../MovieGetStore";
import MovieStore from "../MovieStore";
import PersonStore from "../PersonStore";
import ProjectionStore from "../ProjectionStore";
import RatingStore from "../RatingStore";
import ReservationStore from "../ReservationStore";
import SeatStore from "../SeatStore";
import UserReservationStore from "../UserReservationStore";
import UserStore from "../UserStore";
import AuthStore from "./AuthStore";

class RootStore implements IRootStore {
	private static rootStore: RootStore;

	public authStore: IAuthStore;

	public cinemaStore: CinemaStore;

	public genreStore: GenreStore;

	public hallStore: HallStore;

	public movieStore: MovieStore;

	public movieGetStore: MovieGetStore;

	public movieCarouselStore: MovieCarouselStore;

	public personStore: PersonStore;

	public projectionStore: ProjectionStore;

	public ratingStore: RatingStore;

	public reservationStore: ReservationStore;

	public seatStore: SeatStore;

	public userStore: UserStore;

	public userReservationStore: UserReservationStore;

	private constructor() {
		this.authStore = new AuthStore();
		this.cinemaStore = new CinemaStore(this.authStore);
		this.genreStore = new GenreStore(this.authStore);
		this.hallStore = new HallStore(this.authStore);
		this.movieStore = new MovieStore(this.authStore);
		this.movieGetStore = new MovieGetStore();
		this.movieCarouselStore = new MovieCarouselStore();
		this.personStore = new PersonStore(this.authStore);
		this.projectionStore = new ProjectionStore(this.authStore);
		this.ratingStore = new RatingStore(this.authStore);
		this.reservationStore = new ReservationStore(this.authStore);
		this.seatStore = new SeatStore(this.authStore);
		this.userStore = new UserStore(this.authStore);
		this.userReservationStore = new UserReservationStore(this.authStore);

		makeAutoObservable(this);
	}

	public static GetInstance(): IRootStore {
		if (this.rootStore === undefined || this.rootStore === null)
			this.rootStore = new RootStore();
		return this.rootStore;
	}
}

export default RootStore;
