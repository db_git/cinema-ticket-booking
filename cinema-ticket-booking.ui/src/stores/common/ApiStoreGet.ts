import axios from "axios";
import { action, computed, makeObservable, observable } from "mobx";
import ApiRequest from "../../utils/enum/ApiRequest";
import type IApiStoreGet from "../../utils/interface/IApiStoreGet";
import type ApiResponse from "../../utils/type/ApiResponse";
import type Request from "../../utils/type/Request";
import type ResponseError from "../../utils/type/ResponseError";

class ApiStoreGet<T> implements IApiStoreGet<T> {
	public readonly endpoint: string;

	public request: Request;

	public response: ApiResponse<T>;

	public status: number;

	public errors: ResponseError[];

	public constructor(endpoint: string) {
		this.endpoint = endpoint;
		this.request = { page: 1, size: 10, sort: 1 };
		this.status = 0;
		this.errors = [];

		makeObservable(this, {
			errors: observable,
			get: action,
			getErrors: computed,
			getRequest: computed,
			getResponse: computed,
			getStatus: computed,
			request: observable,
			response: observable,
			setErrors: action,
			setRequest: action,
			setResponse: action,
			setStatus: action,
			status: observable
		});
	}

	public get getRequest(): Request {
		return this.request;
	}

	public setRequest = (type: ApiRequest, value: number | undefined): void => {
		switch (type) {
			case ApiRequest.Page:
				if (typeof value === "number") this.request.page = value;
				break;

			case ApiRequest.Size:
				if (typeof value === "number") this.request.size = value;
				break;

			case ApiRequest.Sort:
				if (typeof value === "number") this.request.sort = value;
				break;

			default:
				break;
		}
	};

	public get getResponse(): ApiResponse<T> {
		return this.response;
	}

	public setResponse = (response: ApiResponse<T>): void => {
		this.response = response;
	};

	public get getStatus(): number {
		return this.status;
	}

	public setStatus = (status: number): void => {
		this.status = status;
	};

	public get getErrors(): ResponseError[] {
		return this.errors;
	}

	public setErrors = (error: ResponseError[]): void => {
		this.errors = error;
		if (!error) this.errors.length = 0;
	};

	public get = async (id?: string) => {
		let endpoint: string;

		if (typeof id === "string") {
			endpoint = this.endpoint + "/" + id;
		} else {
			endpoint = this.endpoint;
		}

		await axios
			.get(endpoint, {
				params: this.getRequest
			})
			.then((response) => {
				this.setResponse(response.data);
				this.setStatus(response.status);
				this.setErrors([]);
			})
			.catch((error) => {
				this.setStatus(error.response.status);
				this.setErrors(error.data);
			});
	};
}

export default ApiStoreGet;
