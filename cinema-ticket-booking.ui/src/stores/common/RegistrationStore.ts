import Endpoint from "../../utils/Endpoint";
import type IRegistrationStore from "../../utils/interface/IRegistrationStore";

class RegistrationStore implements IRegistrationStore {
	public readonly loginEndpoint: string = Endpoint("user/login");

	public readonly registerEndpoint: string = Endpoint("user/register");
}

export default RegistrationStore;
