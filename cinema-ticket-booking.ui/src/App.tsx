import shuffle from "lodash.shuffle";
import { configure, runInAction } from "mobx";
import { observer } from "mobx-react-lite";
import { useEffect, useState } from "react";
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import About from "./pages/About";
import NotFound from "./pages/NotFound";
import AdminCinema from "./pages/admin/cinema/AdminCinema";
import AdminGenre from "./pages/admin/genre/AdminGenre";
import AdminHall from "./pages/admin/hall/AdminHall";
import AdminMovie from "./pages/admin/movie/AdminMovie";
import AdminPerson from "./pages/admin/person/AdminPerson";
import AdminProjection from "./pages/admin/projection/AdminProjection";
import AdminRating from "./pages/admin/rating/AdminRating";
import AdminReservation from "./pages/admin/reservation/AdminReservation";
import AdminSeat from "./pages/admin/seat/AdminSeat";
import AdminUser from "./pages/admin/user/AdminUser";
import Sidebar from "./pages/common/sidebar/Sidebar";
import Home from "./pages/home/Home";
import Registration from "./pages/registration/Registration";
import Reservations from "./pages/reservation/Reservations";
import { useStore } from "./stores/common/StoreProvider";
import ApiRequest from "./utils/enum/ApiRequest";
import Pages from "./utils/enum/Pages";
import type Movie from "./utils/type/Movie";

if (process.env.NODE_ENV === "development") {
	configure({
		computedRequiresReaction: true,
		enforceActions: "always",
		observableRequiresReaction: true,
		reactionRequiresObservable: true
	});
}

const App = () => {
	const { authStore, movieCarouselStore } = useStore();
	const [movieCarousel, setMovieCarousel] = useState<Movie[]>([]);

	useEffect(() => {
		if (!movieCarousel.length) {
			movieCarouselStore.setRequest(ApiRequest.Page, 1);
			movieCarouselStore.setRequest(ApiRequest.Size, 11);
			movieCarouselStore.setRequest(ApiRequest.Sort, 4);
			movieCarouselStore.get().then(() => {
				runInAction(() => {
					const shuffled = shuffle(movieCarouselStore.getResponse);
					shuffled.length = 7;
					setMovieCarousel(shuffled);
				});
			});
		}
	}, [movieCarousel.length, movieCarouselStore]);

	return (
		<Router>
			<Sidebar />
			<Routes>
				{authStore.isAdmin && (
					<>
						<Route
							element={<AdminCinema />}
							path={Pages.AdminCinema}
						/>
						<Route
							element={<AdminGenre />}
							path={Pages.AdminGenre}
						/>
						<Route element={<AdminHall />} path={Pages.AdminHall} />
						<Route
							element={<AdminMovie />}
							path={Pages.AdminMovie}
						/>
						<Route
							element={<AdminPerson />}
							path={Pages.AdminPerson}
						/>
						<Route
							element={<AdminProjection />}
							path={Pages.AdminProjection}
						/>
						<Route
							element={<AdminRating />}
							path={Pages.AdminRating}
						/>
						<Route
							element={<AdminReservation />}
							path={Pages.AdminReservation}
						/>

						<Route element={<AdminSeat />} path={Pages.AdminSeat} />
						<Route element={<AdminUser />} path={Pages.AdminUser} />
					</>
				)}

				{authStore.isLoggedIn && !authStore.isAdmin && (
					<Route
						element={<Reservations />}
						path={Pages.Reservations}
					/>
				)}

				<Route
					element={<Home carouselMovies={movieCarousel} />}
					path={Pages.Home}
				/>

				<Route element={<Registration />} path={Pages.Registration} />
				<Route element={<About />} path={Pages.About} />
				<Route element={<NotFound />} path="*" />
			</Routes>
		</Router>
	);
};

export default observer(App);
