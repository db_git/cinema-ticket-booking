using System.Linq.Expressions;
using AutoMapper;
using cinema_ticket_booking.dal;
using cinema_ticket_booking.model.RatingRest;
using cinema_ticket_booking.repository.common;
using cinema_ticket_booking.service.common;

namespace cinema_ticket_booking.service;

public sealed class RatingService : IGenericService<Rating, UpdateRatingDto>
{
	private readonly IRepositoryWork _repositoryWork;
	private readonly IGenericRepository<Rating> _repo;
	private readonly IMapper _mapper;

	public RatingService(IRepositoryWork repositoryWork, IMapper mapper)
	{
		_repositoryWork = repositoryWork;
		_repo = _repositoryWork.RatingRepository;
		_mapper = mapper;
	}

	public async Task<ICollection<Rating>?> GetAll(string? search, int? sort)
	{
		Expression<Func<Rating, bool>>? expression = null;
		Func<IQueryable<Rating>, IOrderedQueryable<Rating>>? orderBy = null;

		if (search != null) expression = r =>
			r.Type.ToLower().Contains(search.ToLower());

		if (sort != null) orderBy = sort.Value switch
		{
			1 => ir => ir.OrderBy(p => p.Type),
			2 => ir => ir.OrderByDescending(p => p.Type),
			_ => ir => ir.OrderBy(p => p.Type)
		};

		return await _repo.GetAll(expression, orderBy);
	}

	public async Task<Rating?> Get(Expression<Func<Rating, bool>> expression)
	{
		return await _repo.Get(expression);
	}

	public async Task Create(Rating rating)
	{
		rating.Id = Guid.NewGuid();

		await _repo.Create(rating);
		await _repositoryWork.Save();
	}

	public async Task Update(Guid id, UpdateRatingDto ratingDto)
	{
		var oldRating = await _repo.Get(c => c.Id == id);

		if (oldRating != null)
		{
			_mapper.Map(ratingDto, oldRating);
			_repo.Update(oldRating);
			await _repositoryWork.Save();
		}
	}

	public async Task Delete(Guid id)
	{
		var rating = await Get(c => c.Id == id);

		if (rating != null)
		{
			_repo.Delete(rating);
			await _repositoryWork.Save();
		}
	}
}
