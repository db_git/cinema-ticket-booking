using System.IdentityModel.Tokens.Jwt;
using System.Linq.Expressions;
using System.Security.Claims;
using System.Text;
using cinema_ticket_booking.dal.Identity;
using cinema_ticket_booking.model.UserRest;
using cinema_ticket_booking.repository.common;
using cinema_ticket_booking.service.common;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;

namespace cinema_ticket_booking.service
{
	public class UserService : IUserManagerService
	{
		private readonly IGenericRepository<User> _repo;
		private readonly UserManager<User> _userManager;

		public UserService(IGenericRepository<User> repo, UserManager<User> userManager)
		{
			_repo = repo;
			_userManager = userManager;
		}

		public async Task<User?> Get(Expression<Func<User, bool>> expression)
		{
			return await _repo.Get(expression, i => i
				.Include(u => u.Reservations)
				.Include(u => u.UserRoles)
				.ThenInclude(u => u.Role)
			);
		}

		public async Task<ICollection<User>?> GetAll(string? search, int? sort)
		{
			Expression<Func<User, bool>>? expression = null;
			Func<IQueryable<User>, IOrderedQueryable<User>>? orderBy = null;

			if (search != null) expression = u =>
				u.Email.ToLower().Contains(search.ToLower()) ||
				u.FirstName.ToLower().Contains(search.ToLower()) ||
				u.LastName.ToLower().Contains(search.ToLower());

			if (sort != null) orderBy = sort.Value switch
			{
				1 => iu => iu.OrderBy(u => u.Email),
				2 => iu => iu.OrderByDescending(u => u.Email),
				3 => iu => iu.OrderBy(u => u.FirstName),
				4 => iu => iu.OrderByDescending(u => u.FirstName),
				5 => iu => iu.OrderBy(u => u.LastName),
				6 => iu => iu.OrderByDescending(u => u.LastName),
				_ => iu => iu.OrderBy(u => u.Email)
			};

			return await _repo.GetAll(expression, orderBy, i => i
				.Include(u => u.Reservations)
				.Include(u => u.UserRoles)
				.ThenInclude(u => u.Role));
		}

		public Task Create(User user) { throw new NotImplementedException(); }
		public async Task Update(Guid id, UpdateUserDto userDto)
		{
			/*
			 * Implement later
			 *
			 * var oldUser = await Get(u => u.Id == id);
			 * _mapper.Map(userDto, oldUser);
			 * _repo.Update(oldUser);
			 * await _repositoryWork.Save();
			 */
		}
		public Task Delete(Guid id) { throw new NotImplementedException(); }

		public async Task<IdentityResult> CreateAsync(User user, string password)
		{
			user.UserName = user.Email;

			var result = await _userManager.CreateAsync(user, password);
			if (result.Succeeded) await _userManager.AddToRoleAsync(user, "User");
			return result;
		}

		public async Task<IdentityResult> DeleteAsync(Guid id)
		{
			var user = await Get(u => u.Id == id);
			if (user == null) return IdentityResult.Failed();
			return await _userManager.DeleteAsync(user);
		}

		public async Task<bool> ValidateUser(string email, string password)
		{
			var user = await _userManager.FindByEmailAsync(email);
			return user != null && await _userManager.CheckPasswordAsync(user, password);
		}

		public async Task<string> CreateToken(IConfiguration configuration, string username)
		{
			var jwt = configuration.GetSection("Jwt");

			var signingCredentials = GetSigningCredentials(jwt.GetSection("Key").Value);
			var claims = await GetClaims(username);
			var tokenOptions = GenerateTokenOptions(jwt, signingCredentials, claims);

			return new JwtSecurityTokenHandler().WriteToken(tokenOptions);
		}

		private static JwtSecurityToken GenerateTokenOptions(
			IConfiguration jwt,
			SigningCredentials signingCredentials,
			IEnumerable<Claim> claims)
		{
			var token = new JwtSecurityToken(
				jwt.GetSection("Issuer").Value,
				claims: claims,
				expires: DateTime.UtcNow.AddMinutes(double.Parse(jwt.GetSection("Lifetime").Value)),
				signingCredentials: signingCredentials
			);

			return token;
		}

		private async Task<IEnumerable<Claim>> GetClaims(string username)
		{
			var user = await _userManager.FindByEmailAsync(username);
			var roles = await _userManager.GetRolesAsync(user);

			var claims = new List<Claim>
			{
				new ("email", user.Email),
				new ("id", user.Id.ToString())
			};
			claims.AddRange(roles.Select(role => new Claim("role", role)));

			return claims;
		}

		private static SigningCredentials GetSigningCredentials(string key)
		{
			var secret = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(key));
			return new SigningCredentials(secret, SecurityAlgorithms.HmacSha256);
		}
	}
}
