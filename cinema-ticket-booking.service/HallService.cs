using System.Linq.Expressions;
using AutoMapper;
using cinema_ticket_booking.dal;
using cinema_ticket_booking.model.HallRest;
using cinema_ticket_booking.repository.common;
using cinema_ticket_booking.service.common;
using Microsoft.EntityFrameworkCore;

namespace cinema_ticket_booking.service
{
	public sealed class HallService : IGenericService<Hall, UpdateHallDto>
	{
		private readonly IRepositoryWork _repositoryWork;
		private readonly IGenericRepository<Hall> _repo;
		private readonly IMapper _mapper;

		public HallService(IRepositoryWork repositoryWork, IMapper mapper)
		{
			_repositoryWork = repositoryWork;
			_repo = _repositoryWork.HallRepository;
			_mapper = mapper;
		}

		public async Task<ICollection<Hall>?> GetAll(string? search, int? sort)
		{
			Expression<Func<Hall, bool>>? expression = null;
			Func<IQueryable<Hall>, IOrderedQueryable<Hall>>? orderBy = null;

			if (search != null) expression = h =>
				h.Name.ToLower().Contains(search.ToLower()) ||
				h.Cinema.Name.ToLower().Contains(search.ToLower());

			if (sort != null) orderBy = sort.Value switch
			{
				1 => ih => ih.OrderBy(h => h.Name),
				2 => ih => ih.OrderByDescending(h => h.Name),
				3 => ih => ih.OrderBy(h => h.Cinema.Name),
				4 => ih => ih.OrderByDescending(h => h.Cinema.Name),
				_ => ih => ih.OrderBy(h => h.Name)
			};

			return await _repo.GetAll(expression, orderBy, i => i
				.Include(h => h.Cinema)
				.Include(h => h.Seats));
		}

		public async Task<Hall?> Get(Expression<Func<Hall, bool>> expression)
		{
			return await _repo.Get(expression, i => i
				.Include(h => h.Cinema)
				.Include(h => h.Seats)
			);
		}

		public async Task Create(Hall hall)
		{
			hall.Id = Guid.NewGuid();

			await _repo.Create(hall);
			await _repositoryWork.Save();
		}

		public async Task Update(Guid id, UpdateHallDto hallDto)
		{
			var oldHall = await _repo.Get(c => c.Id == id);

			if (oldHall != null)
			{
				_mapper.Map(hallDto, oldHall);
				_repo.Update(oldHall);
				await _repositoryWork.Save();
			}
		}

		public async Task Delete(Guid id)
		{
			var hall = await Get(c => c.Id == id);

			if (hall != null)
			{
				_repo.Delete(hall);
				await _repositoryWork.Save();
			}
		}
	}
}
