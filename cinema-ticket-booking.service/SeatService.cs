using System.Linq.Expressions;
using AutoMapper;
using cinema_ticket_booking.dal;
using cinema_ticket_booking.model.SeatRest;
using cinema_ticket_booking.repository.common;
using cinema_ticket_booking.service.common;
using Microsoft.EntityFrameworkCore;

namespace cinema_ticket_booking.service
{
	public sealed class SeatService : IGenericService<Seat, UpdateSeatDto>
	{
		private readonly IRepositoryWork _repositoryWork;
		private readonly IGenericRepository<Seat> _repo;
		private readonly IMapper _mapper;

		public SeatService(IRepositoryWork repositoryWork, IMapper mapper)
		{
			_repositoryWork = repositoryWork;
			_repo = _repositoryWork.SeatRepository;
			_mapper = mapper;
		}

		public async Task<ICollection<Seat>?> GetAll(string? search, int? sort)
		{
			Expression<Func<Seat, bool>>? expression = null;
			Func<IQueryable<Seat>, IOrderedQueryable<Seat>>? orderBy = null;

			if (search != null) expression = s =>
				s.Hall.Id.ToString().ToLower().Contains(search.ToLower()) ||
				s.Number.ToString().Contains(search);

			if (sort != null) orderBy = sort.Value switch
			{
				1 => x => x.OrderBy(s => s.Number),
				2 => x => x.OrderByDescending(s => s.Number),
				_ => x => x.OrderBy(s => s.Number),
			};

			return await _repo.GetAll(expression, orderBy, i => i
				.Include(s => s.Hall)
			);
		}

		public async Task<Seat?> Get(Expression<Func<Seat, bool>> expression)
		{
			return await _repo.Get(expression, i => i
				.Include(s => s.Hall)
			);
		}

		public async Task Create(Seat seat)
		{
			seat.Id = Guid.NewGuid();

			await _repo.Create(seat);
			await _repositoryWork.Save();
		}

		public async Task Update(Guid id, UpdateSeatDto seatDto)
		{
			var oldSeat = await _repo.Get(c => c.Id == id);

			if (oldSeat != null)
			{
				_mapper.Map(seatDto, oldSeat);
				_repo.Update(oldSeat);
				await _repositoryWork.Save();
			}
		}

		public async Task Delete(Guid id)
		{
			var seat = await Get(c => c.Id == id);

			if (seat != null)
			{
				_repo.Delete(seat);
				await _repositoryWork.Save();
			}
		}
	}
}
