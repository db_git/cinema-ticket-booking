using System.Linq.Expressions;
using AutoMapper;
using cinema_ticket_booking.dal;
using cinema_ticket_booking.model.GenreRest;
using cinema_ticket_booking.repository.common;
using cinema_ticket_booking.service.common;

namespace cinema_ticket_booking.service;

public sealed class GenreService : IGenericService<Genre, UpdateGenreDto>
{
	private readonly IRepositoryWork _repositoryWork;
	private readonly IGenericRepository<Genre> _repo;
	private readonly IMapper _mapper;

	public GenreService(IRepositoryWork repositoryWork, IMapper mapper)
	{
		_repositoryWork = repositoryWork;
		_repo = _repositoryWork.GenreRepository;
		_mapper = mapper;
	}

	public async Task<ICollection<Genre>?> GetAll(string? search, int? sort)
	{
		Expression<Func<Genre, bool>>? expression = null;
		Func<IQueryable<Genre>, IOrderedQueryable<Genre>>? orderBy = null;

		if (search != null) expression = g =>
			g.Name.ToLower().Contains(search.ToLower());

		if (sort != null) orderBy = sort.Value switch
		{
			1 => ig => ig.OrderBy(g => g.Name),
			2 => ig => ig.OrderByDescending(g => g.Name),
			_ => ig => ig.OrderBy(g => g.Name)
		};

		return await _repo.GetAll(expression, orderBy);
	}

	public async Task<Genre?> Get(Expression<Func<Genre, bool>> expression)
	{
		return await _repo.Get(expression);
	}

	public async Task Create(Genre genre)
	{
		genre.Id = Guid.NewGuid();

		await _repo.Create(genre);
		await _repositoryWork.Save();
	}

	public async Task Update(Guid id, UpdateGenreDto genreDto)
	{
		var oldGenre = await _repo.Get(c => c.Id == id);

		if (oldGenre != null)
		{
			_mapper.Map(genreDto, oldGenre);
			_repo.Update(oldGenre);
			await _repositoryWork.Save();
		}
	}

	public async Task Delete(Guid id)
	{
		var genre = await Get(c => c.Id == id);

		if (genre != null)
		{
			_repo.Delete(genre);
			await _repositoryWork.Save();
		}
	}
}
