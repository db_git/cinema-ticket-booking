using System.Linq.Expressions;
using AutoMapper;
using cinema_ticket_booking.dal;
using cinema_ticket_booking.model.MovieRest;
using cinema_ticket_booking.repository.common;
using cinema_ticket_booking.service.common;
using Microsoft.EntityFrameworkCore;

namespace cinema_ticket_booking.service;

public sealed class MovieService : IGenericService<Movie, UpdateMovieDto>
{
	private readonly IRepositoryWork _repositoryWork;
	private readonly IGenericRepository<Movie> _repo;
	private readonly IMapper _mapper;

	public MovieService(IRepositoryWork repositoryWork, IMapper mapper)
	{
		_repositoryWork = repositoryWork;
		_repo = _repositoryWork.MovieRepository;
		_mapper = mapper;
	}

	public async Task<ICollection<Movie>?> GetAll(string? search, int? sort)
	{
		Expression<Func<Movie, bool>>? expression = null;
		Func<IQueryable<Movie>, IOrderedQueryable<Movie>>? orderBy = null;

		if (search != null) expression = m =>
			m.Title.ToLower().Contains(search.ToLower()) ||
			m.Year.ToString().Contains(search);

		if (sort != null) orderBy = sort.Value switch
		{
			1 => im => im.OrderBy(m => m.Title),
			2 => im => im.OrderByDescending(m => m.Title),
			3 => im => im.OrderBy(m => m.Year),
			4 => im => im.OrderByDescending(m => m.Year),
			5 => im => im.OrderBy(m => m.Duration),
			6 => im => im.OrderByDescending(m => m.Duration),
			_ => im => im.OrderBy(m => m.Title)
		};

		return await _repo.GetAll(expression, orderBy, i => i
			.Include(m => m.MovieGenres)
			.ThenInclude(mg => mg.Genre)
			.Include(m => m.MoviePeople)
			.ThenInclude(mp => mp.Person)
			.Include(m => m.MovieRatings)
			.ThenInclude(mr => mr.Rating));
	}

	public async Task<Movie?> Get(Expression<Func<Movie, bool>> expression)
	{
		return await _repo.Get(expression, i => i
			.Include(m => m.MovieGenres)
			.ThenInclude(mg => mg.Genre)
			.Include(m => m.MoviePeople)
			.ThenInclude(mp => mp.Person)
			.Include(m => m.MovieRatings)
			.ThenInclude(mr => mr.Rating)
		);
	}

	public async Task Create(Movie movie)
	{
		movie.Id = Guid.NewGuid();

		await _repo.Create(movie);
		await _repositoryWork.Save();
	}

	public async Task Update(Guid id, UpdateMovieDto movieDto)
	{
		var oldMovie = await Get(c => c.Id == id);

		if (oldMovie != null)
		{
			_mapper.Map(movieDto, oldMovie);
			_repo.Update(oldMovie);
			await _repositoryWork.Save();
		}
	}

	public async Task Delete(Guid id)
	{
		var movie = await Get(c => c.Id == id);

		if (movie != null)
		{
			_repo.Delete(movie);
			await _repositoryWork.Save();
		}
	}
}
