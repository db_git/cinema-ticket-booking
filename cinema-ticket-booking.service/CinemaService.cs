using System.Globalization;
using System.Linq.Expressions;
using AutoMapper;
using cinema_ticket_booking.dal;
using cinema_ticket_booking.model.CinemaRest;
using cinema_ticket_booking.repository.common;
using cinema_ticket_booking.service.common;
using Microsoft.EntityFrameworkCore;

namespace cinema_ticket_booking.service;

public sealed class CinemaService : IGenericService<Cinema, UpdateCinemaDto>
{
	private readonly IRepositoryWork _repositoryWork;
	private readonly IGenericRepository<Cinema> _repo;
	private readonly IMapper _mapper;

	public CinemaService(IRepositoryWork repositoryWork, IMapper mapper)
	{
		_repositoryWork = repositoryWork;
		_repo = _repositoryWork.CinemaRepository;
		_mapper = mapper;
	}

	public async Task<ICollection<Cinema>?> GetAll(string? search, int? sort)
	{
		Expression<Func<Cinema, bool>>? expression = null;
		Func<IQueryable<Cinema>, IOrderedQueryable<Cinema>>? orderBy = null;

		if (search != null) expression = c =>
			c.Name.ToLower().Contains(search.ToLower()) ||
			c.City.ToLower().Contains(search.ToLower()) ||
			c.Street.ToLower().Contains(search.ToLower());

		if (sort != null) orderBy = sort.Value switch
		{
			1 => ic => ic.OrderBy(c => c.Name),
			2 => ic => ic.OrderByDescending(c => c.Name),
			3 => ic => ic.OrderBy(c => c.City),
			4 => ic => ic.OrderByDescending(c => c.City),
			5 => ic => ic.OrderBy(c => c.Street),
			6 => ic => ic.OrderByDescending(c => c.Street),
			7 => ic => ic.OrderBy(c => c.TicketPrice),
			8 => ic => ic.OrderByDescending(c => c.TicketPrice),
			_ => ic => ic.OrderBy(c => c.Name)
		};

		return await _repo.GetAll(expression, orderBy, i => i
			.Include(c => c.Halls));
	}

	public async Task<Cinema?> Get(Expression<Func<Cinema, bool>> expression)
	{
		return await _repo.Get(expression, i => i
			.Include(c => c.Halls)
		);
	}

	public async Task Create(Cinema cinema)
	{
		cinema.Id = Guid.NewGuid();

		await _repo.Create(cinema);
		await _repositoryWork.Save();
	}

	public async Task Update(Guid id, UpdateCinemaDto cinemaDto)
	{
		var oldCinema = await _repo.Get(c => c.Id == id);

		if (oldCinema != null)
		{
			_mapper.Map(cinemaDto, oldCinema);
			_repo.Update(oldCinema);
			await _repositoryWork.Save();
		}
	}

	public async Task Delete(Guid id)
	{
		var cinema = await Get(c => c.Id == id);

		if (cinema != null)
		{
			_repo.Delete(cinema);
			await _repositoryWork.Save();
		}
	}
}
