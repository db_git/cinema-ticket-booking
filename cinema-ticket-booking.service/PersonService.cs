using System.Linq.Expressions;
using AutoMapper;
using cinema_ticket_booking.dal;
using cinema_ticket_booking.model.PersonRest;
using cinema_ticket_booking.repository.common;
using cinema_ticket_booking.service.common;

namespace cinema_ticket_booking.service;

public sealed class PersonService : IGenericService<Person, UpdatePersonDto>
{
	private readonly IRepositoryWork _repositoryWork;
	private readonly IGenericRepository<Person> _repo;
	private readonly IMapper _mapper;

	public PersonService(IRepositoryWork repositoryWork, IMapper mapper)
	{
		_repositoryWork = repositoryWork;
		_repo = _repositoryWork.PersonRepository;
		_mapper = mapper;
	}

	public async Task<ICollection<Person>?> GetAll(string? search, int? sort)
	{
		Expression<Func<Person, bool>>? expression = null;
		Func<IQueryable<Person>, IOrderedQueryable<Person>>? orderBy = null;

		if (search != null) expression = p =>
			p.FirstName.ToLower().Contains(search.ToLower()) ||
			p.LastName.ToLower().Contains(search.ToLower());

		if (sort != null) orderBy = sort.Value switch
		{
			1 => ip => ip.OrderBy(p => p.FirstName),
			2 => ip => ip.OrderByDescending(p => p.FirstName),
			3 => ip => ip.OrderBy(p => p.LastName),
			4 => ip => ip.OrderByDescending(p => p.LastName),
			_ => ip => ip.OrderBy(p => p.LastName),
		};

		return await _repo.GetAll(expression, orderBy);
	}

	public async Task<Person?> Get(Expression<Func<Person, bool>> expression)
	{
		return await _repo.Get(expression);
	}

	public async Task Create(Person person)
	{
		person.Id = Guid.NewGuid();

		await _repo.Create(person);
		await _repositoryWork.Save();
	}

	public async Task Update(Guid id, UpdatePersonDto personDto)
	{
		var oldPerson = await _repo.Get(c => c.Id == id);

		if (oldPerson != null)
		{
			_mapper.Map(personDto, oldPerson);
			_repo.Update(oldPerson);
			await _repositoryWork.Save();
		}
	}

	public async Task Delete(Guid id)
	{
		var person = await Get(c => c.Id == id);

		if (person != null)
		{
			_repo.Delete(person);
			await _repositoryWork.Save();
		}
	}
}
