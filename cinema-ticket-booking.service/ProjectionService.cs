using System.Linq.Expressions;
using AutoMapper;
using cinema_ticket_booking.dal;
using cinema_ticket_booking.model.ProjectionRest;
using cinema_ticket_booking.repository.common;
using cinema_ticket_booking.service.common;
using Microsoft.EntityFrameworkCore;

namespace cinema_ticket_booking.service;

public sealed class ProjectionService : IGenericService<Projection, UpdateProjectionDto>
{
	private readonly IRepositoryWork _repositoryWork;
	private readonly IGenericRepository<Projection> _repo;
	private readonly IMapper _mapper;

	public ProjectionService(IRepositoryWork repositoryWork, IMapper mapper)
	{
		_repositoryWork = repositoryWork;
		_repo = _repositoryWork.ProjectionRepository;
		_mapper = mapper;
	}

	public async Task<ICollection<Projection>?> GetAll(string? search, int? sort)
	{
		Expression<Func<Projection, bool>>? expression = null;
		Func<IQueryable<Projection>, IOrderedQueryable<Projection>>? orderBy = null;

		if (search != null) expression = p =>
			p.Movie.Id.ToString().ToLower().Contains(search.ToLower()) ||
			p.Hall.Name.ToLower().Contains(search.ToLower()) ||
			p.Movie.Title.ToLower().Contains(search.ToLower());

		if (sort != null) orderBy = sort.Value switch
		{
			1 => ip => ip.OrderBy(p => p.Time),
			2 => ip => ip.OrderByDescending(p => p.Time),
			_ => ip => ip.OrderBy(p => p.Time)
		};

		return await _repo.GetAll(expression, orderBy, i => i
			.Include(p => p.Hall)
			.ThenInclude(h => h.Seats)
			.Include(p => p.Movie)
		);
	}

	public async Task<Projection?> Get(Expression<Func<Projection, bool>> expression)
	{
		return await _repo.Get(expression, i => i
			.Include(p => p.Hall)
			.ThenInclude(h => h.Seats)
			.Include(p => p.Movie)
		);
	}

	public async Task Create(Projection projection)
	{
		projection.Id = Guid.NewGuid();
		projection.Time = DateTime.SpecifyKind(projection.Time, DateTimeKind.Utc);

		await _repo.Create(projection);
		await _repositoryWork.Save();
	}

	public async Task Update(Guid id, UpdateProjectionDto projectionDto)
	{
		var oldProjection = await _repo.Get(c => c.Id == id);

		if (oldProjection != null)
		{
			_mapper.Map(projectionDto, oldProjection);
			oldProjection.Time = DateTime.SpecifyKind(oldProjection.Time, DateTimeKind.Utc);
			_repo.Update(oldProjection);
			await _repositoryWork.Save();
		}
	}

	public async Task Delete(Guid id)
	{
		var projection = await Get(c => c.Id == id);

		if (projection != null)
		{
			_repo.Delete(projection);
			await _repositoryWork.Save();
		}
	}
}
