using System.IdentityModel.Tokens.Jwt;
using System.Linq.Expressions;
using cinema_ticket_booking.dal;
using cinema_ticket_booking.model.ReservationRest;
using cinema_ticket_booking.repository.common;
using cinema_ticket_booking.service.common;
using Microsoft.EntityFrameworkCore;

namespace cinema_ticket_booking.service;

public sealed class UserReservationService : IUserReservationService
{
	private readonly IRepositoryWork _repositoryWork;
	private readonly IGenericRepository<Reservation> _repo;

	public UserReservationService(IRepositoryWork repositoryWork)
	{
		_repositoryWork = repositoryWork;
		_repo = _repositoryWork.ReservationRepository;
	}

	public async Task<ICollection<Reservation>?> UserGetAll(string token, string? search, int? sort)
	{
		Expression<Func<Reservation, bool>>? searchExpression = null;
		Func<IQueryable<Reservation>, IOrderedQueryable<Reservation>>? orderBy = null;

		if (search != null)
		{
			searchExpression = ExpressionParameterReplacer<Reservation>.OrElse(
				r => r.User.UserName.ToLower().Contains(search.ToLower()),
				r => r.User.FirstName.ToLower().Contains(search.ToLower())
			);
			searchExpression = ExpressionParameterReplacer<Reservation>.OrElse(searchExpression,
				r => r.User.LastName.ToLower().Contains(search.ToLower()));
			searchExpression = ExpressionParameterReplacer<Reservation>.OrElse(searchExpression,
				r => r.Projection.Movie.Title.ToLower().Contains(search.ToLower())
			);
			searchExpression = ExpressionParameterReplacer<Reservation>.OrElse(searchExpression,
				r => r.Projection.Hall.Name.ToLower().Contains(search.ToLower()));
		}

		if (sort != null) orderBy = sort.Value switch
		{
			1 => iur => iur.OrderBy(r => r.Projection.Movie.Title),
			2 => iur => iur.OrderByDescending(r => r.Projection.Movie.Title),
			3 => iur => iur.OrderBy(r => r.Projection.Hall.Name),
			4 => iur => iur.OrderByDescending(r => r.Projection.Hall.Name),
			5 => iur => iur.OrderBy(r => r.Projection.Time),
			6 => iur => iur.OrderByDescending(r => r.Projection.Time),
			_ => iur => iur.OrderBy(r => r.Projection.Movie.Title)
		};

		var id = new Guid(ParseJwt(token, "id"));
		var expression = searchExpression != null
			? ExpressionParameterReplacer<Reservation>.AndAlso(searchExpression, ur => ur.User.Id == id)
			: ur => ur.User.Id == id;

		return await _repo.GetAll(expression, orderBy, i => i
			.Include(ur => ur.User)
			.Include(ur => ur.Projection)
			.ThenInclude(p => p.Movie)
			.Include(ur => ur.Projection)
			.ThenInclude(p => p.Hall)
			.Include(ur => ur.ReservationSeats)
			.ThenInclude(rs => rs.Seat)
		);
	}

	public async Task<Reservation?> UserGet(string token, Expression<Func<Reservation, bool>> expression)
	{
		var id = new Guid(ParseJwt(token, "id"));

		return await _repo.Get(
			ExpressionParameterReplacer<Reservation>.AndAlso(expression, ur => ur.UserId == id),
			i => i
				.Include(ur => ur.User)
				.Include(ur => ur.Projection)
				.ThenInclude(p => p.Movie)
				.Include(ur => ur.Projection)
				.ThenInclude(p => p.Hall)
				.Include(ur => ur.ReservationSeats)
				.ThenInclude(rs => rs.Seat)
		);
	}

	public async Task UserCreate(string token, Reservation reservation)
	{
		var id = new Guid(ParseJwt(token, "id"));
		if (reservation.UserId != id) return;

		reservation.Id = Guid.NewGuid();
		await _repo.Create(reservation);
		await _repositoryWork.Save();
	}

	public async Task UserUpdate(string token, Guid id, UpdateReservationDto reservationDto)
	{
		var oldReservation = await UserGet(token, c => c.Id == id);

		if (oldReservation != null)
		{
			var userId = new Guid(ParseJwt(token, "id"));
			if (oldReservation.UserId != userId) throw new InvalidOperationException();

			oldReservation.ProjectionId = reservationDto.ProjectionId;
			_repo.Update(oldReservation);
			await _repositoryWork.Save();
		}
	}

	public async Task UserDelete(string token, Guid id)
	{
		var reservation = await UserGet(token, c => c.Id == id);

		if (reservation != null)
		{
			var userId = new Guid(ParseJwt(token, "id"));
			if (reservation.User.Id != userId) throw new InvalidOperationException();

			_repo.Delete(reservation);
			await _repositoryWork.Save();
		}
	}

	private static string ParseJwt(string token, string type)
	{
		var jwt = new JwtSecurityToken(token);
		var value = jwt.Claims.First(c => c.Type == type).Value;
		return value;
	}
}
