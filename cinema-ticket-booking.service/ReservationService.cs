using System.Linq.Expressions;
using AutoMapper;
using cinema_ticket_booking.dal;
using cinema_ticket_booking.model.ReservationRest;
using cinema_ticket_booking.repository.common;
using cinema_ticket_booking.service.common;
using Microsoft.EntityFrameworkCore;

namespace cinema_ticket_booking.service;

public sealed class ReservationService : IGenericService<Reservation, UpdateReservationDto>
{
	private readonly IRepositoryWork _repositoryWork;
	private readonly IGenericRepository<Reservation> _repo;
	private readonly IMapper _mapper;

	public ReservationService(IRepositoryWork repositoryWork, IMapper mapper)
	{
		_repositoryWork = repositoryWork;
		_repo = _repositoryWork.ReservationRepository;
		_mapper = mapper;
	}

	public async Task<ICollection<Reservation>?> GetAll(string? search, int? sort)
	{
		Expression<Func<Reservation, bool>>? expression = null;
		Func<IQueryable<Reservation>, IOrderedQueryable<Reservation>>? orderBy = null;

		if (search != null)
		{
			expression = r =>
				r.User.UserName.ToLower().Contains(search.ToLower()) ||
				r.User.FirstName.ToLower().Contains(search.ToLower()) ||
				r.User.LastName.ToLower().Contains(search.ToLower()) ||
				r.ProjectionId.ToString().ToLower().Contains(search.ToLower()) ||
				r.Projection.Movie.Title.ToLower().Contains(search.ToLower()) ||
				r.Projection.Hall.Name.ToLower().Contains(search.ToLower());
		}

		if (sort != null) orderBy = sort.Value switch
		{
			1 => ir => ir.OrderBy(r => r.User.FirstName),
			2 => ir => ir.OrderByDescending(r => r.User.FirstName),
			3 => ir => ir.OrderBy(r => r.User.LastName),
			4 => ir => ir.OrderByDescending(r => r.User.LastName),
			5 => ir => ir.OrderBy(r => r.User.UserName),
			6 => ir => ir.OrderByDescending(r => r.User.UserName),
			7 => ir => ir.OrderBy(r => r.Projection.Movie.Title),
			8 => ir => ir.OrderByDescending(r => r.Projection.Movie.Title),
			9 => ir => ir.OrderBy(r => r.Projection.Hall.Name),
			10 => ir => ir.OrderByDescending(r => r.Projection.Hall.Name),
			11 => ir => ir.OrderBy(r => r.Projection.Time),
			12 => ir => ir.OrderByDescending(r => r.Projection.Time),
			_ => ir => ir.OrderBy(r => r.User.LastName)
		};

		return await _repo.GetAll(expression, orderBy, i => i
			.Include(r => r.User)
			.Include(r => r.Projection)
			.ThenInclude(p => p.Movie)
			.Include(r => r.Projection)
			.ThenInclude(p => p.Hall)
			.Include(r => r.ReservationSeats)
			.ThenInclude(rs => rs.Seat)
		);
	}

	public async Task<Reservation?> Get(Expression<Func<Reservation, bool>> expression)
	{
		return await _repo.Get(expression, i => i
			.Include(r => r.User)
			.Include(r => r.Projection)
			.ThenInclude(p => p.Movie)
			.Include(r => r.Projection)
			.ThenInclude(p => p.Hall)
			.Include(r => r.ReservationSeats)
			.ThenInclude(rs => rs.Seat)
		);
	}

	public async Task Create(Reservation reservation)
	{
		reservation.Id = Guid.NewGuid();

		await _repo.Create(reservation);
		await _repositoryWork.Save();
	}

	public async Task Update(Guid id, UpdateReservationDto reservationDto)
	{
		var oldReservation = await _repo.Get(c => c.Id == id);

		if (oldReservation != null)
		{
			_mapper.Map(reservationDto, oldReservation);
			_repo.Update(oldReservation);
			await _repositoryWork.Save();
		}
	}

	public async Task Delete(Guid id)
	{
		var reservation = await Get(c => c.Id == id);

		if (reservation != null)
		{
			_repo.Delete(reservation);
			await _repositoryWork.Save();
		}
	}
}
