using System.Linq.Expressions;
using cinema_ticket_booking.dal.common;
using cinema_ticket_booking.repository.common;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Query;

namespace cinema_ticket_booking.repository;

public sealed class GenericRepository<T> : IGenericRepository<T> where T : class
{
	private readonly DatabaseContext _context;
	private readonly DbSet<T> _db;

	public GenericRepository(DatabaseContext context)
	{
		_context = context;
		_db = _context.Set<T>();
	}

	public async Task<ICollection<T>?> GetAll(
		Expression<Func<T, bool>>? expression = null,
		Func<IQueryable<T>, IOrderedQueryable<T>>? orderBy = null,
		Func<IQueryable<T>, IIncludableQueryable<T, object>>? includes = null
	)
	{
		IQueryable<T> query = _db;

		if (expression != null) query = query.Where(expression);
		if (orderBy != null) query = orderBy(query);
		if (includes != null) query = includes(query);

		return await query.AsNoTracking().ToListAsync();
	}

	public async Task<T?> Get(
		Expression<Func<T, bool>> expression,
		Func<IQueryable<T>, IIncludableQueryable<T, object>>? includes = null
	)
	{
		IQueryable<T> query = _db;
		if (includes != null) query = includes(query);

		return await query.FirstOrDefaultAsync(expression);
	}

	public async Task Create(T entity)
	{
		await _db.AddAsync(entity);
	}

	public void Update(T entity)
	{
		_db.Attach(entity);
		_context.Entry(entity).State = EntityState.Modified;
	}

	public void Delete(T entity)
	{
		_context.Entry(entity).State = EntityState.Deleted;
		_db.Remove(entity);
		_context.SaveChanges();
	}
}
