using cinema_ticket_booking.dal;
using cinema_ticket_booking.dal.Identity;
using cinema_ticket_booking.dal.common;
using cinema_ticket_booking.repository.common;

namespace cinema_ticket_booking.repository;

public sealed class RepositoryWork : IRepositoryWork
{
	private readonly DatabaseContext _context;
	private IGenericRepository<Cinema>? _cinemaRepository;
	private IGenericRepository<Genre>? _genreRepository;
	private IGenericRepository<Hall>? _hallRepository;
	private IGenericRepository<Movie>? _movieRepository;
	private IGenericRepository<Person>? _personRepository;
	private IGenericRepository<Projection>? _projectionRepository;
	private IGenericRepository<Rating>? _ratingRepository;
	private IGenericRepository<Reservation>? _reservationRepository;
	private IGenericRepository<Seat>? _seatRepository;
	private IGenericRepository<User>? _userRepository;

	public IGenericRepository<Cinema> CinemaRepository =>
		_cinemaRepository ??= new GenericRepository<Cinema>(_context);

	public IGenericRepository<Genre> GenreRepository =>
		_genreRepository ??= new GenericRepository<Genre>(_context);

	public IGenericRepository<Hall> HallRepository =>
		_hallRepository ??= new GenericRepository<Hall>(_context);

	public IGenericRepository<Movie> MovieRepository =>
		_movieRepository ??= new GenericRepository<Movie>(_context);

	public IGenericRepository<Person> PersonRepository =>
		_personRepository ??= new GenericRepository<Person>(_context);

	public IGenericRepository<Projection> ProjectionRepository =>
		_projectionRepository ??= new GenericRepository<Projection>(_context);

	public IGenericRepository<Rating> RatingRepository =>
		_ratingRepository ??= new GenericRepository<Rating>(_context);

	public IGenericRepository<Reservation> ReservationRepository =>
		_reservationRepository ??= new GenericRepository<Reservation>(_context);

	public IGenericRepository<Seat> SeatRepository =>
		_seatRepository ??= new GenericRepository<Seat>(_context);

	public IGenericRepository<User> UserRepository =>
		_userRepository ??= new GenericRepository<User>(_context);

	public RepositoryWork(DatabaseContext context)
	{
		_context = context;
	}

	public void Dispose()
	{
		_context.Dispose();
		GC.SuppressFinalize(this);
	}

	public async Task Save()
	{
		await _context.SaveChangesAsync();
	}

	~RepositoryWork()
	{
		Dispose();
	}
}
